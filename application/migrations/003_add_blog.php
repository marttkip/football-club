<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_blog extends CI_Migration 
{
	public function up()
	{
		$this->load->dbforge();
		//payroll
		$fields = array(
                        'file_data' => array('type' => 'varchar', 'constraint' => '100', 'null' => TRUE)
					);
		$this->dbforge->add_column('payroll', $fields);
	}

	public function down()
	{
		//$this->dbforge->drop_table('blog');
	}
}