<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'human-resource/personnel/branch_id/'.$order_method.'/'.$page.'">Branch</a></th>
						<th><a href="'.site_url().'human-resource/personnel/personnel.personnel_type_id/'.$order_method.'/'.$page.'">Type</a></th>
						<th><a href="'.site_url().'human-resource/personnel/personnel_onames/'.$order_method.'/'.$page.'">Other names</a></th>
						<th><a href="'.site_url().'human-resource/personnel/personnel_fname/'.$order_method.'/'.$page.'">First name</a></th>
						<th><a href="'.site_url().'human-resource/personnel/personnel_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$personnel_id = $row->personnel_id;
				$branch_id = $row->branch_id;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_username = $row->personnel_username;
				$personnel_phone = $row->personnel_phone;
				$personnel_email = $row->personnel_email;
				$personnel_status = $row->personnel_status;
				$personnel_type_name = $row->personnel_type_name;
				$personnel_name = $personnel_fname.' '.$personnel_onames;
				
				//status
				if($personnel_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				
				
				//get branch
				$branch = '';
				if($branches->num_rows() > 0)
				{
					foreach($branches->result() as $res)
					{
						$branch_id2 = $res->branch_id;
						if($branch_id == $branch_id2)
						{
							$branch = $res->branch_name;
						}
					}
				}
				
				if($module == 2)
				{
					if($personnel_status == 0)
					{
						$status = '<span class="label label-default">Deactivated</span>';
						$button = '<a class="btn btn-sm btn-info" href="'.site_url().'human-resource/activate-user/'.$personnel_id.'" onclick="return confirm(\'Do you want to activate '.$personnel_name.'?\');" title="Activate '.$personnel_name.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
					}
					//create activated status display
					else if($personnel_status == 1)
					{
						$status = '<span class="label label-success">Active</span>';
						$button = '<a class="btn btn-sm btn-default" href="'.site_url().'human-resource/deactivate-user/'.$personnel_id.'" onclick="return confirm(\'Do you want to deactivate '.$personnel_name.'?\');" title="Deactivate '.$personnel_name.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
					}
					$buttons = '<td><a href="'.site_url().'human-resource/reset-password-user/'.$personnel_id.'" class="btn btn-sm btn-sm btn-warning" onclick="return confirm(\'Reset password for '.$personnel_fname.'?\');">Reset Password</a></td>
								<td><a href="'.site_url().'human-resource/edit-user/'.$personnel_id.'" class="btn btn-sm btn-sm btn-success" title="Edit '.$personnel_name.'"><i class="fa fa-pencil"></i> Edit User</a></td>
								<td>'.$button.'</td>
								<td><a href="'.site_url().'human-resource/delete-user/'.$personnel_id.'" class="btn btn-sm btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$personnel_name.'?\');" title="Delete '.$personnel_name.'"><i class="fa fa-trash"> Delete</i></a></td>';
				}
				else
				{
					if($personnel_status == 0)
					{
						$status = '<span class="label label-default">Deactivated</span>';
						$button = '<a class="btn btn-sm btn-info" href="'.site_url().'human-resource/activate-personnel/'.$personnel_id.'" onclick="return confirm(\'Do you want to activate '.$personnel_name.'?\');" title="Activate '.$personnel_name.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
					}
					//create activated status display
					else if($personnel_status == 1)
					{
						$status = '<span class="label label-success">Active</span>';
						$button = '<a class="btn btn-sm btn-default" href="'.site_url().'human-resource/deactivate-personnel/'.$personnel_id.'" onclick="return confirm(\'Do you want to deactivate '.$personnel_name.'?\');" title="Deactivate '.$personnel_name.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
					}
					$buttons = '<td><a href="'.site_url().'human-resource/reset-password/'.$personnel_id.'" class="btn btn-sm btn-sm btn-warning" onclick="return confirm(\'Reset password for '.$personnel_fname.'?\');">Reset Password</a></td>
								<td><a href="'.site_url().'human-resource/edit-personnel/'.$personnel_id.'" class="btn btn-sm btn-sm btn-success" title="Edit '.$personnel_name.'"><i class="fa fa-pencil"></i> Edit Personnel</a></td>
								<td>'.$button.'</td>
								<td><a href="'.site_url().'human-resource/delete-personnel/'.$personnel_id.'" class="btn btn-sm btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$personnel_name.'?\');" title="Delete '.$personnel_name.'"><i class="fa fa-trash"> Delete</i></a></td>';
				}
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$branch.'</td>
						<td>'.$personnel_type_name.'</td>
						<td>'.$personnel_onames.'</td>
						<td>'.$personnel_fname.'</td>
						<td>'.$status.'</td>
						'.$buttons.'
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no personnel";
		}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search personnel</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("hr/personnel/search_personnel", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Branch: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="branch_id">
                            	<option value="">---Select Branch---</option>
                                <?php
                                    if($branches->num_rows() > 0){
                                        foreach($branches->result() as $row):
                                            $branch_name = $row->branch_name;
                                            $branch_id= $row->branch_id;
                                            ?><option value="<?php echo $branch_id; ?>" ><?php echo $branch_name; ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Personnel Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_number" placeholder="Personnel number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">First name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_fname" placeholder="First name">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Other names: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_onames" placeholder="Other names">
                        </div>
                    </div>
            
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4">
                        	<div class="center-align">
                            	<button type="submit" class="btn btn-sm btn-info btn-sm">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
		<div class="row" style="margin-top:-25px;">
            <!--<div class="col-lg-2 col-lg-offset-8">
                <a href="<?php echo site_url();?>human-resource/export-personnel" class="btn btn-sm btn-sm btn-success pull-right">Export</a>
            </div>-->
            <div class="col-lg-12">
            	<?php
            		if($module == 2)
            		{
            			?>

            				<a href="<?php echo site_url();?>human-resource/add-user" class="btn btn-sm btn-sm btn-info pull-right fa fa-plus"> Add User</a>
            			<?php
            		}
            		else
            		{
            			?>

            				<a href="<?php echo site_url();?>human-resource/add-personnel" class="btn btn-sm btn-sm btn-info pull-right fa fa-plus">Add Personnel</a>
            			<?php
            		}
            	?>
            </div>
        </div>
	</header>
	<div class="panel-body">
    	<?php
		$search = $this->session->userdata('personnel_search_title2');
		
		if(!empty($search))
		{
			echo '<h6>Filtered by: '.$search.'</h6>';
			echo '<a href="'.site_url().'hr/personnel/close_search" class="btn btn-sm btn-sm btn-info pull-left">Close search</a>';
		}
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		?>

		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    <div class="panel-footer">
    	<?php if(isset($links)){echo $links;}?>
    </div>
</section>