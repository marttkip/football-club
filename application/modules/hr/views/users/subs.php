<?php
//referee data
$minute = set_value('minute');
$home_player_in = set_value('home_player_in');
$away_player_in = set_value('away_player_in');
$home_player_out = set_value('home_player_out');
$away_player_out = set_value('away_player_out');

?>         
 <section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
   
    <div class="panel-body">
    	<div class="row" style="margin-bottom:20px;">
            <a href="<?php echo site_url();?>referee-postings" class="btn btn-warning pull-right">Back to Referee Posting</a>
        </div>
                        
    <!-- Adding Errors -->
    <?php
		$success = $this->session->userdata('success_message');
		$error = $this->session->userdata('error_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			
			$this->session->unset_userdata('error_message');
		}
		$validation_errors = validation_errors();
		
		if(!empty($validation_errors))
		{
			echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
		}
    ?>
		<div class = "row">
			<div class = "col-md-6">
				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title"><?php echo $title_home;?></h2>
					</header>
				   
					<div class="panel-body">
						<?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
						<div class="form-group">
							<label class="col-lg-5 control-label">Minute: </label>
							
							<div class="col-lg-7">
								<input type="text" class="form-control" name="home_minute" placeholder="minute" value="<?php echo $minute;?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label">Player Out: </label>
							
							<div class="col-lg-7">
								<select class="form-control" name="home_player_out">
									<?php
										if($team_details_home_out->num_rows() > 0)
										{
											$gender = $team_details_home_out->result();
											
											foreach($gender as $res)
											{
												$player_id = $res->player_id;
												$player_fname = $res->player_fname;
												$player_onames = $res->player_onames;
												$player_name = $player_fname.' '.$player_onames;
												
												if($player_id == $home_player_out)
												{
													echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
												}
												
												else
												{
													echo '<option value="'.$player_id.'">'.$player_name.'</option>';
												}
											}
										}
									?>
								</select>
							</div>
		
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label">Player In: </label>
							
							<div class="col-lg-7">
								<select class="form-control" name="home_player_in">
									<?php
										if($team_details_home_in->num_rows() > 0)
										{
											$gender = $team_details_home_in->result();
											
											foreach($gender as $res)
											{
												$player_id = $res->player_id;
												$player_fname = $res->player_fname;
												$player_onames = $res->player_onames;
												$player_name = $player_fname.' '.$player_onames;
												
												if($player_id == $home_player_in)
												{
													echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
												}
												
												else
												{
													echo '<option value="'.$player_id.'">'.$player_name.'</option>';
												}
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="row" style="margin-top:10px;">
							<div class="form-actions center-align">
								<button class="submit btn btn-success" type="submit">
									Add Home Substitute
								</button>
							</div>
						</div>
						<?php echo form_close();?>
					</div>
				</section>
			</div>
			<div class = "col-md-6">
				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title"><?php echo $title_away;?></h2>
					</header>
				   
					<div class="panel-body">
						<?php echo form_open(base_url().'soccer_management/referee/away-substitute/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
						<div class="form-group">
							<label class="col-lg-5 control-label">Minute: </label>
							
							<div class="col-lg-7">
								<input type="text" class="form-control" name="away_minute" placeholder="minute" value="<?php echo $minute;?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label">Player Out: </label>
							
							<div class="col-lg-7">
								<select class="form-control" name="away_player_out">
									<?php
										if($team_details_away_out->num_rows() > 0)
										{
											$away_out = $team_details_away_out->result();
											
											foreach($away_out as $away_res)
											{
												$player_id = $away_res->player_id;
												$player_fname = $away_res->player_fname;
												$player_onames = $res->player_onames;
												$player_name = $player_fname.' '.$player_onames;
												
												if($player_id == $away_player_out)
												{
													echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
												}
												
												else
												{
													echo '<option value="'.$player_id.'">'.$player_name.'</option>';
												}
											}
										}
									?>
								</select>
							</div>

						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label">Player In: </label>
							
							<div class="col-lg-7">
								<select class="form-control" name="away_player_in">
									<?php
										if($team_details_away_in->num_rows() > 0)
										{
											$away_in = $team_details_away_in->result();
											
											foreach($away_in as $away_res)
											{
												$player_id = $res->player_id;
												$player_fname = $res->player_fname;
												$player_onames = $res->player_onames;
												$player_name = $player_fname.' '.$player_onames;
												
												if($player_id == $away_player_in)
												{
													echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
												}
												
												else
												{
													echo '<option value="'.$player_id.'">'.$player_name.'</option>';
												}
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="row" style="margin-top:10px;">
							<div class="form-actions center-align">
								<button class="submit btn btn-danger" type="submit">
									Add Away Substitute
								</button>
							</div>
						</div>
						<?php echo form_close();?>
					</div>
					
				</section>
	
			</div>
		</div>
	
	</div>
</section>