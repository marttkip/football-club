<?php
$fixture_team_player_id = set_value('fixture_team_player_id');
$foul_player_id = set_value('foul_player_id');
$foul_minute = set_value('foul_minute');
$action_id= set_value('action_id');
$foul_type_id= set_value('foul_type_id');
$goal_minute = set_value('goal_minute');
$goul_type_id = set_value('goul_type_id');
$result = '';
$foul_result = '';
if($fixture_fouls->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Reason</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($fixture_fouls->result() as $fouls_committed)
	{
		$action_name = $fouls_committed->action_name;
		$foul_type_name = $fouls_committed->foul_type_name;
		$foul_team_name = $fouls_committed->team_name;
		$foul_player_number = $fouls_committed->player_number;
		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$foul_player_name = $player_fname. ' ' .$player_onames;
		$foul_minute_time = $fouls_committed->foul_minute;
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$foul_team_name.'</td>
						<td>'.$foul_player_number.'</td>
						<td>'.$foul_player_name.'</td>
						<td>'.$foul_minute_time.'</td>
						<td>'.$foul_type_name.'</td>
						<td>'.$action_name.'</td>
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}
if($total_fixture_goal->num_rows() > 0)
{
	$count = 0;
	$result.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Goal Type</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($total_fixture_goal->result() as $fixture_goals)
	{
		
		$team_name = $fixture_goals->team_name;
		$player_number = $fixture_goals->player_number;
		$player_fname = $fixture_goals->player_fname;
		$player_onames = $fixture_goals->player_onames;
		$player_name = $player_fname. ' ' .$player_onames;
		$score_minute = $fixture_goals->goal_minute;
		$goal_type = $fixture_goals->goal_type_name;
		$count++;
		
		$result.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$player_number.'</td>
					<td>'.$player_name.'</td>
					<td>'.$score_minute.'</td>
					<td>'.$goal_type.'</td>
				</tr>
				';
	}
	$result .='</tbody>
				</table>';
}
else
{
	$result .= 'No goals scored';
}
foreach ($fixture_detail->result() as $key_fixture) {
    # code...
    $first_half_start_time = $key_fixture->first_half_start_time;
    $first_half_end_time = $key_fixture->first_half_end_time;
    $second_half_start_time = $key_fixture->second_half_start_time;
    $second_half_end_time = $key_fixture->second_half_end_time;
    $first_half_score = 0;
    $second_half_score = 0;
    $penalty_scores = 0;
   if($first_half_end_time != NULL )
   {
         $array = explode(':', $first_half_end_time);
         $first_half_minute = $array[1];

         $first_half_score = $this->referee_model->get_first_half_goals($first_half_minute,$fixture_id);
   }
   
   if($second_half_end_time != null)
   {

    $array2 = explode(':', $second_half_end_time);

    $second_half_minute = $array[1];

    $second_half_score = $this->referee_model->get_second_half_goals($first_half_minute,$fixture_id,$second_half_minute);
    
    $penalty_scores = $this->referee_model->get_penalties_goals($fixture_id,$second_half_minute);

   }
   

}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
        <div class="pull-right"  style="margin-top: -25px;">
            <a href="<?php echo base_url().'referee-postings';?>" class="btn  btn-sm btn-info fa fa-arrow-left"> Back</a>
        </div>
    </header>
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-3">
                <button type="button" class="btn btn-success btn-sm col-md-12"  data-toggle="modal" data-target="#add_scores_items">
                     Add Score
                    </button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-warning btn-sm col-md-12"  data-toggle="modal" data-target="#add_faul_items">
                     Add Caution
                    </button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-default btn-sm col-md-12"  data-toggle="modal" data-target="#add_match_timing">
                     Match Timings
                    </button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-primary btn-sm col-md-12"  data-toggle="modal" data-target="#add_scores">
                     Scores
                    </button>
            </div>
            
        </div>
         <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
       
    </div>
    <div class="modal fade" id="add_scores_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Score</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer_management/referee/add_fixture_goal/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Player: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="fixture_team_player_id">
                                        <?php
                                            if($goal_scorer->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Scorer--" selected>--Select Scorer--</option>';
                                                foreach($goal_scorer->result() as $res)
                                                {
                                                    $db_fixture_team_player_id = $res->fixture_team_player_id;
                                                    $player_fname = $res->player_fname;
                                                    $player_onames = $res->player_onames;
                                                    $player_name = $player_fname. ' ' .$player_onames;
                                                    
                                                    if($db_fixture_team_player_id == $fixture_team_player_id)
                                                    {
                                                        echo '<option value="'.$db_fixture_team_player_id.'" selected>'.$player_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_fixture_team_player_id.'">'.$player_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Goal Type: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="goal_type_id">
                                        <?php
                                            if($goul_types->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Scorer--" selected>--Select Goal Type--</option>';
                                                foreach($goul_types->result() as $res)
                                                {
                                                    $db_goul_type_id = $res->goal_type_id;
                                                    $goul_type_name = $res->goal_type_name;
                                                    
                                                    if($db_goul_type_id == $goal_type_id)
                                                    {
                                                        echo '<option value="'.$db_goul_type_id.'" selected>'.$goul_type_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_goul_type_id.'">'.$goul_type_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Score Minute </label>
                                
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="goal_minute" placeholder="Minute" value="<?php echo $goal_minute;?>">
                                </div>
                            </div>
                        
                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-12">
                                    <div class="form-actions center-align">
                                        <button class="submit btn btn-success" type="submit">
                                            Submit Score
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_faul_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Fauls</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer_management/referee/add_fixture_foul/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
                    <div class="row">
                        <div class="col-md-12">
                        
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Player: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="foul_player_id">
                                        <?php
                                            if($goal_scorer->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Offender--" selected>--Select Offender--</option>';
                                                foreach($goal_scorer->result() as $res)
                                                {
                                                    $db_fixture_team_player_id = $res->fixture_team_player_id;
                                                    $player_fname = $res->player_fname;
                                                    $player_onames = $res->player_onames;
                                                    $player_name = $player_fname. ' ' .$player_onames;
                                                    
                                                    if($db_fixture_team_player_id == $fixture_team_player_id)
                                                    {
                                                        echo '<option value="'.$db_fixture_team_player_id.'" selected>'.$player_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_fixture_team_player_id.'">'.$player_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Foul type: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="foul_type_id">
                                        <?php
                                            if($foul_types->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Offense--" selected>--Select Offense-</option>';
                                                foreach($foul_types->result() as $fouls)
                                                {
                                                    $db_foul_type_id = $fouls->foul_type_id;
                                                    $foul_type_name = $fouls->foul_type_name;
                                                    
                                                    if($db_foul_type_id == $foul_type_id)
                                                    {
                                                        echo '<option value="'.$db_foul_type_id.'" selected>'.$foul_type_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_foul_type_id.'">'.$foul_type_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Foul Minute </label>
                                
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="foul_minute" placeholder="Minute" value="<?php echo $foul_minute;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Action: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="action_id">
                                        <?php
                                            if($action_types->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Action--" selected>--Select Action--</option>';
                                                foreach($action_types->result() as $actions)
                                                {
                                                    $db_action_id = $actions->action_id;
                                                    $action_name = $actions->action_name;
                                                    
                                                    if($db_action_id == $action_id)
                                                    {
                                                        echo '<option value="'.$db_action_id.'" selected>'.$action_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_action_id.'">'.$action_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-12">
                                    <div class="form-actions center-align">
                                        <button class="submit btn btn-danger" type="submit">
                                            Add caution
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_match_timing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Match Timings</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer_management/referee/add_fixture_timing/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>First Half Timings</h5>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Start time : </label>
                            
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="08:00:00" name="first_half_start_time" value="<?php echo $first_half_start_time;?>" >
                                    </div>
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label class="col-lg-4 control-label">End time : </label>
                                
                                <div class="col-lg-8">      
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="08:45:00"  name="first_half_end_time" value="<?php echo $first_half_end_time;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Second Half Timings</h5>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Start time : </label>
                            
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="09:00:00"  name="second_half_start_time" value="<?php echo $second_half_start_time;?>">
                                    </div>
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label class="col-lg-4 control-label">End time : </label>
                                
                                <div class="col-lg-8">      
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="09:45:00"  name="second_half_end_time" value="<?php echo $second_half_end_time;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                            <div class="form-actions center-align">
                                <button class="submit btn btn-success" type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>

        <div class="row" style="margin-bottom:20px;">
            <div class="modal fade" id="add_scores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Scores</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4"><label class="col-lg-12 control-label">Team</label>
                                </div>
                                <div class="col-md-4"><label class="col-lg-12 control-label">Timings</label>
                                </div>
                                <div class="col-md-4"><label class="col-lg-12 control-label">Scores</label>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                               <p><strong>Home:</strong> <?php echo $home_team;?></p>
                                               <p><strong> Away:</strong> <?php echo $away_team;?></p>

                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                            <p><strong> First Half :</strong> <?php echo $first_half_start_time.' - '.$first_half_end_time;?></p>
                                            <p><strong> Second Half:</strong> <?php echo $second_half_start_time.' - '.$second_half_end_time;?></p>
                                            <p><strong> Penalties:</strong></p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <p><?php echo $first_half_score;?></p>
                                                <p><?php echo $second_half_score;?></p>
                                                <p><?php echo $penalty_scores;?></p>
                                            </div>
                                        </div>
                                       
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Goals</h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $result;?>
        </div> 
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Fouls</h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $foul_result;?>
        </div> 
    </div>
</section> 