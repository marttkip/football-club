<?php
$result = '';
$foul_result = '';
if($team_details->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>No</th>
						<th>Name</th>
						<th>Licence Number</th>
						
					</tr>
				</thead>
				  <tbody>
				';
	foreach($team_details->result() as $fouls_committed)
	{

		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$player_name = $player_fname.' '.$player_onames;

		$licence_number = $fouls_committed->licence_number;
		$player_number = $fouls_committed->player_number;
		
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_number.'</td>
						<td>'.$player_name.'</td>
						<td>'.$licence_number.'</td>
						
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}

?>


<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Players</h2>
    </header>
    <div class="panel-body" onLoad="window.print();return false;">
    	<div class="table-responsive">
            <?php echo $foul_result;?>
        </div> 
    </div>
</section> 