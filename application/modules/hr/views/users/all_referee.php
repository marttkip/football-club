<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'soccer-management/referee/referee_number/'.$order_method.'/'.$page.'">Referee number</a></th>
						<th><a href="'.site_url().'soccer-management/referee/referee_onames/'.$order_method.'/'.$page.'">Other names</a></th>
						<th><a href="'.site_url().'soccer-management/referee/referee_fname/'.$order_method.'/'.$page.'">First name</a></th>
						<th><a href="'.site_url().'soccer-management/referee/referee_phone/'.$order_method.'/'.$page.'">Phone number</a></th>
						<th><a href="'.site_url().'soccer-management/referee/referee_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$referee_id = $row->referee_id;
				$referee_fname = $row->referee_fname;
				$referee_onames = $row->referee_onames;
				$referee_phone = $row->referee_phone;
				$referee_email = $row->referee_email;
				$referee_number = $row->referee_number;
				$referee_status = $row->referee_status;
				$referee_name = $referee_fname.' '.$referee_onames;
				
				//status
				if($referee_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($referee_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-referee/'.$referee_id.'" onclick="return confirm(\'Do you want to activate '.$referee_name.'?\');" title="Activate '.$referee_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($referee_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-referee/'.$referee_id.'" onclick="return confirm(\'Do you want to deactivate '.$referee_name.'?\');" title="Deactivate '.$referee_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				//get team
				$team = '';
				if($teams->num_rows() > 0)
				{
					foreach($teams->result() as $res)
					{
						$team_id2 = $res->team_id;
						/*if($team_id == $team_id2)
						{
							$team = $res->team_name;
						}*/
					}
				}
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$referee_number.'</td>
						<td>'.$referee_onames.'</td>
						<td>'.$referee_fname.'</td>
						<td>'.$referee_phone.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'soccer-management/edit-referee/'.$referee_id.'" class="btn btn-sm btn-success" title="Edit '.$referee_name.'"><i class="fa fa-pencil"></i>Edit Referee</a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'soccer-management/delete-referee/'.$referee_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$referee_name.'?\');" title="Delete '.$referee_name.'"><i class="fa fa-trash"> Delete</i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no referee";
		}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search referee</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("soccer_management/referee/search_referee", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Fixture: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="team_id">
                            	<option value="">---Select Fixture---</option>
                                <?php
                                    if($teams->num_rows() > 0){
                                        foreach($teams->result() as $row):
                                            $team_name = $row->team_name;
                                            $team_id= $row->team_id;
                                            ?><option value="<?php echo $team_id; ?>" ><?php echo $team_name; ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Referee Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="referee_number" placeholder="Referee number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">First name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="referee_fname" placeholder="First name">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Other names: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="referee_onames" placeholder="Other names">
                        </div>
                    </div>
            
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4">
                        	<div class="center-align">
                            	<button type="submit" class="btn btn-info btn-sm">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
							</header>
							<div class="panel-body">
                            	<?php
								$search = $this->session->userdata('referee_search_title2');
								
								if(!empty($search))
								{
									echo '<h6>Filtered by: '.$search.'</h6>';
									echo '<a href="'.site_url().'soccer_management/referee/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
								}
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>soccer-management/export-referee" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    	<a href="<?php echo site_url();?>soccer-management/add-referee" class="btn btn-sm btn-info pull-right">Add Referee</a>
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>