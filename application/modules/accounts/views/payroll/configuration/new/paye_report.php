<?php
$personnel_id = $this->session->userdata('personnel_id');
$prepared_by = $this->session->userdata('first_name');
$roll = $payroll->row();
$year = $roll->payroll_year;
$month = $roll->month_id;
$totals = array();

//$query = retrieval of all payroll personnel
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result = 
	'
	<table class="table table-bordered table-striped table-condensed" id ="customers">
		<thead>
			<tr>
				<th>#</th>
				<th>Ref</th>
				<th>Personnel</th>
				<th>Pin</th>
				<th>Chargeable Pay</th>
				<th>PAYE</th>
				
				';
	$total = 'total_';
	$total_number_of_payments = 3;
	$total_gross = 0;
	$total_paye = 0;
	$total_nssf = 0;
	$total_nhif = 0;
	$total_life_ins = 0;
	$total_allowances = 0;
	$total_savings = 0;
	$total_loans = 0;
	$total_schemes = 0;
	$total_net = 0;
	$total_personnel_benefits = $total_personnel_payments = $total_personnel_allowances = $total_personnel_deductions = $total_personnel_other_deductions = array();
	
	$benefits_amount = $payroll_data->benefits;
	$total_benefits = $payroll_data->total_benefits;
	$payments_amount = $payroll_data->payments;
	$total_payments = $payroll_data->total_payments;
	$allowances_amount = $payroll_data->allowances;
	$total_allowances = $payroll_data->total_allowances;
	$deductions_amount = $payroll_data->deductions;
	$total_deductions = $payroll_data->total_deductions;
	$other_deductions_amount = $payroll_data->other_deductions;
	$total_other_deductions = $payroll_data->total_other_deductions;
	$nssf_amount = $payroll_data->nssf;
	$nhif_amount = $payroll_data->nhif;
	$life_ins_amount = $payroll_data->life_ins;
	$paye_amount = $payroll_data->paye;
	$monthly_relief_amount = $payroll_data->monthly_relief;
	$insurance_relief_amount = $payroll_data->insurance_relief;
	$insurance_amount_amount = $payroll_data->insurance;
	$scheme = $payroll_data->scheme;
	$savings = $payroll_data->savings;
	
	//payments
	if($payments->num_rows() > 0)
	{
		foreach($payments->result() as $res)
		{
			$payment_abbr = $res->payment_name;
			$payment_id = $res->payment_id;
			$total_payment_amount[$payment_id] = 0;
			if(isset($total_payments->$payment_id))
			{
				$total_payment_amount[$payment_id] = $total_payments->$payment_id;
			}
		}
	}
	
	//benefits
	if($benefits->num_rows() > 0)
	{
		foreach($benefits->result() as $res)
		{
			$benefit_abbr = $res->benefit_name;
			$benefit_id = $res->benefit_id;
				
			$total_benefit_amount[$benefit_id] = 0;
			if(isset($total_benefits->$benefit_id))
			{
				$total_benefit_amount[$benefit_id] = $total_benefits->$benefit_id;
			}
		}
	}
	
	//allowances
	if($allowances->num_rows() > 0)
	{
		foreach($allowances->result() as $res)
		{
			$allowance_abbr = $res->allowance_name;
			$allowance_id = $res->allowance_id;
				
			$total_allowance_amount[$allowance_id] = 0;
			if(isset($total_allowances->$allowance_id))
			{
				$total_allowance_amount[$allowance_id] = $total_allowances->$allowance_id;
			}
		}
	}
	
	$result .= '
			</tr>
		</thead>
		<tbody>
	';
	
	foreach ($query->result() as $row)
	{
		$personnel_id = $row->personnel_id;
		$personnel_number = $row->personnel_number;
		$personnel_fname = $row->personnel_fname;
		$personnel_onames = $row->personnel_onames;
		$personnel_kra_pin = $row->personnel_kra_pin;
		$staff_id = $row->staff_id;
		$engagement_date = $row->engagement_date;
		$date_of_exit = $row->date_of_exit;
		$cost_center = $row->cost_center;
		$gross = 0;
		
		$table_id = 0;
		
		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$personnel_number.'</td>
				<td>'.$personnel_onames.' '.$personnel_fname.'</td>
				<td>'.$personnel_kra_pin.'</td>
		';
		
		//payments
		if($payments->num_rows() > 0)
		{
			foreach($payments->result() as $res)
			{
				$payment_id = $res->payment_id;
				$payment_abbr = $res->payment_name;
				$table_id = $payment_id;
				
				if($total_payment_amount[$payment_id] != 0)
				{
					$payment_amt = 0;
					if(isset($payments_amount->$personnel_id->$table_id))
					{
						$payment_amt = $payments_amount->$personnel_id->$table_id;
					}
					$gross += $payment_amt;
					if(!isset($total_personnel_payments[$payment_id]))
					{
						$total_personnel_payments[$payment_id] = 0;
					}
					$total_personnel_payments[$payment_id] += $payment_amt;
				}
			}
		}
		
		//benefits
		$total_benefits = 0;
		if($benefits->num_rows() > 0)
		{
			foreach($benefits->result() as $res)
			{
				$benefit_id = $res->benefit_id;
				$benefit_name = $res->benefit_name;
				$table_id = $benefit_id;
								
				if($total_benefit_amount[$benefit_id] != 0)
				{
					$benefit_amt = 0;
					if(isset($benefits_amount->$personnel_id->$table_id))
					{
						$benefit_amt = $benefits_amount->$personnel_id->$table_id;
					}
					$total_benefits += $benefit_amt;
					if(!isset($total_personnel_benefits[$benefit_id]))
					{
						$total_personnel_benefits[$benefit_id] = 0;
					}
					$total_personnel_benefits[$benefit_id] += $benefit_amt;
				}
			}
		}
		
		//allowances
		if($allowances->num_rows() > 0)
		{
			foreach($allowances->result() as $res)
			{
				$allowance_id = $res->allowance_id;
				$allowance_name = $res->allowance_name;
				$table_id = $allowance_id;
				
				if($total_allowance_amount[$allowance_id] != 0)
				{
					$allowance_amt = 0;
					if(isset( $allowances_amount->$personnel_id->$table_id))
					{
						$allowance_amt =  $allowances_amount->$personnel_id->$table_id;
					}
					$gross += $allowance_amt;
					if(!isset($total_personnel_allowances[$allowance_id]))
					{
						$total_personnel_allowances[$allowance_id] = 0;
					}
					$total_personnel_allowances[$allowance_id] += $allowance_amt;
				}
			}
		}
		
		$result .= '<td>'.number_format($gross, 2).'</td>';
		$total_gross += $gross;
		
		/*
			--------------------------------------------------------------------------------------
			Select & display untaxable deductions for the personnel
			--------------------------------------------------------------------------------------
		*/
		
		//nssf
		$nssf = $nssf_amount->$personnel_id;
		$total_nssf += $nssf;
		
		//nhif
		$nhif = $nhif_amount->$personnel_id;
		$total_nhif += $nhif;
		
		//paye
		$paye =$paye_amount->$personnel_id;
		
		//relief
		$relief = $monthly_relief_amount->$personnel_id;
		
		//insurance_relief
		$insurance_relief = $insurance_relief_amount->$personnel_id;
		
		//relief
		$insurance_amount = $insurance_amount_amount->$personnel_id;
		//echo $insurance_relief;
		$paye -= ($relief + $insurance_relief);
						
		if($paye < 0)
		{
			$paye = 0;
		}
		$total_paye += $paye;
		$total_life_ins += $insurance_amount;
		$result .= 
		'
				<td>'.number_format($paye, 2).'</td>
		';
		
		$result .= 
		'
			</tr> 
		';
	}
	
	$result .= '
			<tr> 
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<th>'.number_format($total_gross, 2, '.', ',').'</th>
				<th>'.number_format($total_paye, 2, '.', ',').'</th>
			</tr>
		</tbody>
	</table>
	';
}

else
{
	$result = "There are no personnel";
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>PAYE REPORT</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
        </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
    	<div class="col-md-12 center-align receipt_bottom_border">
    		<table class="table table-condensed">
                <tr>
                    <th><?php echo $child_branch_name;?> PAYE Report for The month of <?php echo date('M Y',strtotime($year.'-'.$month));?></th>
                    <th class="align-right">
						<?php echo $branch_name;?><br/>
                        <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                        E-mail: <?php echo $branch_email;?><br/>
                        Tel : <?php echo $branch_phone;?><br/>
                        <?php echo $branch_location;?>
                    </th>
                    <th>
                        <img src="<?php echo base_url().'assets/logo/'.$branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo"/>
                    </th>
                </tr>
            </table>
        </div>
    <!--<body class="receipt_spacing">
    	<div class="row" >
        	<img src="<?php echo base_url().'assets/logo/'.$branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo"/>
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $branch_name;?><br/>
                    <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                    E-mail: <?php echo $branch_email;?>. Tel : <?php echo $branch_phone;?><br/>
                    <?php echo $branch_location;?><br/>
                </strong>
            </div>
        </div>
        
      	<div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<h4><?php echo '<h3>Payroll for The month of '.date('M Y',strtotime($year.'-'.$month)).'</h3>';?></h4>
            </div>
        </div>-->
        
        <div class="row receipt_bottom_border" >
        	<div class="col-md-12">
            	<?php echo $result;?>
            </div>
        	<div class="col-md-12 center-align">
            	<?php echo 'Prepared By:	'.$prepared_by.' '.date('jS M Y H:i:s',strtotime(date('Y-m-d H:i:s')));?>
            </div>
        </div>
		<a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">XLS</a>
<!--<a href="#" onClick ="$('#customers').tableExport({type:'csv',escape:'false'});">CSV</a>
<a href="#" onClick ="$('#customers').tableExport({type:'pdf',escape:'false'});">PDF</a>-->

    </body>
</html>