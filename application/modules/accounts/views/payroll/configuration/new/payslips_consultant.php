
<?php
//$personnel_id = $this->session->userdata('personnel_id');
$created_by = $this->session->userdata('first_name');

?>

<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
		.receipt_spacing{letter-spacing:0px; font-size: 14px;}
		.center-align{margin:0 auto; text-align:center;}
		
		.receipt_bottom_border{border-bottom: #888888 medium solid;}
		.row .col-md-12 table {
			border:solid #000 !important;
			border-width:0px 0 0 1px !important;
			font-size:12px;
		}
		.row .col-md-12 th, .row .col-md-12 td {
			border:solid #000 !important;
			border-width:0 1px 1px 0 !important;
		}
		.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
		.title-img{float:left; padding-left:30px;}
		img.logo{max-height:70px; margin:0 auto;}
		.col-xs-6{text-align:right;}
		.col-xs-6#align-left{text-align:left !important; padding-left: 0px !important;}
		.row {
    margin-left: 5px !important;
    margin-right: 5px !important;
}
		
	</style>
    <head>
        <title>Payslip for <?php echo $personnel_name;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
    </head>
    <body class="receipt_spacing">
        <div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
				<div class="row">
					<strong>
                    	<img src="<?php echo base_url();?>assets/logo/<?php echo $branch_image_name;?>" alt="<?php echo $child_branch_name;?>" class="img-responsive logo" style="float:right;"/>
						<?php echo $child_branch_name;?><br/>
					</strong>
                    <h5><strong>Payslip Preview</strong></h5>
					<h4><?php echo strtoupper($personnel_name) ?></br></h4>
					<h5>STAFF NUMBER. <?php echo  $personnel_number ?></br>
					KRA PIN: <?php echo  $kra_pin?></br></h5>
				</div>
                <div class="row">
                	<strong>EARNINGS</strong>
                </div>
                <div class="row">
                    <?php 
                    /*********** Payments ***********/
                    $total_payments = 0;
                                                                
                    if($payments->num_rows() > 0)
                    {
                        foreach ($payments->result() as $row2)
                        {
                            $payment_id = $row2->payment_id;
                            $payment_name = $row2->payment_name;
                            
                            if($personel_payments->num_rows() > 0){
                                foreach($personel_payments->result() as $allow){
                                    $id = $allow->id;
                                    
                                    if($id == $payment_id)
                                    {
                                        $amount = $allow->amount;
										if($amount != 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo  strtoupper($payment_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
                                    }
                            
                                    else{
                                        $amount = 0;
                                    }
                                }
                            }
                            
                            else{
                                $amount = 0;
                            }
                            $total_payments += $amount;
                        }
                    }
        
                    ?>
                </div>
                
                <?php
                    
                    /*********** Taxable ***********/
					$taxable = 0;
					$paye = 0;
					$taxable = $total_payments;
					
					$paye = $this->payroll_model->calculate_paye($taxable, $personnel_type_id);
                ?>
                
                <!-- Taxable -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        <strong>TOTAL EARNINGS</strong>
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format(($taxable), 2);?>
                    </div>
                </div>
				
				<div class="row">
                	 <strong>Withholding Tax </strong>
                </div>
				
                <!-- End NSSF -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        CHARGEABLE AMT KSHS
                    </div>
                    
                    <div class="col-xs-6">
                       <?php echo number_format(($taxable), 2);?>
                    </div>
                </div>
                
                 <div class="row">
                	<div class="col-xs-6" id="align-left">
                        TAX CHARGED
                    </div>
                    
                    <div class="col-xs-6">
                       <?php echo number_format(($paye), 2);?>
                    </div>
                </div>
				<?php
				$net_pay = $taxable - $paye;
                ?>
                
                <div class="row">
                	 <strong>DEDUCTIONS </strong>
                </div>
                
                <div class="row">
                <?php
					
                    
                    /*********** Other deductions ***********/
					$total_other_deductions = 0;
											
					if($other_deductions->num_rows() > 0)
					{
						foreach ($other_deductions->result() as $row2)
						{
							$other_deduction_id = $row2->other_deduction_id;
							$other_deduction_name = $row2->other_deduction_name;
							$other_deduction_taxable = $row2->other_deduction_taxable;
							
							if($personnel_other_deductions->num_rows() > 0){
								foreach($personnel_other_deductions->result() as $allow){
									$id = $allow->id;
									
									if($id == $other_deduction_id){
										$amount = $allow->amount;
										if($amount > 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo strtoupper($other_deduction_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
									}
							
									else{
										$amount = 0;
									}
								}
							}
							
							else{
								$amount = 0;
							}
							$total_other_deductions += $amount;
						}
					} ?>
                </div>
                				
                <!-- Net pay -->		
                <div class="row">
                    <div class="col-xs-6" id="align-left">
                        <strong>NET PAY</strong>
                    </div>
                    
                    <div class="col-xs-6">
                        <?php 
						$net_pay -= $total_other_deductions;
						if($net_pay < 0)
						{	
							$net_pay = 0;
						}
						echo number_format($net_pay, 2);?>
                    </div>
                </div>
                <!-- End Net -->
               
            </div>
        </div>
        
    </body>
</html>
