<?php
$personnel_id = $this->session->userdata('personnel_id');
$prepared_by = $this->session->userdata('first_name');
$roll = $payroll->row();
$year = $roll->payroll_year;
$month = $roll->month_id;
$totals = array();
//$query = retrieval of all payroll personnel
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$total = 'total_';
	$total_number_of_payments = 0;
	$total_gross = 0;
	$total_paye = 0;
	$total_nssf = 0;
	$total_nhif = 0;
	$total_life_ins = 0;
	$total_allowances = 0;
	$total_savings = 0;
	$total_loans = 0;
	$total_schemes = 0;
	$total_net = 0;
	$total_personnel_benefits = $total_personnel_payments = $total_personnel_allowances = $total_personnel_deductions = $total_personnel_other_deductions = array();
	
	$benefits_amount = $payroll_data->benefits;
	$total_benefits = $payroll_data->total_benefits;
	$payments_amount = $payroll_data->payments;
	$total_payments = $payroll_data->total_payments;
	$allowances_amount = $payroll_data->allowances;
	$total_allowances = $payroll_data->total_allowances;
	$deductions_amount = $payroll_data->deductions;
	$total_deductions = $payroll_data->total_deductions;
	$other_deductions_amount = $payroll_data->other_deductions;
	$total_other_deductions2 = $payroll_data->total_other_deductions;
	$nssf_amount = $payroll_data->nssf;
	$nhif_amount = $payroll_data->nhif;
	$life_ins_amount = $payroll_data->life_ins;
	$paye_amount = $payroll_data->paye;
	$monthly_relief_amount = $payroll_data->monthly_relief;
	$insurance_relief_amount = $payroll_data->insurance_relief;
	$insurance_amount_amount = $payroll_data->insurance;
	$scheme = $payroll_data->scheme;
	$total_scheme = $payroll_data->total_scheme;
	$savings = $payroll_data->savings;
	$result = '';
	$all_deductions = $gross = 0;
	
	$result = 
	'
	<table class="table table-bordered table-striped table-condensed" id ="customers" center-align>
		
				';
	//payments
	if($payments->num_rows() > 0)
	{
		foreach($payments->result() as $res)
		{
			$payment_abbr = $res->payment_name;
			$payment_id = $res->payment_id;
			$total_payment_amount[$payment_id] = 0;

			if(isset($total_payments->$payment_id))
			{
				$total_payment_amount[$payment_id] = $total_payments->$payment_id;
				$gross += $total_payment_amount[$payment_id];
			}
			
			if($total_payment_amount[$payment_id] != 0)
			{
				$result .= '
				<tr>
					<th>'.$payment_abbr.'</th>
					<th>'.number_format($total_payment_amount[$payment_id], 2).'</th>
				</tr>';
			}
		}
	}
	
	//benefits
	if($benefits->num_rows() > 0)
	{
		foreach($benefits->result() as $res)
		{
			$benefit_abbr = $res->benefit_name;
			$benefit_id = $res->benefit_id;
			$total_benefit_amount[$benefit_id] = 0;
			if(isset($total_benefits->$benefit_id))
			{
				$total_benefit_amount[$benefit_id] = $total_benefits->$benefit_id;
				$gross += $total_benefit_amount[$benefit_id];
			}
			
			if($total_benefit_amount[$benefit_id] != 0)
			{
				$result .= '
				<tr>
					<th>'.$benefit_abbr.'</th>
					<th>'.number_format($total_benefit_amount[$benefit_id], 2).'</th>
				</tr>';
			}
		}
	}
	
	//allowances
	if($allowances->num_rows() > 0)
	{
		foreach($allowances->result() as $res)
		{
			$allowance_abbr = $res->allowance_name;
			$allowance_id = $res->allowance_id;
			
			$total_allowance_amount[$allowance_id] = 0;
			if(isset($total_allowances->$allowance_id))
			{
				$total_allowance_amount[$allowance_id] = $total_allowances->$allowance_id;
				$gross += $total_allowance_amount[$allowance_id];
			}
			if($total_allowance_amount[$allowance_id] != 0)
			{
				$result .= '
				<tr>
					<th>'.$allowance_abbr.'</th>
					<th>'.number_format($total_allowance_amount[$allowance_id], 2).'</th>
				</tr>';
			}
		}
	}
	
	//gross
	$total_gross = $gross;
		
	
	foreach ($query->result() as $row)
	{
		$personnel_id = $row->personnel_id;
		//nssf
		$nssf = $nssf_amount->$personnel_id;
		$total_nssf += $nssf;
		
		//nhif
		$nhif = $nhif_amount->$personnel_id;
		$total_nhif += $nhif;
		
		//paye
		$paye =$paye_amount->$personnel_id;
		
		//relief
		$relief = $monthly_relief_amount->$personnel_id;
		
		//insurance_relief
		$insurance_relief = $insurance_relief_amount->$personnel_id;
		
		//relief
		$insurance_amount = $insurance_amount_amount->$personnel_id;
		//echo $insurance_relief;
		$paye -= ($relief + $insurance_relief);
						
		if($paye < 0)
		{
			$paye = 0;
		}
		$total_paye += $paye;
		$total_life_ins += $insurance_amount;
	}
	
	
	$result .= '
		<tr>
			<th>Gross</th>
			<th>'.number_format($total_gross, 2, '.', ',').'</th>
		</tr>
		<tr>
			<th>PAYE</th>
			<th>'.number_format($total_paye, 2, '.', ',').'</th>
		</tr>
		<tr>
			<th>NSSF</th>
			<th>'.number_format($total_nssf, 2, '.', ',').'</th>
		</tr>
		<tr>
			<th>NHIF</th>
			<th>'.number_format($total_nhif, 2, '.', ',').'</th>
		</tr>
		<tr>
			<th>Life Ins</th>
			<th>'.number_format($total_life_ins, 2, '.', ',').'</th>
		</tr>
	';
	
	//deductions
	$all_deductions = 0;
	if($deductions->num_rows() > 0)
	{
		foreach($deductions->result() as $res)
		{
			$deduction_name = $res->deduction_name;
			$deduction_abbr = $res->deduction_name;
			$deduction_id = $res->deduction_id;
			$total_deduction_amount[$deduction_id] = 0;
			
			if(isset($total_deductions->$deduction_id))
			{
				$total_deduction_amount[$deduction_id] = $total_deductions->$deduction_id;
				$all_deductions += $total_deduction_amount[$deduction_id];
			}
			
			if($total_deduction_amount[$deduction_id]!= 0)
			{
				$result .= '
				<tr>
					<th>'.$deduction_name.'</th>
					<th>'.number_format($total_deduction_amount[$deduction_id], 2).'</th>
				</tr>';
			}
		}
	}
	
	//other deductions
	if($other_deductions->num_rows() > 0)
	{
		foreach($other_deductions->result() as $res)
		{
			$other_deduction_abbr = $res->other_deduction_name;
			$other_deduction_id = $res->other_deduction_id;
			
			$total_other_deduction_amount[$other_deduction_id] = 0;
			if(isset($total_other_deductions2->$other_deduction_id))
			{
				$total_other_deduction_amount[$other_deduction_id] = $total_other_deductions2->$other_deduction_id;
				$all_deductions += $total_other_deduction_amount[$other_deduction_id];
			}
			
			if($total_other_deduction_amount[$other_deduction_id] != 0)
			{
				$result .= '
				<tr>
					<th>'.$other_deduction_abbr.'</th>
					<th>'.number_format($total_other_deduction_amount[$other_deduction_id], 2).'</th>
				</tr>';
			}
		}
	}
		
		if($rs_schemes->num_rows() > 0)
		{
			foreach($rs_schemes->result() as $res)
			{
				$loan_scheme_name = $res->loan_scheme_name;
				$loan_scheme_id = $res->loan_scheme_id;
					
				$total_loan_scheme_amount[$loan_scheme_id] = 0;
				$table_id = $loan_scheme_id;
				if(isset($total_scheme->$loan_scheme_id))
				{
					$total_loan_scheme_amount[$loan_scheme_id] = $total_scheme->$loan_scheme_id;
				}
				
				if($total_loan_scheme_amount[$loan_scheme_id] != 0)
				{
					$result .= '
					<tr>
						<th>'.$loan_scheme_name.'</th>
						<th>'.number_format($total_loan_scheme_amount[$loan_scheme_id], 2).'</th>
					</tr>';
					
					$total_schemes += $total_loan_scheme_amount[$loan_scheme_id];
				}
			}
		}
		
	//net
	$total_net = $total_gross - ($total_paye + $total_nssf + $total_nhif + $all_deductions + $total_schemes);
	
	$result .= '
				<!--<th>Savings</th>
				<th>Loans</th>-->
				<!--<th>'.number_format($total_savings, 2, '.', ',').'</th>
				<th>'.number_format($total_loans, 2, '.', ',').'</th>-->
			<tr>
				<th>Net pay</th>
				<th>'.number_format($total_net, 2, '.', ',').'</th>
			</tr>
		</table>
	';
}

else
{
	$result = "There are no personnel";
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Payroll Summary</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
				.table-condensed > tbody > tr > th {
				font-size: 15px !important;
				padding-left: 5px !important;
			}
			
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
        </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
    	<div class="col-md-12 center-align receipt_bottom_border">
    		<table class="table table-condensed">
                <tr>
                    <th><?php echo $branch_name;?> Payroll Summary for <?php echo date('M Y',strtotime($year.'-'.$month));?></th>
                    <th class="align-right">
						<?php echo $branch_name;?><br/>
                        <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                        E-mail: <?php echo $branch_email;?><br/>
                        Tel : <?php echo $branch_phone;?><br/>
                        <?php echo $branch_location;?>
                    </th>
                    <th>
                        <img src="<?php echo base_url().'assets/logo/'.$branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo"/>
                    </th>
                </tr>
            </table>
        </div>
    <!--<body class="receipt_spacing">
    	<div class="row" >
        	<img src="<?php echo base_url().'assets/logo/'.$branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo"/>
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $branch_name;?><br/>
                    <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                    E-mail: <?php echo $branch_email;?>. Tel : <?php echo $branch_phone;?><br/>
                    <?php echo $branch_location;?><br/>
                </strong>
            </div>
        </div>
        
      	<div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<h4><?php echo '<h3>Payroll for The month of '.date('M Y',strtotime($year.'-'.$month)).'</h3>';?></h4>
            </div>
        </div>-->
        
        <div class="row receipt_bottom_border" >
        	<div class="col-md-12">
            	<?php echo $result;?>
            </div>
        	<div class="col-md-12 center-align">
            	<?php echo 'Prepared By:	'.$prepared_by.' '.date('jS M Y H:i:s',strtotime(date('Y-m-d H:i:s')));?>
            </div>
        </div>
		<a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">XLS</a>
<!--<a href="#" onClick ="$('#customers').tableExport({type:'csv',escape:'false'});">CSV</a>
<a href="#" onClick ="$('#customers').tableExport({type:'pdf',escape:'false'});">PDF</a>-->

    </body>
</html>
