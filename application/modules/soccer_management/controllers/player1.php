<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Player extends soccer_management 
{
	var $document_upload_path;
	var $document_upload_location;
	var $csv_path;
	
	function __construct()
	{
		parent:: __construct();
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->document_upload_path = realpath(APPPATH . '../assets/document_uploads');
		$this->document_upload_location = base_url().'assets/document_uploads/';
	}
	
	//show all playeres
	public function index($order = 'player_id', $order_method = 'ASC') 
	{
		$where = 'player_id > 0 AND deleted = 0';
		$table = 'player';
		$player_search = $this->session->userdata('player_search2');
		
		if(!empty($player_search))
		{
			$where .= $player_search;
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/player/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->player_model->get_all_player($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Player';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('player/all_player', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function close_search()
	{
		$this->session->unset_userdata('player_search2', $search);
		$this->session->unset_userdata('player_search_title2', $search_title);
		
		redirect('soccer-management/player');
	}
	public function search_player()
	{
		$player_number = $this->input->post('player_number');
		$team_id = $this->input->post('team_id');
		$search_title = '';
		
		/*if(!empty($player_number))
		{
			$search_title .= ' member number <strong>'.$player_number.'</strong>';
			$player_number = ' AND player.player_number LIKE \'%'.$player_number.'%\'';
		}*/
		if(!empty($player_number))
		{
			$search_title .= ' player number <strong>'.$player_number.'</strong>';
			$player_number = ' AND player.player_number = \''.$player_number.'\'';
		}
		
		if(!empty($team_id))
		{
			$search_title .= ' team id <strong>'.$team_id.'</strong>';
			$team_id = ' AND player.team_id = \''.$team_id.'\' ';
		}
		
		//search surname
		if(!empty($_POST['player_fname']))
		{
			$search_title .= ' first name <strong>'.$_POST['player_fname'].'</strong>';
			$surnames = explode(" ",$_POST['player_fname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' player.player_fname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\'';
				}
				
				else
				{
					$surname .= ' player.player_fname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['player_onames']))
		{
			$search_title .= ' other names <strong>'.$_POST['player_onames'].'</strong>';
			$other_names = explode(" ",$_POST['player_onames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' player.player_onames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\'';
				}
				
				else
				{
					$other_name .= ' player.player_onames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $player_number.$team_id.$surname.$other_name;
		$this->session->set_userdata('player_search2', $search);
		$this->session->set_userdata('player_search_title2', $search_title);
		
		$this->index();
	}
	
	public function add_player() 
	{
		//form validation rules
		$this->form_validation->set_rules('team_id', 'Team', 'required|xss_clean');
		$this->form_validation->set_rules('player_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('player_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('player_dob', 'Date of Birth', 'required|xss_clean');
		$this->form_validation->set_rules('player_email', 'Email', 'valid_email|is_unique[player.player_email]|xss_clean');
		$this->form_validation->set_rules('player_phone', 'Phone', 'integer|xss_clean');
		$this->form_validation->set_rules('player_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'xss_clean');
		$this->form_validation->set_rules('player_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('player_number', 'Player number', 'xss_clean');
		$this->form_validation->set_rules('player_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('player_post_code', 'Post code', 'integer|xss_clean');
		$this->form_validation->set_rules('player_national_id_number', 'National ID', 'xss_clean|is_unique[player.player_national_id_number]');
		$this->form_validation->set_rules('engagement_date','Start Date','required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$engagement_date = $this->input->post('engagement_date');
			$player_dob = $this->input->post('player_dob');
			if($engagement_date > $player_dob)
			{
				$player_id = $this->player_model->add_player();
				if($player_id > 0)
				{
					$this->session->set_userdata("success_message", "Player added successfully");
					redirect('soccer-management/player/');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not add player. Please try again ".$player_id);
				}
			}
			
		}
		
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$data['title'] = 'Add player';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('player/add_player', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_player($player_id)
    {
		$this->form_validation->set_rules('team_id', 'Team', 'required|xss_clean');
    	$this->form_validation->set_rules('player_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('player_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('player_dob', 'Date of Birth', 'xss_clean');
		// $this->form_validation->set_rules('player_email', 'Email', 'valid_email|is_unique[player.player_email]|xss_clean');
		$this->form_validation->set_rules('player_phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('player_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'xss_clean');
		$this->form_validation->set_rules('player_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('player_number', 'Player number', 'xss_clean');
		$this->form_validation->set_rules('player_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('player_post_code', 'Post code', 'xss_clean');
		$this->form_validation->set_rules('player_national_id_number', 'ID number', 'xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->player_model->edit_player($player_id))
			{
				$this->session->set_userdata('success_message', 'player\'s general details updated successfully');
				redirect('soccer-management/player/');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update player\'s general details. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add player. Please try again");
		}
		$v_data['player_other_documents'] = $this->player_model->get_player_uploads($player_id);
		$v_data['player'] = $this->player_model->get_player($player_id);
		$v_data['player_id'] = $player_id;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$v_data['title'] = 'Edit Player';
		$data['content'] = $this->load->view('player/edit_player', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
    }
	public function activate_player($player_id)
	{
		$this->player_model->activate_player($player_id);
		$this->session->set_userdata('success_message', 'Player activated successfully');
		redirect('soccer-management/player');
	}
	public function deactivate_player($player_id)
	{
		$this->player_model->deactivate_player($player_id);
		$this->session->set_userdata('success_message', 'Player disabled successfully');
		redirect('soccer-management/player');
	}
	public function delete_player($player_id)
	{
		if($this->player_model->delete_player($player_id))
		{
			$this->session->set_userdata('success_message', 'Player has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Player could not deleted');
		}
		redirect('soccer-management/player');
	}
	public function upload_player_documents($player_id) 
	{
		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';
		
		//upload image if it has been selected
		$response = $this->personnel_model->upload_any_file($this->document_upload_path, $this->document_upload_location, $document_name, 'document_scan');
		if($response)
		{
			$document_upload_location = $this->document_upload_location.$this->session->userdata($document_name);
		}
		
		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);
		$this->form_validation->set_rules('document_item_name', 'Document Name', 'xss_clean');
		//$this->form_validation->set_rules('document_type_id', 'Document Type', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{

			if($this->player_model->upload_personnel_documents($player_id, $document))
			{
				$this->session->set_userdata('success_message', 'Document uploaded successfully');
				$this->session->unset_userdata($document_name);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
		}
		
		redirect('soccer-management/edit-player/'.$player_id);
	}
	public function import_players()
	{
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		$v_data['teams'] = $this->team_model->all_teams();
		$data['content'] = $this->load->view('player/import_player', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function player_template()
	{
		$this->player_model->player_template();
	}
	
	public function do_player_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->player_model->import_csv_players($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		$v_data['teams'] = $this->team_model->all_teams();
		$data['content'] = $this->load->view('player/import_player', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	
	}
}
?>