<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Configuration extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('soccer_management/configuration_model');
	}
	public function index()
	{
		// var_dump('asdasd');die();
		$v_data['referee_payments'] = $this->configuration_model->get_referee_payments();

		$v_data['referee_types'] = $this->fixture_model->get_fixture_referee_type();

		$v_data['title'] = $data['title'] = 'Soccer Configuration';


		$data['content'] = $this->load->view('configuration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_ref_payment()
	{
		//form validation
		$this->form_validation->set_rules('payment_amount', 'Referee Payment Amount', 'required|xss_clean');
		$this->form_validation->set_rules('referee_type_id', 'Referee type', 'required|xss_clean');
		
		$referee_type_id  =$this->input->post('referee_type_id');
		if ($this->form_validation->run())
		{
			if($this->configuration_model->referee_payment_exists_not($referee_type_id))
			{
				$ref_payment_id = $this->configuration_model->add_referee_payment($referee_type_id);
				if($ref_payment_id > 0)
				{
					$this->session->set_userdata("success_message", "Ref payment added successfully");
					redirect('soccer-management/configuration');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not add referee payment. Please try again ".$ref_payment_id);
				}
			}
			else
			{
				$this->session->set_userdata("error_message","This payment is already captured");
			}
			
		}
		$v_data['referee_payments'] = $this->configuration_model->get_referee_payments();
		$v_data['referee_types'] = $this->fixture_model->get_fixture_referee_type();
		$v_data['title'] = $data['title'] = 'Soccer Configuration';
		$data['content'] = $this->load->view('configuration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function edit_referee_payment($referee_payment_id)	
	{
	//form validation
		
		$this->form_validation->set_rules('payment_amount2', 'Referee Payment Amount', 'required|xss_clean');
		
		
		if ($this->form_validation->run())
		{
			
				if($this->configuration_model->update_referee_payment($referee_payment_id))
				{
					$this->session->set_userdata("success_message", "Ref payment added successfully");
				
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not add referee payment. Please try again ".$ref_payment_id);
				}
		
		}
		redirect('soccer-management/configuration');


	}


}
?>des