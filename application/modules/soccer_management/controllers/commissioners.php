<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Commissioners extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('soccer_management/commissioners_model');
	}
	public function index()
	{
		$personnel_id = $this->session->userdata('personnel_id');
		$personnel_name = $this->session->userdata('first_name');
		
		//get all games to be commissioned by the logged in personnel for leagues and tournaments
		$league_commissioner_games = $this->commissioners_model->get_all_commissioner_games($personnel_id);
		$tournament_commissioner_games = $this->commissioners_model->get_all_tournament_commissioner_games($personnel_id);
		
		$v_data['title'] = $data['title'] = "Commissioner ".$personnel_name.' Fixtures';
		$v_data['league_commissioner_games'] = $league_commissioner_games;
		$v_data['tournament_commissioner_games'] = $tournament_commissioner_games;
		$data['content'] = $this->load->view('commissioner_post',$v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_analysis($fixture_id)
	{
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->fixture_model->get_comment_fixtures($fixture_id);
		$v_data['all_assessment_fixtures'] = $this->fixture_model->get_fixture_assessments($fixture_id);
		$v_data['fixture_id'] = $fixture_id;
		$v_data['title'] = $data['title'] = 'Analysis for fixture '.$fixture_id;
		
		$data['content'] = $this->load->view('commissioner_match_analysis',$v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function post_fixture_comment($comment_id, $fixture_id)
	{
		//form validation
		$this->form_validation->set_rules('comment_fixture_description','Comment DEscription','required|xss_clean');
		
		if ($this->form_validation->run())
		{
			//insert
			if($this->fixture_model->add_fixture_comments($fixture_id,$comment_id))
			{
				$this->session->set_userdata('success_message', 'Comment added successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Comment addition failed');
			}
		}
		redirect('soccer-management/commissioners');
	}
	public function post_fixture_assignment($assessment_id, $fixture_id)
	{
		//form validation
		$this->form_validation->set_rules('assessment_fixture_description','Assessment DEscription','required|xss_clean');
		$this->form_validation->set_rules('assessment_name','Rating','required|xss_clean');
		
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_fixture_assessments($fixture_id,$assessment_id))
			{
				$this->session->set_userdata('success_message', 'Assessment added successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Assessment addition failed');
			}
		}
		else
		{
			
		}
		redirect('soccer-management/commissioners');
	}
	public function assigned_leagues()
	{
		// $personnel_id = $this->session->userdata('personnel_id');
		// $where = 'league.league_deleted = 0 AND league.league_type = 1 AND fixture_commissioner.fixture_id = fixture.fixture_id AND fixture.league_duration_id = league_duration.league_duration_id AND league_duration.league_id = league.league_id AND fixture.venue_id = venue.venue_id AND fixture_commissioner.personnel_id = '.$personnel_id;
		// $table = 'league,fixture,league_duration,fixture_commissioner,venue';
		// // $league_search = $this->session->userdata('league_search2');
		
		// // if(!empty($league_search))
		// // {
		// // 	$where .= $league_search;
		// // }
		// //pagination
		// $segment = 3;
		// $this->load->library('pagination');
		// $config['base_url'] = site_url().'commissioner/league';
		// $config['total_rows'] = $this->users_model->count_items($table, $where);
		// $config['uri_segment'] = $segment;
		// $config['per_page'] = 20;
		// $config['num_links'] = 5;
		
		// $config['full_tag_open'] = '<ul class="pagination pull-right">';
		// $config['full_tag_close'] = '</ul>';
		
		// $config['first_tag_open'] = '<li>';
		// $config['first_tag_close'] = '</li>';
		
		// $config['last_tag_open'] = '<li>';
		// $config['last_tag_close'] = '</li>';
		
		// $config['next_tag_open'] = '<li>';
		// $config['next_link'] = 'Next';
		// $config['next_tag_close'] = '</span>';
		
		// $config['prev_tag_open'] = '<li>';
		// $config['prev_link'] = 'Prev';
		// $config['prev_tag_close'] = '</li>';
		
		// $config['cur_tag_open'] = '<li class="active"><a href="#">';
		// $config['cur_tag_close'] = '</a></li>';
		
		// $config['num_tag_open'] = '<li>';
		// $config['num_tag_close'] = '</li>';
		// $this->pagination->initialize($config);
		
		// $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
  //       $v_data["links"] = $this->pagination->create_links();
		// $query = $this->commissioners_model->get_all_league_fixtures($table, $where, $config["per_page"], $page, $order='fixture.fixture_id', $order_method='DESC');
		
		// //change of order method 
	
		
		// $data['title'] = 'League';
		// $v_data['title'] = $data['title'];
		
		// $v_data['order'] = $order;
		// $v_data['order_method'] = $order_method;
		// $v_data['league_fixtures'] = $query;
		// $v_data['teams'] = $this->team_model->all_teams();
		// $v_data['page'] = $page;
		// $v_data['venues'] = $this->fixture_model->get_all_venues();
		// $data['content'] = $this->load->view('fixture/commissioner_fixtures', $v_data, true);


		
		// $this->load->view('admin/templates/general_page', $data);

		$referee_id = $this->session->userdata('personnel_id');
		$v_data['referee_details'] = $this->referee_model->get_referee($referee_id);

		$where = 'personnel_id = '.$referee_id;
		$table = 'fixture_commissioner';
		$referee_search = $this->session->userdata('referee_search2');
		
		if(!empty($referee_search))
		{
			$where .= $referee_search;
		}
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'commissioner/league';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->referee_model->get_all_referee($table, $where, $config["per_page"], $page, $order='personnel_id', $order_method='ASC');
		
		$data['title'] = 'Referee';
		$v_data['title'] = $data['title'];
		
		$v_data['league_referee_games'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('commissioner/all_commissioner_leagues', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
	}

	public function add_fixture_result($fixture_id,$module=null)
	{
		//get league_id
		$league_duration_id = $this->referee_model->get_league_duraion($fixture_id);
		$league_id = $this->referee_model->get_league($league_duration_id);
		$league_name = $this->league_model->get_league_name($league_id);
		$v_data['title'] = 'Results for fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		$v_data['league_id'] = $league_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->fixture_model->get_comment_fixtures($fixture_id);
		$v_data['all_assessment_fixtures'] = $this->fixture_model->get_fixture_assessments($fixture_id);
		$v_data['goal_scorer'] = $this->league_model->get_all_fixture_players_scorers($fixture_id);
		$v_data['fixture_fouls'] = $this->fixture_model->get_all_fixture_fouls_commissioner($fixture_id);
		$v_data['total_fixture_goal'] = $this->league_model->get_all_goals_scored_commissioner($fixture_id);
		$v_data['home_team'] = $home_team = $this->league_model->get_home_team($fixture_id);
		$v_data['fixture_teams'] = $this->league_model->get_fixture_teams($fixture_id);


		$v_data['team_details'] = $this->fixture_model->get_fixture_teams_details($fixture_id);
		$v_data['team_details_home_in'] = $this->fixture_model->get_fixture_teams_details_home_in($fixture_id);
		$v_data['team_details_home_out'] = $this->fixture_model->get_fixture_teams_details_home_out($fixture_id);
		$v_data['team_details_away_in'] = $this->fixture_model->get_fixture_teams_details_away_in($fixture_id);
		$v_data['team_details_away_out'] = $this->fixture_model->get_fixture_teams_details_away_out($fixture_id);




		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $away_team = $this->league_model->get_away_team($fixture_id);
		$v_data['fixture_detail'] = $this->referee_model->get_fixture_detail_commissioner($fixture_id);
		$v_data['league_name'] = $league_name;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['module'] = $module;
		$v_data['league_duration_id'] = $league_duration_id;

		$v_data['title_away'] = $away_team.' Away Substitutes';
		$v_data['title_home'] = $home_team.' Home Substitutes';

		$data['content'] = $this->load->view('league/fixture_results', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function commissioner_tournaments()
	{
		$referee_id = $this->session->userdata('personnel_id');
		$v_data['referee_details'] = $this->referee_model->get_referee($referee_id);

		$where = 'personnel_id = '.$referee_id;
		$table = 'tournament_fixture_commissioner';
		$referee_search = $this->session->userdata('referee_search2');
		
		if(!empty($referee_search))
		{
			$where .= $referee_search;
		}
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'commissioner/tournament';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->referee_model->get_all_referee($table, $where, $config["per_page"], $page, $order='personnel_id', $order_method='ASC');
		
		$data['title'] = 'Referee';
		$v_data['title'] = $data['title'];
		
		$v_data['tournament_referee_games'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('commissioner/all_commissioner_tournaments', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function add_torna_commissioner_fixture_result($tournament_fixture_id)
	{
		//get league_id
		$tournament_duration_id = $this->referee_model->get_tournament_duration($tournament_fixture_id);
		$tournament_id = $this->referee_model->get_tournament($tournament_duration_id);
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['title'] = 'Results for fixture '.$tournament_id.' season '.$tournament_duration_id.' in '.$tournament_name.' tournament';
		$v_data['tournament_id'] = $tournament_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->tournament_model->get_comment_fixtures($tournament_fixture_id);
		$v_data['all_assessment_fixtures'] = $this->tournament_model->get_fixture_assessments($tournament_fixture_id);
		$v_data['goal_scorer'] = $this->tournament_model->get_all_fixture_players_scorers($tournament_fixture_id);
		$v_data['fixture_fouls'] = $this->tournament_model->get_all_fixture_fouls_commissioner($tournament_fixture_id);
		$v_data['total_fixture_goal'] = $this->tournament_model->get_all_goals_scored_commissioner($tournament_fixture_id);
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['tournament_name'] = $tournament_name;
		$v_data['fixture_teams'] = $this->tournament_model->get_fixture_teams($tournament_fixture_id);
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['fixture_detail'] = $this->tournament_model->get_fixture_detail($tournament_fixture_id);


		$v_data['team_details_home_in'] = $this->fixture_model->get_tournament_fixture_teams_details_home_in($tournament_fixture_id);
		$v_data['team_details_home_out'] = $this->fixture_model->get_tournament_fixture_teams_details_home_out($tournament_fixture_id);
		$v_data['team_details_away_in'] = $this->fixture_model->get_tournament_fixture_teams_details_away_in($tournament_fixture_id);
		$v_data['team_details_away_out'] = $this->fixture_model->get_tournament_fixture_teams_details_away_out($tournament_fixture_id);

		$v_data['title'] = 'Add Tournament Substitutions';
		$v_data['title_away'] = 'Add Away Substitutes';
		$v_data['title_home'] = 'Add Home Substitutes';
		$v_data['tournament_fixture_id'] =  $tournament_fixture_id;


		$data['content'] = $this->load->view('commissioner/tournament_fixture_results', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}