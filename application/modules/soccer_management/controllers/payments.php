<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Payments extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('payments_model');
	}
	
	public function index($order = "personnel.personnel_fname", $order_method = "ASC")
	{
		//all referees who've beeen sent to the admin for payments
		$where = 'personnel.personnel_id = tournament_fixture_referee.referee_id';
		$table = 'personnel, tournament_fixture_referee';
		
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/referee-payment/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$payment_referees = $this->payments_model->get_payment_referees($table, $where, $config["per_page"], $page, $order, $order_method);
		$league_payment_referees = $this->payments_model->get_league_payment_referees();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		
		$v_data['title'] = $data['title'] = 'Referee Payments';
		$v_data['payment_referees'] = $payment_referees;
		$v_data['league_payment_referees'] = $league_payment_referees;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('referee_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function commissioner_payments($order = "personnel.personnel_fname", $order_method = "ASC")
	{
		//all referees who've beeen sent to the admin for payments
		$where = 'personnel.personnel_id = tournament_fixture_commissioner.personnel_id';
		$table = 'personnel, tournament_fixture_commissioner';
		
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/commissioner-payment/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$payment_commissioners = $this->payments_model->get_payment_commissioners($table, $where, $config["per_page"], $page, $order, $order_method);
		$league_payment_commisioners = $this->payments_model->get_league_payment_commissioners();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		
		$v_data['title'] = $data['title'] = 'Commissioners Payments';
		$v_data['payment_commissioners'] = $payment_commissioners;
		$v_data['league_payment_commissioners'] = $league_payment_commisioners;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('commissioner_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>