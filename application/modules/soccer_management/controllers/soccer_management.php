
<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Soccer_management extends MX_Controller 
{
	var $team_logo_path;
	var $team_logo_location;
	var $csv_path;
	var $document_upload_path;
	var $document_upload_location;

	function __construct()
	{
		parent:: __construct();
		
		
		$this->team_logo_path = realpath(APPPATH . '../assets/team_logo');
		$this->team_logo_location = base_url().'assets/team_logo/';
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->document_upload_path = realpath(APPPATH . '../assets/document_uploads');
		$this->document_upload_location = base_url().'assets/document_uploads/';
		
		$this->load->model('site/site_model');
		//$this->load->model('administration/reports_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/email_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('soccer_management_model');
		$this->load->model('coach_model');
		$this->load->model('team_model');
		$this->load->model('player_model');
		$this->load->model('league_model');
		$this->load->model('fixture_model');
		$this->load->model('referee_model');
		$this->load->model('tournament_model');
		$this->load->model('soccer_management/reports_model');
		$this->load->model('soccer_management/payments_model');
		
		$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard() 
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('dashboard', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}
	public function index()
	{
		$this->session->unset_userdata('all_transactions_search');
		
		$data['content'] = $this->load->view('dashboard', '', TRUE);
		
		$data['title'] = 'Dashboard';
		$this->load->view('admin/templates/general_page', $data);
	}
	public function youth_league()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$v_data['query'] = $this->soccer_management_model->get_youth_league();
		$data['content'] = $this->load->view('youth_league', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_youth_league()
	{
		//form validation rules
		$this->form_validation->set_rules('league_name', 'League Names', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_id = $this->soccer_management_model->add_youth_league();
			if($league_id > 0)
			{
				$this->session->set_userdata("success_message", "League added successfully");
				redirect('soccer-management/youth-league');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add league. Please try again ".$league_id);
			}
		}
		$data['title'] = 'Add league';
		$v_data['title'] = $data['title'];
		$v_data['order_method'] = 'DESC';
		$data['content'] = $this->load->view('add_youth_league', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	
	}
	public function edit_youth_league($league_id)
	{
		$this->form_validation->set_rules('league_name', 'League Names', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->league_model->edit_league($league_id))
			{
				$this->session->set_userdata('success_message', 'league\'s general details updated successfully');
				redirect('soccer-management/youth-league');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update league\'s general details. Please try again');
			}
		}

		$v_data['league'] = $this->league_model->get_league($league_id);
		$v_data['league_id'] = $league_id;
		$v_data['title'] = 'Edit League';
		$data['content'] = $this->load->view('league/edit_league', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function activate_youth_league($league_id)
	{
		$this->league_model->activate_league($league_id);
		$this->session->set_userdata('success_message', 'League activated successfully');
		redirect('soccer-management/youth-league');
	}
	public function deactivate_youth_league($league_id)
	{
		$this->league_model->deactivate_league($league_id);
		$this->session->set_userdata('success_message', 'League disabled successfully');
		redirect('soccer-management/youth-league');
	}
	public function delete_youth_league($league_id)
	{
		if($this->league_model->delete_league($league_id))
		{
			$this->session->set_userdata('success_message', 'League has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'League could not deleted');
		}
		redirect('soccer-management/youth-league');
	}
	public function add_youth_league_duration($league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('league_duration_end_date', 'End date', 'required|xss_clean');
		$this->form_validation->set_rules('league_duration_start_date', 'Start date', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_duration_id = $this->league_model->add_league_duration($league_id);
			if($league_duration_id > 0)
			{
				$this->session->set_userdata("success_message", "League duration added successfully");
				redirect('soccer-management/add-youth-league-duration/'.$league_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add league duration. Please try again ".$league_id);
			}
		}
		$data['title'] = 'Add league duartion';
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['league_name'] = $this->league_model->get_league_name($league_id);
		$v_data['league_durations'] = $this->league_model->get_all_league_durations($league_id);
		$data['content'] = $this->load->view('league/add_league_duration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function youth_teams($order = 'team.team_id', $order_method = 'ASC')
	{
		$this->session->set_userdata('youth_teams', 2);
		$table = '';
		$where = 'team.team_id > 0 AND team.team_deleted = 0 AND team_type = 2';
		$table .= 'team';
		$team_search = $this->session->userdata('team_search2');
		
		if(!empty($team_search))
		{
			$where .= $team_search;
			$table .=',team_coach';
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/team/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->team_model->get_all_team($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Youth Teams';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['coaches'] = $this->coach_model->all_coach();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('team/all_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_youth_team()
	{
		$this->session->set_userdata('youth_teams',2);
		$v_data['team_logo_location'] = 'http://placehold.it/500x500';
		$this->session->unset_userdata('logo_error_message');
		//upload image if it has been selected
		$response = $this->team_model->upload_team_logo($this->team_logo_path);
		if($response)
		{
			$v_data['team_logo_location'] = $this->team_logo_location.$this->session->userdata('team_logo_file_name');
		}
		
		//case of upload error
		else
		{
			$v_data['logo_error'] = $this->session->userdata('logo_error_message');
		}
		
		//form validation rules
		$this->form_validation->set_rules('team_name', 'Team Names', 'required|xss_clean');
		$this->form_validation->set_rules('team_venue', 'Team venue', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if(empty($upload_error))
			{
				$data = array(
					'team_name'=>ucwords(strtolower($this->input->post('team_name'))),
					'team_logo'=>$this->session->userdata('team_logo_name'),
					'team_venue'=>ucwords(strtolower($this->input->post('team_venue'))),
					'created_by'=>$this->session->userdata('personnel_id'),
					'modified_by'=>$this->session->userdata('personnel_id'),
					'created' =>date('Y-m-d H:i:s'),
					'team_type'=>2
				);
				
				if($this->db->insert('team', $data))
				{
					$this->session->set_userdata('success_message', 'team added successfully');
					redirect('soccer-management/youth-teams');
				}
				else{
					$this->session->set_userdata('success_message', 'team not added');
					redirect('soccer-management/add-youth-team');
				}
			}
		}
		$v_data['team_logo_name'] = $this->session->userdata('team_logo_name');
		$v_data['team_logo_location'] = $this->team_logo_location;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$data['title'] = 'Add team';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('team/add_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function youth_league_players($order = 'player_id', $order_method = 'ASC') 
	{
		$this->session->set_userdata('youth_teams',2);
		$where = 'player_id > 0 AND deleted = 0 AND player_type = 2';
		$table = 'player';
		$player_search = $this->session->userdata('player_search2');
		
		if(!empty($player_search))
		{
			$where .= $player_search;
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/player/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->player_model->get_all_player($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Youth League Players';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('player/all_player', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_youth_player()
	{
		//form validation rules
		$this->form_validation->set_rules('team_id', 'Team', 'required|xss_clean');
		$this->form_validation->set_rules('player_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('player_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('player_dob', 'Date of Birth', 'required|xss_clean');
		$this->form_validation->set_rules('player_email', 'Email', 'valid_email|is_unique[player.player_email]|xss_clean');
		$this->form_validation->set_rules('player_phone', 'Phone', 'integer|xss_clean');
		$this->form_validation->set_rules('player_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'xss_clean');
		$this->form_validation->set_rules('player_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		//$this->form_validation->set_rules('license_number', 'License number', 'required|xss_clean');
		$this->form_validation->set_rules('player_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('player_post_code', 'Post code', 'integer|xss_clean');
		$this->form_validation->set_rules('player_national_id_number', 'National ID', 'xss_clean|is_unique[player.player_national_id_number]');
		$this->form_validation->set_rules('engagement_date','Start Date','required|xss_clean');
		
		//check if youth league player is being added
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			//validate guardian - details
			$this->form_validation->set_rules('player_guardian_fname', 'Guardian First Name', 'required|xss_clean');
			$this->form_validation->set_rules('player_guardian_surname', 'Other Name', 'required|xss_clean');
			$this->form_validation->set_rules('player_dob', 'Date of Birth', 'required|xss_clean');
			$this->form_validation->set_rules('player_guardian_email', 'Guardian Email', 'valid_email|is_unique[player_guardian.player_guardian_email]|xss_clean');
			//validate school	
			$this->form_validation->set_rules('school_name', 'School Name', 'required|xss_clean');
		}
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$file_name = '';
			$resize['width'] = 600;
			$resize['height'] = 800;
			$this->load->library('image_lib');
			$document_upload_path = $this->document_upload_path;
			
           if(is_uploaded_file($_FILES['player_image']['tmp_name']))
			{
				$response = $this->file_model->upload_file($document_upload_path, 'player_image', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			}//var_dump($file_name);die();
			$engagement_date = $this->input->post('engagement_date');
			$player_dob = $this->input->post('player_dob');
			if($engagement_date > $player_dob)
			{
				$player_id = $this->player_model->add_player($file_name);
				if($player_id > 0)
				{
					$youth_teams = $this->session->userdata('youth_teams');
					//update player guardian details
					if(!empty($youth_teams))
					{
						$this->soccer_management_model->add_player_guardian($player_id);
						$this->soccer_management_model->add_player_school($player_id);
					}
					$this->session->set_userdata("success_message", "Player added successfully");
					if(!empty($youth_teams))
					{
						redirect('soccer-management/youth-league-players');
					}
					else
					{
						redirect('soccer-management/player/');
					}
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not add player. Please try again ".$player_id);
				}
			}
			
		}
		
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$data['title'] = 'Add player';
		$v_data['document_upload_path'] = $this->document_upload_path;
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('player/add_player', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>