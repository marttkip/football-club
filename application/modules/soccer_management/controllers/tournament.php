<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Tournament extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('soccer_management/payments_model');
	}
	
	//show all tournamentes
	public function index($order = 'tournament_id', $order_method = 'DESC') 
	{
		$where = 'tournament_id > 0 AND tournament_delete = 0';
		$table = 'tournament';
		
		$tournament_search = $this->session->userdata('tournament_search2');
		
		if(!empty($tournament_search))
		{
			$where .= $tournament_search;
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/tournament/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->tournament_model->get_all_tournament($table, $where, $config["per_page"], $page, $order, $order_method);
		//var_dump($query);die();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Tournament';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('tournament/all_tournament', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_tournament()
	{
		$search_title = '';
		//search tournament name
		if(!empty($_POST['tournament_name']))
		{
			$search_title .= ' tournament name <strong>'.$_POST['tournament_name'].'</strong>';
			$tournament_names = explode(" ",$_POST['tournament_name']);
			$total = count($tournament_names);
			
			$count = 1;
			$tournament_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$tournament_name .= ' tournament.tournament_name LIKE \'%'.mysql_real_escape_string($tournament_names[$r]).'%\'';
				}
				
				else
				{
					$tournament_name .= ' tournament.tournament_name LIKE \'%'.mysql_real_escape_string($tournament_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$tournament_name .= ') ';
		}
		
		else
		{
			$tournament_name = '';
		}
		
		$search =$tournament_name;
		$this->session->set_userdata('tournament_search2', $search);
		$this->session->set_userdata('tournament_search_title2', $search_title);
		
		$this->index();
	}
	public function close_search()
	{
		$this->session->unset_userdata('tournament_search2', $search);
		$this->session->unset_userdata('tournament_search_title2', $search_title);
		
		redirect('soccer-management/tournament');
	}
	public function add_tournament()
	{
		//form validation rules
		$this->form_validation->set_rules('tournament_name', 'Tournament Names', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$tournament_id = $this->tournament_model->add_tournament();
			if($tournament_id > 0)
			{
				$this->session->set_userdata("success_message", "Tournament added successfully");
				redirect('soccer-management/tournament/');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add tournament. Please try again ".$tournament_id);
			}
		}
		$data['title'] = 'Add tournament';
		$v_data['title'] = $data['title'];
		$v_data['tournament_types'] = $this->tournament_model->get_all_tournament_types();
		$data['content'] = $this->load->view('tournament/add_tournament', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function activate_tournament($tournament_id)
	{
		$this->tournament_model->activate_tournament($tournament_id);
		$this->session->set_userdata('success_message', 'Tournament activated successfully');
		redirect('soccer-management/tournament');
	}
	public function deactivate_tournament($tournament_id)
	{
		$this->tournament_model->deactivate_tournament($tournament_id);
		$this->session->set_userdata('success_message', 'Tournament disabled successfully');
		redirect('soccer-management/tournament');
	}
	public function edit_tournament($tournament_id)
	{
		$this->form_validation->set_rules('tournament_name', 'Tournament Names', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->tournament_model->edit_tournament($tournament_id))
			{
				$this->session->set_userdata('success_message', 'tournament\'s general details updated successfully');
				redirect('soccer-management/tournament/');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update tournament\'s general details. Please try again');
			}
		}

		$v_data['tournament'] = $this->tournament_model->get_tournament($tournament_id);
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_types'] = $this->tournament_model->get_all_tournament_types();
		$v_data['title'] = 'Edit Tournament';
		$data['content'] = $this->load->view('tournament/edit_tournament', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function delete_tournament($tournament_id)
	{
		if($this->tournament_model->delete_tournament($tournament_id))
		{
			$this->session->set_userdata('success_message', 'tournament has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'tournament could not deleted');
		}
		redirect('soccer-management/tournament/');
	}
	public function add_tournament_duration($tournament_id)
	{
		
		//form validation rules
		$this->form_validation->set_rules('tournament_duration_end_date', 'Tournament end date', 'required|xss_clean');
		$this->form_validation->set_rules('tournament_duration_start_date', 'Tournament start date', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$tournament_duration_id = $this->tournament_model->add_tournament_duration($tournament_id);
			if($tournament_duration_id > 0)
			{
				$this->session->set_userdata("success_message", "Tournament duration added successfully");
				redirect('soccer-management/add-tournament-duration/'.$tournament_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add tournament duration. Please try again ".$tournament_id);
			}
		}
		$data['title'] = 'Add tournament duration';
		$v_data['title'] = $data['title'];
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_name'] = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['tournament_durations'] = $this->tournament_model->get_all_tournament_durations($tournament_id);
		$data['content'] = $this->load->view('tournament/add_tournament_duration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function edit_tournament_duration($tournament_duration_id,$tournament_id)
	{
		$this->form_validation->set_rules('tournament_duration_start_date', 'Start Date', 'xss_clean');
    	$this->form_validation->set_rules('tournament_duration_end_date', 'End Date', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->tournament_model->edit_tournament_duration($tournament_duration_id))
			{
				$this->session->set_userdata('success_message', 'season details updated successfully');
				redirect('soccer-management/add-tournament-duration/'.$tournament_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update season details. Please try again');
			}
		}
		$v_data['tournament_duration'] = $this->tournament_model->get_tournament_duration($tournament_duration_id);
		$v_data['tournament_name'] = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['title'] = 'Edit Season';
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$data['content'] = $this->load->view('tournament/edit_tournament_duration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function activate_tournament_duration($tournament_duration_id,$tournament_id)
	{
		$this->tournament_model->activate_tournament_duration($tournament_duration_id);
		$this->session->set_userdata('success_message', 'Season activated successfully');
		redirect('soccer-management/add-tournament-duration/'.$tournament_id);
	}
	public function deactivate_tournament_duration($tournament_duration_id,$tournament_id)
	{
		$this->tournament_model->deactivate_tournament_duration($tournament_duration_id);
		$this->session->set_userdata('success_message', 'Season disabled successfully');
		redirect('soccer-management/add-tournament-duration/'.$tournament_id);
	}
	public function delete_tournament_duration($tournament_duration_id,$tournament_id)
	{
		if($this->tournament_model->delete_tournament_duration($tournament_duration_id))
		{
			$this->session->set_userdata('success_message', 'Season has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Season could not deleted');
		}
		redirect('soccer-management/add-tournament-duration/'.$tournament_id);
	}
	public function add_tournament_duration_team($tournament_duration_id,$tournament_id)
	{
		//form validation rules
		$this->form_validation->set_rules('team_id', 'Team', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$tournament_team_id = $this->tournament_model->add_tournament_duration_team($tournament_duration_id);
			if($tournament_team_id > 0)
			{
				$this->session->set_userdata("success_message", "Team added successfully");
				redirect('soccer-management/add-tournament-duration-team/'.$tournament_duration_id.'/'.$tournament_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$data['title'] = 'Add Team to '.$tournament_name.' season '.$tournament_duration_id;
		$v_data['title'] = $data['title'];
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_teams'] = $this->tournament_model->get_tournament_teams($tournament_duration_id);
		$v_data['teams'] = $this->tournament_model->all_tournament_teams($tournament_duration_id);
		$v_data['tournament_name'] = $tournament_name; 
		$data['content'] = $this->load->view('tournament/add_tournament_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function remove_tournament_team($team_id,$tournament_id, $tournament_duration_id)
	{
		if($this->tournament_model->remove_tournament_team($team_id,$tournament_duration_id))
		{
			$this->session->set_userdata('success_message', 'Team removed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Team could not removed');
		}
		redirect('soccer-management/add-tournament-duration-team/'.$tournament_duration_id.'/'.$tournament_id);
	}
	public function add_tournament_duration_fixture($tournament_duration_id,$tournament_id)
	{
		//form validation rules
		$this->form_validation->set_rules('tournament_fixture_date', 'Date', 'required|xss_clean');
		$this->form_validation->set_rules('tournament_fixture_time', 'Time', 'required|xss_clean');
		$this->form_validation->set_rules('venue_id', 'Venue', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_id = $this->tournament_model->add_fixture($tournament_duration_id);
			if($fixture_id > 0)
			{
				$this->session->set_userdata("success_message", "Fixture added successfully");
				redirect('soccer-management/add-tournament-duration-fixture/'.$tournament_duration_id.'/'.$tournament_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add fixture. Please try again ");
			}
		}
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['tournament_fixtures'] = $this->tournament_model->get_season_fixtures($tournament_duration_id);
		$data['title'] = 'Add fixture to '.$tournament_name.' season '.$tournament_duration_id;
		$v_data['title'] = $data['title'];
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['venues'] = $this->fixture_model->get_all_venues();
		$data['content'] = $this->load->view('fixture/add_tournament_fixture', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function manage_tournament_fixture($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['title'] = $data['title'] = 'Tournament Fixture '.$tournament_fixture_id.' Management';
		$v_data['season_teams'] = $this->tournament_model->get_tournament_fixture_teams($tournament_duration_id,$tournament_fixture_id); 
		$v_data['fixture_teams'] = $this->tournament_model->get_fixture_teams($tournament_fixture_id);
		$v_data['team_types'] = $this->fixture_model->get_fixture_team_type();
		$v_data['referee_types'] = $this->fixture_model->get_fixture_referee_type();
		$v_data['referees'] = $this->referee_model->get_all_referees();
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		$v_data['personnel_commissioners'] = $this->fixture_model->get_all_personnel();
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_id'] = $tournament_id;
		$data['content'] = $this->load->view('tournament/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_tournament_fixture_referee($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		//form validation rules
		$this->form_validation->set_rules('referee_id', 'Referee', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$tournament_fixture_referee_id = $this->tournament_model->add_fixture_referee($tournament_fixture_id);
			if($tournament_fixture_referee_id > 0)
			{
				$this->session->set_userdata("success_message", "Referee added successfully to the tournament fixture");
				redirect('soccer-management/manage-tournament-fixture/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add referee. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			redirect('soccer-management/manage-tournament-fixture/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);  
		}
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$data['content'] = $this->load->view('tournament/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_tournament_fixture_commissioner($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		//form validation rules
		$this->form_validation->set_rules('commissioner_id', 'Commissioner', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_commission_id = $this->tournament_model->add_fixture_commissioner($tournament_fixture_id);
			if($fixture_commission_id > 0)
			{
				$this->session->set_userdata("success_message", "Commissioner added successfully to the tournament fixture");
				redirect('soccer-management/manage-tournament-fixture/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id); 
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add commissioner. Please try again ");
			}
		}
		$v_data['title'] = $data['title'];
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$data['content'] = $this->load->view('tournament/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_tournament_fixture_team($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$this->form_validation->set_rules('tournament_team_id', 'Team', 'required|xss_clean');
		$this->form_validation->set_rules('fixture_team_type_id', 'Team Type', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_id = $this->tournament_model->add_tournament_fixture_team($tournament_fixture_id);
			if($fixture_id > 0)
			{
				$this->session->set_userdata("success_message", "Team added successfully to the tournament fixture");
				redirect('soccer-management/manage-tournament-fixture/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);   
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		$data['title'] = 'Add team to tournament fixture '.$tournament_fixture_id.' season '.$tournament_duration_id;
		$v_data['title'] = $data['title'];
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['fixture_teams'] = $this->tournament_model->get_fixture_teams($tournament_fixture_id);
		$v_data['season_teams'] = $this->tournament_model->get_tournament_fixture_teams($tournament_duration_id,$tournament_fixture_id);
		$v_data['team_types'] = $this->fixture_model->get_fixture_team_type();
		$v_data['referee_types'] = $this->fixture_model->get_fixture_referee_type();
		$v_data['referees'] = $this->referee_model->get_all_referees();
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		$v_data['personnel_commissioners'] = $this->fixture_model->get_all_personnel();
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$data['content'] = $this->load->view('tournament/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_tournament_fixture_player($team_id,$tournament_fixture_team_id,$tournament_duration_id,$tournament_id,$tournament_fixture_id)
	{
		//form validation rules
		$this->form_validation->set_rules('player_id', 'Player', 'required|xss_clean');
		$this->form_validation->set_rules('fixture_player_type_id','Fixture Player Type', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_player_id = $this->tournament_model->add_fixture_player($tournament_fixture_team_id);
			if($fixture_player_id > 0)
			{
				$this->session->set_userdata("success_message", "Player added successfully to the fixture");
				redirect('soccer-management/add-tournament-fixture-player/'.$team_id.'/'.$tournament_fixture_team_id.'/'.$tournament_duration_id.'/'.$tournament_id.'/'.$tournament_fixture_id);    
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add player. Please try again ");
			}
		}
		$v_data['fixture_players'] = $this->league_model->get_all_fixture_players($team_id);
		$v_data['team_name'] = $this->league_model->get_team_name($team_id);
		$v_data['all_fixture_players'] = $this->tournament_model->get_all_fixture_players($tournament_fixture_team_id);
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$data['title'] = 'Add players to team';
		$v_data['title'] = $data['title'];
		$v_data['tournament_id'] = $tournament_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_fixture_team_id'] = $tournament_fixture_team_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['team_id'] = $team_id;
		$data['content'] = $this->load->view('tournament/add_fixture_player', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
		
	}
	public function remove_tournament_fixture_team($tournament_fixture_team_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		if($this->tournament_model->remove_league_team($tournament_fixture_team_id,$tournament_fixture_id))
		{
			$this->session->set_userdata('success_message', 'Team removed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Team could not removed');
		}
		redirect('soccer-management/manage-tournament-fixture/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}
	public function add_tournament_fixture_result($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['title'] = 'Results for fixture '.$tournament_fixture_id.' season '.$tournament_duration_id.' in '.$tournament_name.' tournament';
		$v_data['tournament_id'] = $tournament_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->tournament_model->get_comment_fixtures($tournament_fixture_id);
		$v_data['all_assessment_fixtures'] = $this->tournament_model->get_fixture_assessments($tournament_fixture_id);
		$v_data['goal_scorer'] = $this->tournament_model->get_all_fixture_players_scorers($tournament_fixture_id);
		$v_data['fixture_fouls'] = $this->tournament_model->get_all_fixture_fouls($tournament_fixture_id);
		$v_data['total_fixture_goal'] = $this->tournament_model->get_all_goals_scored($tournament_fixture_id);
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['tournament_name'] = $tournament_name;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$data['content'] = $this->load->view('tournament/fixture_results', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_tournament_fixture_scores($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$this->form_validation->set_rules('tournament_fixture_team_player_id', 'Scorer', 'required|xss_clean');
		$this->form_validation->set_rules('goal_minute','Score time', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$goal_id = $this->tournament_model->add_fixture_goals($tournament_fixture_id);
			if($goal_id > 0)
			{
				$this->session->set_userdata("success_message", "Goal added successfully to the tournament fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
		redirect('soccer-management/add-tournament-fixture-result/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}
	public function add_fixture_fouls($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		//form validation
		$this->form_validation->set_rules('foul_player_id', 'Offender', 'required|xss_clean');
		$this->form_validation->set_rules('foul_type_id','Foul type', 'required|xss_clean');
		$this->form_validation->set_rules('foul_minute','Foul minute', 'required|xss_clean');
		$this->form_validation->set_rules('action_id','Action taken', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$foul_id = $this->tournament_model->add_fixture_fouls($tournament_fixture_id);
			if($foul_id > 0)
			{
				$this->session->set_userdata("success_message", "Foul added successfully to the tournament fixture");
				redirect('soccer-management/add-tournament-fixture-result/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add foul. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			redirect('soccer-management/add-tournament-fixture-result/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
		}	
	}
	public function post_fixture_comment($comment_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		//form validation
		$this->form_validation->set_rules('comment_fixture_description', 'Comment', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//echo $comment_id;die();
			$fixture_comment_id = $this->tournament_model->add_fixture_comments($tournament_fixture_id,$comment_id);
			if($fixture_comment_id > 0)
			{
				$this->session->set_userdata("success_message", "Comment added successfully to the fixture");
				
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add comment. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
		
		}	
		redirect('soccer_management/commissioner/add_torna_fixture_result/'.$tournament_fixture_id);  
	}
	public function post_fixture_assignment($assessment_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$this->form_validation->set_rules('assessment_fixture_description', 'Assessment', 'required|xss_clean');
		$this->form_validation->set_rules('assessment_name', 'Rating', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//echo $comment_id;die();
			$fixture_assessment_id = $this->tournament_model->add_fixture_assessmentss($tournament_fixture_id,$assessment_id);
			if($fixture_assessment_id > 0)
			{
				$this->session->set_userdata("success_message", "Assessment added successfully to the fixture");
				
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add assessment. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			
		}
		redirect('soccer_management/commissioner/add_torna_fixture_result/'.$tournament_fixture_id);  
	}
	public function view_tournament_duration_table($tournament_duration_id,$tournament_id)
	{
		$v_data['title'] = $data['title'] = 'Tournament Standings';
		$v_data['teams'] = $this->tournament_model->get_league_teams($tournament_duration_id);
		$data['content'] = $this->load->view('tournament/league_table', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_tournament_fixture_payments($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['title'] = $data['title'] = 'Tournament Payments';
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		$data['content'] = $this->load->view('tournament/fixture_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function send_referee_to_admin($referee_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		if($this->tournament_model->send_referee_to_admin($referee_id,$tournament_fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee sent successfully to admin");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to admin. Please try again ");
		}
		redirect('soccer-management/add-tournament-fixture-payments/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}

	public function send_commissioner_to_admin($commissioner_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		if($this->tournament_model->send_commissioner_to_admin($commissioner_id,$tournament_fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee sent successfully to admin");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to admin. Please try again ");
		}
		redirect('soccer-management/add-tournament-fixture-payments/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}
	public function send_referee_to_accounts($referee_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		if($this->tournament_model->send_referee_to_accounts($referee_id,$tournament_fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee sent successfully to accounts");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to accounts. Please try again ");
		}
		redirect('soccer-management/add-tournament-fixture-payments/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}

	public function send_commissioner_to_accounts($commissioner_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		if($this->tournament_model->send_commissioner_to_accounts($commissioner_id,$tournament_fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee sent successfully to accounts");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to accounts. Please try again ");
		}
		redirect('soccer-management/add-tournament-fixture-payments/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}
	public function send_payments_to_referee($referee_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		
		if($this->tournament_model->send_payments_to_referee($referee_id,$tournament_fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee payment for fixture added successfully");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add referee payment. Please try again ");
		}
		redirect('soccer-management/add-tournament-fixture-payments/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}

	public function send_payments_to_commissioner($commissioner_id,$tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_id'] = $tournament_id;
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		$v_data['fixture_commissioners'] = $this->tournament_model->get_all_fixture_commissioners($tournament_fixture_id);
		
		if($this->tournament_model->send_payments_to_commissioner($commissioner_id,$tournament_fixture_id))
		{
			$this->session->set_userdata("success_message", "commissioner payment for fixture added successfully");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add commissioner payment. Please try again ");
		}
		redirect('soccer-management/add-tournament-fixture-payments/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id);
	}
	public function pay_referee($referee_id,$referee_payments_queue_id,$fixture_type)
	{
		//form validation 
		$this->form_validation->set_rules('payment_amount','Payment Amount','required|xss_clean');
		
		if($this->form_validation->run())
		{
			//save the referee payment
			if($this->tournament_model->pay_referee($referee_payments_queue_id))
			{
				$this->session->set_userdata("success_message", "Referee payment for fixture added successfully");
				redirect('accounts/referee-payments');
			}
			else
			{
			}
		}
		if($fixture_type == 2)
		{
			$page_title = "Tournament";
		}
		else
		{
			$page_title = "League";
		}
		$referee_details = $this->referee_model->get_referee($referee_id)->row();
		$referee_fname = $referee_details->personnel_fname;	
		$referee_onames = $referee_details->personnel_onames;
		$referee_name = $referee_fname.' '.$referee_onames;
		
		$data['title'] = "Ref Payments";
		$v_data['title'] =  $referee_name.' Payments For '.$page_title;
		$v_data['referee_id'] = $referee_id;
		$v_data['referee_payments_queue_id'] = $referee_payments_queue_id;
		$data['content'] = $this->load->view('tournament/pay_referee', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function view_referee_queue($referee_id,$referee_payments_queue_id,$fixture_type,$tournament_fixture_id)
	{
		if($referee_payments_queue_id == 0)
		{
			$this->session->set_userdata("error_message","The referee details cannot be viewed until the referee is added to the admin queue");
			redirect('accounts/referee-payments');
		}
		$referee_invoice = $this->tournament_model->get_ref_invoice($tournament_fixture_id,$referee_id);
		$referee_payments = $this->tournament_model->get_ref_payment($referee_payments_queue_id);
		$v_data['referee_invoice'] = $referee_invoice;
		$v_data['referee_payments'] = $referee_payments;
		
		$referee = $this->referee_model->get_referee($referee_id);
		$referee_details = $referee->row();
		$referee_fname = $referee_details->personnel_fname;
		$referee_onames = $referee_details->personnel_onames;
		$v_data['title'] = $referee_fname.' '.$referee_onames;
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['fixture_date'] = $this->payments_model->get_fixture_date($tournament_fixture_id);
		
		$data['title'] = 'Referee Fixture Statement';
		$data['content'] = $this->load->view('tournament/ref_queue_details', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
		
	}

	public function view_commissioner_queue($personnel_id,$personnel_payments_queue_id,$fixture_type,$tournament_fixture_id)
	{
		if($personnel_payments_queue_id == 0)
		{
			$this->session->set_userdata("error_message","The personnel details cannot be viewed until the personnel is added to the admin queue");
			redirect('accounts/commissioner-payments');
		}
		$personnel_invoice = $this->tournament_model->get_ref_commissioner_invoice($tournament_fixture_id,$personnel_id);
		$personnel_payments = $this->tournament_model->get_commissioner_ref_payment($personnel_payments_queue_id);
		$v_data['personnel_invoice'] = $personnel_invoice;
		$v_data['personnel_payments'] = $personnel_payments;
		
		$personnel = $this->personnel_model->get_personnel($personnel_id);
		$personnel_details = $personnel->row();
		$personnel_fname = $personnel_details->personnel_fname;
		$personnel_onames = $personnel_details->personnel_onames;
		$v_data['title'] = $personnel_fname.' '.$personnel_onames;
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['fixture_date'] = $this->payments_model->get_fixture_date($tournament_fixture_id);
		
		$data['title'] = 'Tournament Commissioner Fixture Statement';
		$data['content'] = $this->load->view('tournament/commissioner_ref_queue_details', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
		
	}
	public function pay_commissioner($commissioner_id,$commissioner_payments_queue_id,$fixture_type)
	{
		//form validation 
		$this->form_validation->set_rules('payment_amount','Payment Amount','required|xss_clean');
		
		if($this->form_validation->run())
		{
			//save the referee payment
			if($this->tournament_model->pay_commissioner($commissioner_payments_queue_id))
			{
				$this->session->set_userdata("success_message", "Referee payment for fixture added successfully");
				redirect('accounts/commissioner-payments');
			}
			else
			{
			}
		}
		if($fixture_type == 2)
		{
			$page_title = "Tournament";
		}
		else
		{
			$page_title = "League";
		}
		$personnel_details = $this->personnel_model->get_personnel($commissioner_id)->row();
		$personnel_fname = $personnel_details->personnel_fname;	
		$personnel_onames = $personnel_details->personnel_onames;
		$personnel_name = $personnel_fname.' '.$personnel_onames;
		
		$data['title'] = "Ref Payments";
		$v_data['title'] =  $personnel_name.' Payments For '.$page_title;
		$v_data['commissioner_id'] = $commissioner_id;
		$v_data['commissioner_payments_queue_id'] = $commissioner_payments_queue_id;
		$data['content'] = $this->load->view('tournament/pay_personnel', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function delete_league_fixture_summary($fixture_summary_id, $fixture_id)
	{
		if($this->tournament_model->delete_league_fixture_summary($fixture_summary_id))
		{
			$this->session->set_userdata("success_message", "Summary deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Summary could not be deleted");
		}
		redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id);
	}
	public function delete_league_subs($sub_id, $fixture_id)
	{
		if($this->tournament_model->delete_league_subs($sub_id))
		{
			$this->session->set_userdata("success_message", "SUb deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "sub could not be deleted");
		}
		redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id);
	}
	public function delete_league_foul($foul_id, $fixture_id)
	{
		if($this->tournament_model->delete_league_foul($foul_id))
		{
			$this->session->set_userdata("success_message", "Foul deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "foul could not be deleted");
		}
		redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id);
	}
	public function delete_league_goal($goal_id, $fixture_id)
	{
		if($this->tournament_model->delete_league_goal($goal_id))
		{
			$this->session->set_userdata("success_message", "Goal deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Goal could not be deleted");
		}
		redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id);
	}
	public function get_tournament_fixture_summary($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$tournament_duration_id = $this->referee_model->get_tournament_duration($tournament_fixture_id);
		$tournament_id = $this->referee_model->get_tournament($tournament_duration_id);
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['all_fixture_comments'] = $this->tournament_model->get_comment_fixtures($tournament_fixture_id);
		$v_data['all_assessment_fixtures'] = $this->tournament_model->get_fixture_assessments($tournament_fixture_id);
		$v_data['goal_scorer'] = $this->tournament_model->get_all_fixture_players_scorers($tournament_fixture_id);
		$v_data['fixture_fouls'] = $this->tournament_model->get_all_fixture_fouls($tournament_fixture_id);
		$v_data['total_fixture_goal'] = $this->tournament_model->get_all_goals_scored($tournament_fixture_id);
		$v_data['penalty_goal'] = $this->tournament_model->get_all_penalty_scored($tournament_fixture_id);
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['tournament_name'] = $tournament_name;
		$v_data['fixture_teams'] = $this->tournament_model->get_fixture_teams($tournament_fixture_id);
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['fixture_details'] = $this->tournament_model->get_fixture_detail($tournament_fixture_id);

		$v_data['match_timings'] = $this->tournament_model->get_fixture_detail($tournament_fixture_id);
		$v_data['home_players'] = $this->tournament_model->get_home_players($tournament_fixture_id);
		$v_data['ref_summary'] = $this->tournament_model->get_ref_summary($tournament_fixture_id);
		$v_data['away_players'] = $this->tournament_model->get_away_players($tournament_fixture_id);
		$v_data['home_substitutes'] = $this->tournament_model->get_home_fixture_subs($tournament_fixture_id);
		$v_data['away_substitutes'] = $this->tournament_model->get_away_fixture_subs($tournament_fixture_id);
		$v_data['title'] = 'Referee Fixture Summary for '.$v_data['home_team'].' Vs '.$v_data['away_team'];
		$this->load->view('tournament/ref_league_summary', $v_data);
	}
	public function get_comm_tournament_fixture_summary($tournament_fixture_id,$tournament_duration_id,$tournament_id)
	{
		$v_data['fixture_details'] = $this->tournament_model->get_fixture_detail($tournament_fixture_id);
		$v_data['all_fixture_comments'] = $this->tournament_model->get_comment_fixtures($tournament_fixture_id);
		$v_data['all_assessment_fixtures'] = $this->tournament_model->get_fixture_assessments($tournament_fixture_id);
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['fixture_referees'] = $this->tournament_model->get_fixture_referee($tournament_fixture_id);
		
		$v_data['title'] = 'Commissioner Fixture Summary for '.$v_data['home_team'].' Vs '.$v_data['away_team'];
		$this->load->view('tournament/comm_tournament_summary', $v_data);
	}
	public function delete_fixture_player($player_fixture_id,$team_id,$tournament_fixture_team_id,$tournament_duration_id,$tournament_id,$tournament_fixture_id)
	{
		if($this->tournament_model->delete_fixture_player($player_fixture_id))
		{
			$this->session->set_userdata("success_message", "Player deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Player could not be deleted");
		}
		redirect('soccer-management/add-tournament-fixture-player/'.$team_id.'/'.$tournament_fixture_team_id.'/'.$tournament_duration_id.'/'.$tournament_id.'/'.$tournament_fixture_id);
	}
}
?>