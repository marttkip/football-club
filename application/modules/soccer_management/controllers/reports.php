<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Reports extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	public function tournament_payments()
	{
		//$where = 'tournament_fixture.tournament_fixture_id = tournament_fixture_referee.tournament_fixture_id AND tournament_fixture_referee.referee_id = tournament_fixture_referee_payment.referee_id AND tournament_fixture.tournament_fixture_id = tournament_fixture_referee_payment.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = tournament_fixture_team.tournament_fixture_id AND tournament_fixture_team.tournament_team_id = tournament_team.tournament_team_id AND tournament_team.team_id = team.team_id AND tournament_fixture_referee_payment.referee_id = referee.referee_id';
		//$table = 'tournament_fixture, tournament_fixture_referee, tournament_fixture_referee_payment, tournament_team, team, tournament_fixture_team,referee';
		
		//$v_data['tournament_referee_payments'] = $this->reports_model->get_all_tournament_payments($table, $where);
		$v_data['tournament_referee_payments'] = $this->reports_model->referee_tournament_payements();
		$data['title'] = $v_data['title'] = 'Tournament Payments';
		$data['content'] = $this->load->view('reports/tournament_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function referees()
	{
		//league_referees
		$league_referees = $this->payments_model->get_league_payment_referees();
		$v_data['league_referees'] = $league_referees;
		$tournament_referees = $this->payments_model->get_tournament_payment_referees();
		$v_data['tournament_referees'] = $tournament_referees;
		$v_data['title'] = $data['title'] = 'Referee Payments';
		
		$data['content'] = $this->load->view('reports/payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>