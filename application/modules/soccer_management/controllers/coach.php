<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Coach extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	//show all coaches
	public function index($order = 'coach.coach_id', $order_method = 'ASC') 
	{
		$table ='';
		$where = 'coach.coach_id > 0 AND coach.deleted = 0';
		$table .= 'coach';
		$coach_search = $this->session->userdata('coach_search2');
		
		if(!empty($coach_search))
		{
			$where .= $coach_search;
			$table .=',team_coach';
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/coach/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->coach_model->get_all_coach($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Coach';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('coach/all_coach', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function close_search()
	{
		$this->session->unset_userdata('coach_search2', $search);
		$this->session->unset_userdata('coach_search_title2', $search_title);
		
		redirect('soccer-management/coach');
	}
	public function search_coach()
	{
		$coach_number = $this->input->post('coach_number');
		$team_id = $this->input->post('team_id');
		$search_title = '';
		$table ='';
		if(!empty($coach_number))
		{
			$search_title .= ' coach number <strong>'.$coach_number.'</strong>';
			$coach_number = ' AND coach.coach_number = \''.$coach_number.'\'';
		}
		
		if(!empty($team_id))
		{
			$search_title .= ' team id <strong>'.$team_id.'</strong>';
			$team_id = ' AND coach.coach_id = team_coach.coach_id AND team_coach.team_id ='.$team_id;
		}
		
		//search surname
		if(!empty($_POST['coach_fname']))
		{
			$search_title .= ' first name <strong>'.$_POST['coach_fname'].'</strong>';
			$surnames = explode(" ",$_POST['coach_fname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' coach.coach_fname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\'';
				}
				
				else
				{
					$surname .= ' coach.coach_fname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['coach_onames']))
		{
			$search_title .= ' other names <strong>'.$_POST['coach_onames'].'</strong>';
			$other_names = explode(" ",$_POST['coach_onames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' coach.coach_onames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\'';
				}
				
				else
				{
					$other_name .= ' coach.coach_onames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $coach_number.$team_id.$surname.$other_name;
		$this->session->set_userdata('coach_search2', $search);
		$this->session->set_userdata('coach_search_title2', $search_title);
		
		$this->index();
	}
	
	public function add_coach() 
	{
		//form validation rules
		$this->form_validation->set_rules('team_id', 'Branch', 'xss_clean');
		$this->form_validation->set_rules('coach_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('coach_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('coach_dob', 'Date of Birth', 'xss_clean');
		$this->form_validation->set_rules('coach_email', 'Email', 'valid_email|is_unique[coach.coach_email]|xss_clean');
		$this->form_validation->set_rules('coach_phone', 'Phone', 'integer|xss_clean');
		$this->form_validation->set_rules('coach_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'integer|xss_clean');
		$this->form_validation->set_rules('coach_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('coach_number', 'Coach number', 'xss_clean');
		$this->form_validation->set_rules('coach_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('coach_post_code', 'Post code', 'integer|xss_clean');
		$this->form_validation->set_rules('coach_national_id_number', 'National ID', 'required|xss_clean|is_unique[coach.coach_national_id_number]');
		$this->form_validation->set_rules('engagement_date','Start Date','required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$engagement_date = $this->input->post('engagement_date');
			$dob = $this->input->post('coach_dob');
			if($engagement_date > $dob)
			{
				$coach_id = $this->coach_model->add_coach();
				if($coach_id > 0)
				{
					$this->session->set_userdata("success_message", "Coach added successfully");
					redirect('soccer-management/coach/');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not add coach. Please try again ".$coach_id);
				}
			}
			else
			{
				$this->session->set_userdata("error_message", "Please make sure that the Engagement Date is greater than the Date of Birth");
					redirect('soccer-management/coach/');
			}
		}
		
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$data['title'] = 'Add coach';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('coach/add_coach', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_coach($coach_id)
    {
		$this->form_validation->set_rules('team_id', 'Branch', 'xss_clean');
    	$this->form_validation->set_rules('coach_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('coach_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('coach_dob', 'Date of Birth', 'xss_clean');
		// $this->form_validation->set_rules('coach_email', 'Email', 'valid_email|is_unique[coach.coach_email]|xss_clean');
		$this->form_validation->set_rules('coach_phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('coach_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'xss_clean');
		$this->form_validation->set_rules('coach_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('coach_number', 'Coach number', 'xss_clean');
		$this->form_validation->set_rules('coach_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('coach_post_code', 'Post code', 'xss_clean');
		$this->form_validation->set_rules('coach_national_id_number', 'ID number', 'xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->coach_model->edit_coach($coach_id))
			{
				$this->session->set_userdata('success_message', 'coach\'s general details updated successfully');
				redirect('soccer-management/coach/');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update coach\'s general details. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}

		$v_data['coach'] = $this->coach_model->get_coach($coach_id);
		$v_data['coach_id'] = $coach_id;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$v_data['title'] = 'Edit Coach';
		$data['content'] = $this->load->view('coach/edit_coach', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
    }
	public function activate_coach($coach_id)
	{
		$this->coach_model->activate_coach($coach_id);
		$this->session->set_userdata('success_message', 'Coach activated successfully');
		redirect('soccer-management/coach');
	}
	public function deactivate_coach($coach_id)
	{
		$this->coach_model->deactivate_coach($coach_id);
		$this->session->set_userdata('success_message', 'Coach disabled successfully');
		redirect('soccer-management/coach');
	}
	public function delete_coach($coach_id)
	{
		if($this->coach_model->delete_coach($coach_id))
		{
			$this->session->set_userdata('success_message', 'Coach has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Coach could not deleted');
		}
		redirect('soccer-management/coach');
	}
}
?>