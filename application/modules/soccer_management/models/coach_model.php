<?php
class Coach_model extends CI_Model 
{
	public function all_coach()
	{
		$this->db->where('deleted = 0');
		$query = $this->db->get('coach');
		
		return $query;
	}
	public function add_coach()
	{
		$data = array(
			'coach_onames'=>ucwords(strtolower($this->input->post('coach_onames'))),
			'coach_fname'=>ucwords(strtolower($this->input->post('coach_fname'))),
			'coach_dob'=>$this->input->post('coach_dob'),
			'coach_email'=>$this->input->post('coach_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'coach_phone'=>$this->input->post('coach_phone'),
			'civilstatus_id'=>$this->input->post('civil_status_id'),
			'coach_address'=>$this->input->post('coach_address'),
			'coach_locality'=>$this->input->post('coach_locality'),
			'title_id'=>$this->input->post('title_id'),
			'coach_number'=>$this->input->post('coach_number'),
			'coach_city'=>$this->input->post('coach_city'),
			'coach_post_code'=>$this->input->post('coach_post_code'),
			'coach_national_id_number'=>$this->input->post('coach_national_id_number'),
			'engagement_date'=>$this->input->post('engagement_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'created_on' =>date('Y-m-d H:i:s'),
		);
		
		if($this->db->insert('coach', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function edit_coach($coach_id)
	{
		$data = array(
			'coach_onames'=>ucwords(strtolower($this->input->post('coach_onames'))),
			'coach_fname'=>ucwords(strtolower($this->input->post('coach_fname'))),
			'coach_dob'=>$this->input->post('coach_dob'),
			'coach_email'=>$this->input->post('coach_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'coach_phone'=>$this->input->post('coach_phone'),
			'civilstatus_id'=>$this->input->post('civil_status_id'),
			'coach_address'=>$this->input->post('coach_address'),
			'coach_locality'=>$this->input->post('coach_locality'),
			'title_id'=>$this->input->post('title_id'),
			'coach_number' => $this->input->post('coach_number'),
			'coach_city' => $this->input->post('coach_city'),
			'coach_post_code' => $this->input->post('coach_post_code'),
			'coach_national_id_number' => $this->input->post('coach_national_id_number'),
			'engagement_date'=>$this->input->post('engagement_date'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		$this->db->where('coach_id', $coach_id);
		if($this->db->update('coach', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_coach($coach_id)
	{
		$data = array(
				'coach_status' => 1
			);
		$this->db->where('coach_id', $coach_id);
		

		if($this->db->update('coach', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_coach($coach_id)
	{
		$data = array(
				'coach_status' => 0
			);
		$this->db->where('coach_id', $coach_id);
		
		if($this->db->update('coach', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_coach($coach_id)
	{
		//delete coach
		$data = array(
				'deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('coach_id', $coach_id);
		
		if($this->db->update('coach', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_coach($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all coaches
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	public function get_coach($coach_id)
	{
		$this->db->select('*');
		$this->db->where('coach_id = '.$coach_id);
		$query = $this->db->get('coach');
		
		return $query;
	}
	public function get_team_coached($coach_id)
	{
		$this->db->select('team_id');
		$this->db->where('coach_id = '.$coach_id);
		$query = $this->db->get('team_coach');
		$coach_team_id = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$coach_team_id = $row->team_id;
			}
		}
		return $coach_team_id;
	}
	public function get_team_coach($team_id)
	{
		$this->db->select('coach_id');
		$this->db->where('team_id = '.$team_id);
		$query = $this->db->get('team_coach');
		$team_coach_id = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$team_coach_id = $row->coach_id;
			}
		}
		return $team_coach_id;
	}
	public function get_coach_name($coach_id)
	{
		$this->db->select('coach_fname, coach_onames');
		$this->db->where('coach_id = '.$coach_id);
		$query = $this->db->get('coach');
		$coach_name  = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$coach_fname = $row->coach_fname;
				$coach_onames = $row->coach_onames;
				$coach_name = $coach_fname. ' ' .$coach_onames;
			}
		}
		return $coach_name;
	}
}
?>