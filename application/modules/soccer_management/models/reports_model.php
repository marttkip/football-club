<?php
class Reports_model extends CI_Model 
{
	public function get_all_tournament_payments($table, $where)
	{
		$this->db->select('tournament_fixture_referee_payment.tournament_fixture_id, tournament_fixture_referee_payment.paid_on, tournament_fixture_referee_payment.amount_paid, referee.referee_fname, referee.referee_onames,team.team_name, tournament_fixture.tournament_fixture_date');
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query;
	}
	public function referee_tournament_payements()
	{
		$this->db->select('*');
		$this->db->where('referee.referee_id = tournament_fixture_referee_payment.referee_id');
		$query = $this->db->get('tournament_fixture_referee_payment, referee');
		return $query;
	}
	public function tournament_fixture_details($tournament_fixture_id)
	{
		$this->db->select('tournament_fixture.*');
		$this->db->where('tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id.' AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id');
		$query = $this->db->get('tournament_fixture, tournament_fixture_team');
		return $query;
	}
	public function referee_type($referee_id, $tournament_fixture_id)
	{
		$referee_name = '';
		$this->db->select('referee_type.referee_type_name');
		$this->db->where('tournament_fixture_referee.referee_id = '.$referee_id.' AND tournament_fixture_referee.tournament_fixture_id ='.$tournament_fixture_id.' AND tournament_fixture_referee.referee_type_id = referee_type.referee_type_id');
		$query = $this->db->get('tournament_fixture_referee,referee_type');
		if($query->num_rows() > 0)
		{
			$name = $query->row();
			$referee_name = $name->referee_type_name;
		}
		return $referee_name;
	}
	
}
?>