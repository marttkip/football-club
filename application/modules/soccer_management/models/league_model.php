<?php
class League_model extends CI_Model 
{
	public function all_league()
	{
		$this->db->where('deleted = 0');
		$query = $this->db->get('league');
		
		return $query;
	}
	public function add_league()
	{
		$data = array(
			'league_name'=>ucwords(strtolower($this->input->post('league_name'))),
			'created_by'=>$this->session->userdata('personnel_id'),
			'created_on' =>date('Y-m-d H:i:s'),
		);
		
		if($this->db->insert('league', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function edit_league($league_id)
	{
		$data = array(
			'league_name'=>ucwords(strtolower($this->input->post('league_name'))),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'last_modified' =>date('Y-m-d H:i:s'),
		);
		
		$this->db->where('league_id', $league_id);
		if($this->db->update('league', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_league($league_id)
	{
		$data = array(
				'league_status' => 1
			);
		$this->db->where('league_id', $league_id);
		

		if($this->db->update('league', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_league($league_id)
	{
		$data = array(
				'league_status' => 0
			);
		$this->db->where('league_id', $league_id);
		
		if($this->db->update('league', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_league($league_id)
	{
		//delete league
		$data = array(
				'league_deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('league_id', $league_id);
		
		if($this->db->update('league', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_league($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all leaguees
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	public function get_league($league_id)
	{
		$this->db->select('*');
		$this->db->where('league_id = '.$league_id);
		$query = $this->db->get('league');
		
		return $query;
	}
	public function get_all_league_durations($league_id)
	{
		$this->db->select('*');
		$this->db->where('league_duration_deleted = 0 AND league_id = '.$league_id);
		$query = $this->db->get('league_duration');
		
		return $query;
	}
	public function get_league_name($league_id)
	{
		$name = '';
		$this->db->select('league_name');
		$this->db->where('league_id = '.$league_id);
		$query = $this->db->get('league');
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $leaguename)
			{
				$name = $leaguename->league_name;
			}
		}
		return $name;
	}

	public function add_league_duration($league_id)
	{
		$season_data = array(
			'league_duration_end_date'=>$this->input->post('league_duration_end_date'),
			'league_duration_start_date'=>$this->input->post('league_duration_start_date'),
			'created' =>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'league_id'=>$league_id
			);
			if($this->db->insert('league_duration', $season_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
	}
	public function get_league_duration($league_duration_id)
	{
		$this->db->select('*');
		$this->db->where('league_duration_id = '.$league_duration_id);
		$query = $this->db->get('league_duration');
		return $query;
	}
	public function edit_league_duration($league_duration_id)
	{
		$season_updates = array(
				'league_duration_end_date'=>$this->input->post('league_duration_end_date'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'league_duration_start_date'=>$this->input->post('league_duration_start_date')
				);
		$this->db->where('league_duration_id', $league_duration_id);
		

		if($this->db->update('league_duration', $season_updates))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_league_duration($league_duration_id)
	{
		$data = array(
				'league_duration_status' => 1
			);
		$this->db->where('league_duration_id', $league_duration_id);
		

		if($this->db->update('league_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_league_duration($league_duration_id)
	{
		$data = array(
				'league_duration_status' => 0
			);
		$this->db->where('league_duration_id', $league_duration_id);
		

		if($this->db->update('league_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_league_duration($league_duration_id)
	{
		//delete season
		$data = array(
				'league_duration_deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('league_duration_id', $league_duration_id);
		
		if($this->db->update('league_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_league_duration_team($league_duration_id)
	{
		$team_id = $this->input->post('team_id');
		//var_dump($team_id);die();
		$league_team_data = array(
			'team_id'=>$this->input->post('team_id'),
			'created' =>date('Y-m-d H:i:s'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'league_duration_id'=>$league_duration_id
			);
			if($this->db->insert('league_team', $league_team_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
	}
	public function get_league_teams($league_duration_id)
	{
		$this->db->select('*');
		$this->db->where('league_team.league_team_deleted = 0 AND team.team_id = league_team.team_id AND league_duration_id = '.$league_duration_id);
		$query = $this->db->get('league_team, team');
		return $query;
	}
	public function all_league_teams($league_duration_id)
	{
		$this->db->select('*');
		$this->db->where('team.team_id NOT IN (SELECT league_team.team_id FROM league_team, team WHERE league_team.league_duration_id = '.$league_duration_id.' AND league_team.league_team_id = team.team_id)');
		$this->db->group_by('team.team_id');
		$query = $this->db->get('team');


		return $query;
	}
	public function remove_league_team($team_id,$league_duration_id)
	{
		$data = array(
				'league_team_deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('team_id = '.$team_id. ' AND league_duration_id = '.$league_duration_id);
		
		if($this->db->update('league_team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_season_teams($league_duration_id)
	{
		$this->db->select('team.team_name,team.team_id, league_team.*, league_duration.*');
		$this->db->where('team.team_id = league_team.team_id AND league_team.league_team_deleted = 0 AND league_team.league_duration_id = league_duration.league_duration_id AND league_duration.league_duration_id = '.$league_duration_id);
		$query = $this->db->get('league_duration, league_team, team');
		
		return $query;
	}
	public function get_all_fixture_players($team_id)
	{
		$this->db->select('player.*');
		$this->db->where('player.team_id = team.team_id AND team.team_id = '.$team_id);
		$query = $this->db->get('player, team');
		
		return $query;
	}
	public function get_team_name($team_id)
	{
		$this->db->select('team_name');
		$this->db->where('team_id = '.$team_id);
		$query = $this->db->get('team');
		$team_name = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$team_name = $name->team_name;
			}
		}
		return $team_name;
	}
	public function get_player_type($fixture_team_player_type)
	{
		$this->db->select('fixture_player_type_name');
		$this->db->where('fixture_player_type_id = '.$fixture_team_player_type);
		$query = $this->db->get('fixture_player_type');
		$player_type = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$player_type = $name->fixture_player_type_name;
			}
		}
		return $player_type;
	}
	public function get_fixture_teams($fixture_id)
	{
		$this->db->select('team.team_name,team.team_id');
		$this->db->where('fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = team.team_id AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team,league_team, team');
		
		
		return $query;
	}
	
	public function get_home_team($fixture_id)
	{
		$this->db->select('team.team_name');
		$this->db->where('fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = team.team_id AND fixture_team_type_id = 1 AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team,league_team, team');
		$home_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$home_team = $name->team_name;
			}
		}
		return $home_team;
	}
	public function get_home_players($fixture_id)
	{
		$home_team = 0;
		$this->db->select('fixture_team.fixture_team_id');
		$this->db->where('fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = team.team_id AND fixture_team_type_id = 1 AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team,league_team, team');
		$home_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$home_team = $name->fixture_team_id;
			}
		}
		//get all players forthat team
		$this->db->where('fixture_team_player.fixture_team_id = '.$home_team.' AND fixture_team_player.player_id = player.player_id AND fixture_team_player.fixture_team_player_type_id = fixture_player_type.fixture_player_type_id');
		$query = $this->db->get('fixture_player_type, fixture_team_player, player');
		
		return $query;
		
	}
	public function get_away_players($fixture_id)
	{
		$away = 0;
		$this->db->select('fixture_team.fixture_team_id');
		$this->db->where('fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = team.team_id AND fixture_team_type_id = 2 AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team,league_team, team');
		$home_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$away = $name->fixture_team_id;
			}
		}
		//get all players forthat team
		$this->db->where('fixture_team_player.fixture_team_id = '.$away.' AND fixture_team_player.player_id = player.player_id AND fixture_team_player.fixture_team_player_type_id = fixture_player_type.fixture_player_type_id');
		$query = $this->db->get('fixture_player_type, fixture_team_player, player');
		
		return $query;
		
	}
	public function get_away_team($fixture_id)
	{
		$this->db->select('team.team_name');
		$this->db->where('fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = team.team_id AND fixture_team_type_id = 2 AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team,league_team, team');
		$away_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$away_team = $name->team_name;
			}
		}
		return $away_team;
	}
	public function get_all_fixture_players_scorers($fixture_id)
	{
		$this->db->select('player.*, fixture_team_player.fixture_team_player_id');
		$this->db->where('fixture_team.fixture_team_id = fixture_team_player.fixture_team_id AND fixture_team_player.player_id = player.player_id AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team_player, player,fixture_team');
		return $query;
	}
	public function get_all_goals_scored($fixture_id)
	{
		$this->db->select('player.*,team.team_name,goal.goal_minute, goal_type.goal_type_name,goal.goal_scored, goal.goal_id');
		$this->db->where('goal.fixture_team_player_id = fixture_team_player.fixture_team_player_id AND fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND fixture_team.fixture_team_id = fixture_team_player.fixture_team_id AND fixture_team.fixture_id = '.$fixture_id.' AND goal_type.goal_type_id = goal.goal_type_id AND goal.goal_delete = 0');
		$query = $this->db->get('team,player,goal,fixture_team_player,fixture_team, goal_type');
		return $query;
	}

	public function get_all_goals_scored_commissioner($fixture_id)
	{
		$this->db->select('player.*,team.team_name,goal.goal_minute, goal_type.goal_type_name,goal.goal_scored');
		$this->db->where('goal.fixture_team_player_id = fixture_team_player.fixture_team_player_id AND fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND fixture_team.fixture_team_id = fixture_team_player.fixture_team_id AND fixture_team.fixture_id = '.$fixture_id.' AND goal_type.goal_type_id = goal.goal_type_id');
		$query = $this->db->get('team,player,commissioner_goal AS goal,fixture_team_player,fixture_team, goal_type');
		return $query;
	}
	public function get_all_penalty_scored($fixture_id)
	{
		//goal type _id 1 = penalty
		$this->db->select('player.*,team.team_name,goal.goal_minute, goal.goal_scored');
		$this->db->where('goal.fixture_team_player_id = fixture_team_player.fixture_team_player_id AND fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND fixture_team.fixture_team_id = fixture_team_player.fixture_team_id AND fixture_team.fixture_id = '.$fixture_id.' AND goal.goal_type_id = 1');
		$query = $this->db->get('team,player,goal,fixture_team_player,fixture_team');
		return $query;
	}
	public function get_all_foul_types()
	{
		$this->db->select('*');
		$this->db->where('foul_type_status = 1');
		$query = $this->db->get('foul_type');
		return $query;
	}
	public function get_all_action_types()
	{
		$this->db->select('*');
		$this->db->where('action_status = 1');
		$query = $this->db->get('action');
		return $query;
	}
	public function calculate_results($league_team_id)
	{
		$this->db->where('league_team_id = '.$league_team_id);
		$team_fixtures = $this->db->get('fixture_team');
		$wins = $draws = $losses = $goals_scored = $goals_against = 0;
		//echo $team_fixtures->num_rows();die();
		if($team_fixtures->num_rows() > 0)
		{
			//get opponents
			foreach($team_fixtures->result() as $fixtures_team)
			{
				$fixture_id = $fixtures_team->fixture_id;
				$this->db->where('league_team_id != '.$league_team_id.' AND fixture_id = '.$fixture_id);
				$opponent_fixture =$this->db->get('fixture_team');
				$team_goals = $this->calculate_fixture_goals($league_team_id);
				//var_dump($team_goals);die();
				$opponent_goals = 0;
				//echo $opponent_fixture->num_rows();die();
				if($opponent_fixture->num_rows() > 0)
				{
					$row = $opponent_fixture->row();
					$opponent_league_team_id = $row->league_team_id;
					//echo $opponent_league_team_id;die();
					$opponent_goals = $this->calculate_fixture_goals($opponent_league_team_id);
				}
				//var_dump($opponent_goals);die();
				//check wins
				if($team_goals > $opponent_goals)
				{
					$wins ++;
				}
				elseif($team_goals < $opponent_goals)
				{
					$losses++;
				}
				else
				{
					$draws++;
				}
				$goals_scored += $team_goals;
				$goals_against += $opponent_goals;
			}
		}
		$team_league_result = array(
			"wins"=>$wins,"losses"=>$losses,"draws"=>$draws,"goals_scored"=>$goals_scored,"goals_against"=>$goals_against);
			return $team_league_result;
	}
	
	public function calculate_fixture_goals($league_team_id)
	{
		$this->db->select('COUNT(goal_id) AS goals');
		$this->db-> where ('goal.fixture_team_player_id = fixture_team_player.fixture_team_player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.league_team_id = '.$league_team_id);
		$result = $this->db->get('goal, fixture_team_player, fixture_team');
		$goasl= 0;
		if($result->num_rows()>0)
		{
			$row = $result->row();
			$goals = $row->goals;
		}
		return $goals;
	}
	public function calculate_points($wins,$draws)
	{
		$wins_points = ($wins * 3) + $draws;
		return $wins_points;
	}
	public function get_season_fixture_teams($league_duration_id,$fixture_id)
	{
		$this->db->select('team.team_name,team.team_id, league_team.*');
		$this->db->where('league_team.team_id = team.team_id AND league_team.league_duration_id = '.$league_duration_id.' AND league_team.league_team_id NOT IN (SELECT league_team_id from fixture_team where fixture_id = '.$fixture_id.')');
		$query = $this->db->get('team,league_team ');
		
		return $query;
	}
	public function get_season_tournament_fixture_teams($league_duration_id,$fixture_id)
	{
		$this->db->select('team.team_name,team.team_id, tournament_team.*');
		$this->db->where('tournament_team.team_id = team.team_id AND tournament_team.tournament_duration_id = '.$league_duration_id.' AND tournament_team.tournament_team_id NOT IN (SELECT tournament_team_id from tournament_fixture_team where tournament_fixture_id = '.$fixture_id.')');
		$query = $this->db->get('team,tournament_team ');
		
		return $query;
	}
	public function get_fixture_referee($fixture_id)
	{
		$this->db->select('fixture_referee.*, referee.*, referee_type.referee_type_name');
		$this->db->where('fixture_referee.referee_id = referee.referee_id AND fixture_referee.referee_type_id = referee_type.referee_type_id AND fixture_referee.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_referee, referee, referee_type');
		return $query;
	}
	public function get_all_fixture_commissioners($fixture_id)
	{
		$this->db->select('personnel.personnel_fname,personnel.personnel_onames, fixture_commissioner.*');
		$this->db->where('personnel.personnel_id = fixture_commissioner.personnel_id AND fixture_commissioner.fixture_id = '.$fixture_id);
		$query = $this->db->get('personnel,fixture_commissioner');
		return $query;
	}
	public function get_referee_payment_status($referee_id,$tournament_fixture_id,$fixture_type)
	{
		$status = 0;
		$this->db->select('referee_payment_status');
		$this->db->where('referee_id = '.$referee_id.' AND fixture_type = '.$fixture_type.' AND tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('referee_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$status_row = $query->row();
			$status = $status_row->referee_payment_status;
		}
		
		return $status;
	}
	public function get_commissioner_payment_status($commissioner_id,$tournament_fixture_id,$fixture_type)
	{
		$status = 0;
		$this->db->select('commissioner_payment_status');
		$this->db->where('commissioner_id = '.$commissioner_id.' AND fixture_type = '.$fixture_type.' AND tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('commissioner_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$status_row = $query->row();
			$status = $status_row->commissioner_payment_status;
		}
		
		return $status;
	}
	function send_referee_to_admin($referee_id,$fixture_id)
	{
		//fixture_type  1 = league payments
		//insert details to the referee payments queue
		$referee_queue_data = array(
				'referee_id'=>$referee_id,
				'tournament_fixture_id'=>$fixture_id,
				'to_admin_sender'=>$this->session->userdata('personnel_id'),
				'to_admin_on'=>date('Y-m-d'),
				'referee_payment_status'=>0,
				'fixture_type'=>1
				);
		if($this->db->insert('referee_payments_queue', $referee_queue_data))
		{
			//update referee payment status to 1
			if($this->update_payment_to_admin($referee_id,$fixture_id,1))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	function send_commissioner_to_admin($commissioner_id,$fixture_id)
	{
		//fixture_type  1 = league payments
		//insert details to the referee payments queue
		$commissioner_queue_data = array(
				'commissioner_id'=>$commissioner_id,
				'tournament_fixture_id'=>$fixture_id,
				'to_admin_sender'=>$this->session->userdata('personnel_id'),
				'to_admin_on'=>date('Y-m-d'),
				'commissioner_payment_status'=>0,
				'fixture_type'=>1
				);
		if($this->db->insert('commissioner_payments_queue', $commissioner_queue_data))
		{
			//update referee payment status to 1
			if($this->update_commissioner_payment_to_admin($commissioner_id,$fixture_id,1))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	function update_payment_to_admin($referee_id,$fixture_id,$fixture_type)
	{
		$data = array(
				'referee_payment_status' => 1
			);
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = '.$fixture_type);
		
		if($this->db->update('referee_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function update_commissioner_payment_to_admin($commissioner_id,$fixture_id,$fixture_type)
	{
		$data = array(
				'commissioner_payment_status' => 1
			);
		$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = '.$fixture_type);
		
		if($this->db->update('commissioner_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function send_referee_to_accounts($referee_id,$fixture_id)
	{
		//update referee payment status to 2
		if($this->update_payment_to_accounts($referee_id,$fixture_id,1))
		{
			//insert details to the referee payments queue
			$referee_queue_data = array(
					'to_accounts_sender'=>$this->session->userdata('personnel_id'),
					'to_accounts_on'=>date('Y-m-d'),
					'referee_payment_status'=>2
					);
			//if($this->db->insert('referee_payments_queue', $referee_queue_data))
			$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = 1' );
			if($this->db->update('referee_payments_queue', $referee_queue_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}

	public function send_commissioner_to_accounts($commissioner_id,$fixture_id)
	{
		//update referee payment status to 2
		if($this->update_commissioner_payment_to_accounts($commissioner_id,$fixture_id,1,2))
		{
			//insert details to the referee payments queue
			$referee_queue_data = array(
					'to_accounts_sender'=>$this->session->userdata('personnel_id'),
					'to_accounts_on'=>date('Y-m-d'),
					'commissioner_payment_status'=>2
					);
			//if($this->db->insert('referee_payments_queue', $referee_queue_data))
			$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = 1' );
			if($this->db->update('commissioner_payments_queue', $referee_queue_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	function update_payment_to_accounts($referee_id,$fixture_id,$fixture_type)
	{
		$data = array(
				'referee_payment_status' => 2
			);
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = '.$fixture_type );
		

		if($this->db->update('referee_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function update_commissioner_payment_to_accounts($commissioner_id,$fixture_id,$fixture_type,$commissioner_payment_status = NULL)
	{
		$data = array(
				'commissioner_payment_status' => $commissioner_payment_status
			);
		$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = '.$fixture_type );
		

		if($this->db->update('commissioner_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function send_payments_to_referee($referee_id,$fixture_id)
	{
		//tournament fixture are type 2 and league type 1
		$fixture_type = 1;
		//update to 3
		if($this->update_payment_to_paid($referee_id, $fixture_id, $fixture_type))
		{
			//get the position of that referee for the fixture
			$referee_type_id = $this->get_referee_type($referee_id,$fixture_id);
			//get the amount to be paid for that referee type
			$referee_amount = $this->tournament_model->get_referee_amount($referee_type_id);
			
			$referee_payment_data = array(
					'referee_id'=>$referee_id,
					'tournament_fixture_id'=>$fixture_id,
					'amount_paid'=>$referee_amount,
					'paid_by'=>$this->session->userdata('personnel_id'),
					'paid_on'=>date('Y-m-d'),
					'fixture_type' =>$fixture_type 
					);
					
			if($this->db->insert('tournament_fixture_referee_payment',$referee_payment_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}

	public function send_payments_to_commissioner($commissioner_id,$fixture_id)
	{
		//tournament fixture are type 2 and league type 1
		$fixture_type = 1;
		//update to 3
		if($this->update_commissioner_payment_to_accounts($commissioner_id, $fixture_id, $fixture_type,3))
		{
			//get the position of that referee for the fixture
			// $referee_type_id = $this->get_referee_type($commissioner_id,$fixture_id);
			//get the amount to be paid for that referee type
			$referee_amount = $this->tournament_model->get_commissioner_amount(1);
			
			$referee_payment_data = array(
					'commissioner_id'=>$commissioner_id,
					'tournament_fixture_id'=>$fixture_id,
					'amount_paid'=>$referee_amount,
					'paid_by'=>$this->session->userdata('personnel_id'),
					'paid_on'=>date('Y-m-d'),
					'fixture_type' =>$fixture_type 
					);
					
			if($this->db->insert('tournament_fixture_commissioner_payment',$referee_payment_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	function get_referee_type($referee_id,$fixture_id)
	{
		$referee_type_id = '';
		$this->db->select('referee_type_id');
		$this->db->where('referee_id = '.$referee_id.' AND fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_referee');
		$type_id = $query->row();
		 
		$referee_type_id = $type_id->referee_type_id;
		return $referee_type_id; 
	}
	function update_payment_to_paid($referee_id, $fixture_id, $fixture_type)
	{
		$data = array(
				'referee_payment_status' => 3
			);
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = '.$fixture_type);
		

		if($this->db->update('referee_payments_queue', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_league_fixture_date($fixture_id)
	{
		$date = '';
		$this->db->select('fixture_date');
		$this->db->where('fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture')->row();
		
		$date = $query->fixture_date;
		return $date;
	}
	public function get_league_referee_payment_status($referee_id,$fixture_id,$fixture_type)
	{//echo $fixture_type;die();
		$status = 0;
		$this->db->select('referee_payment_status');
		$this->db->where('referee_id = '.$referee_id.' AND fixture_type = '.$fixture_type.' AND tournament_fixture_id = '.$fixture_id);
		$query = $this->db->get('referee_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$status_row = $query->row();
			$status = $status_row->referee_payment_status;
		}
		
		return $status;
	}

	public function get_league_commissioner_payment_status($commissioner_id,$fixture_id,$fixture_type)
	{//echo $fixture_type;die();
		$status = 0;
		$this->db->select('commissioner_payment_status');
		$this->db->where('commissioner_id = '.$commissioner_id.' AND fixture_type = '.$fixture_type.' AND tournament_fixture_id = '.$fixture_id);
		$query = $this->db->get('commissioner_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$status_row = $query->row();
			$status = $status_row->commissioner_payment_status;
		}
		
		return $status;
	}
	public function get_ref_invoice($tournament_fixture_id,$referee_id)
	{
		$invoice_total = 0;
		$this->db->select('SUM(amount_paid) AS total_invoice');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id.' AND referee_id = '.$referee_id.' AND fixture_type = 1');
		$query = $this->db->get('tournament_fixture_referee_payment');
		
		if($query->num_rows() > 0)
		{
			$invoice_row = $query->row();
			$invoice_total = $invoice_row->total_invoice;
		}
		return $invoice_total;
	}
	public function get_ref_commissioner_invoice($tournament_fixture_id,$commissioner_id)
	{
		//1 = league ,2 = torunament
		$invoice_total = 0;
		$this->db->select('SUM(amount_paid) AS total_invoice');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id.' AND commissioner_id = '.$commissioner_id.' AND fixture_type = 1');
		$query = $this->db->get('tournament_fixture_commissioner_payment');
		
		if($query->num_rows() > 0)
		{
			$invoice_row = $query->row();
			$invoice_total = $invoice_row->total_invoice;
		}
		return $invoice_total;
	}
	public function get_ref_payment($referee_payments_queue_id)
	{
		$payment_total = 0;
		$this->db->select('SUM(payment_amount) AS total_payment');
		$this->db->where('referee_payments_queue_id = '.$referee_payments_queue_id);
		$query = $this->db->get('referee_paid');
		
		if($query->num_rows() > 0)
		{
			$payment_row = $query->row();
			$payment_total = $payment_row->total_payment;
		}
		return $payment_total;
	}
	public function get_ref_summary($fixture_id)
	{
		$this->db->where('fixture_id = '.$fixture_id.' AND fixture_summary_deleted = 0');
		$query = $this->db->get('fixture_summary');
		
		return $query;
	}
	public function get_home_fixture_subs($fixture_id)
	{
		$this->db->where('subs.fixture_id = '.$fixture_id.' AND subs.player_in = player.player_id AND subs.sub_type = 1 AND sub_deleted = 0');
		$query = $this->db->get('subs,player');
		
		return $query;
	}
	public function get_away_fixture_subs($fixture_id)
	{
		$this->db->where('subs.fixture_id = '.$fixture_id.' AND subs.player_in = player.player_id AND subs.sub_type = 2 AND sub_deleted = 0');
		$query = $this->db->get('subs,player');
		
		return $query;
	}
	public function get_player_name($home_player_in)
	{
		$player_name = '';
		$this->db->where('player_id = '.$home_player_in);
		$query = $this->db->get('player');
		$row = $query->row();
		$player_fname = $row->player_fname;
		$player_onames = $row->player_onames;
		$player_name = $player_fname.' '.$player_onames;
		
		return $player_name;
	}
	public function get_fixture_date($tournament_fixture_id)
	{
		$date = '';
		$this->db->select('fixture_date');
		$this->db->where('fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('fixture')->row();
		
		$date = $query->fixture_date;
		return $date;
	}
	public function get_commissioner_ref_payment($commissioner_payments_queue_id)
	{
		$payment_total = 0;
		$this->db->select('SUM(payment_amount) AS total_payment');
		$this->db->where('commissioner_payments_queue_id = '.$commissioner_payments_queue_id);
		$query = $this->db->get('commissioner_paid');
		
		if($query->num_rows() > 0)
		{
			$payment_row = $query->row();
			$payment_total = $payment_row->total_payment;
		}
		return $payment_total;
	}
}
?>