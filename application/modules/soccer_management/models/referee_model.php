<?php
class Referee_model extends CI_Model 
{
	//all referees to display
	public function all_referee()
	{
		$this->db->where('deleted = 0');
		$query = $this->db->get('referee');
		
		return $query;
	}
	//active referees to add to fixtures
	public function get_all_referees()
	{
		$this->db->where('personnel_type_id = 3 AND personnel_status = 1');
		$query = $this->db->get('personnel');
		
		return $query;
	}
	public function add_referee()
	{
		$username = str_split($this->input->post('referee_fname'));
		$username1 = lcfirst($username[0]);
		$ronames = explode(' ',$this->input->post('referee_onames'));
		$username2 = lcfirst($ronames[0]);
		$data = array(
			'referee_onames'=>ucwords(strtolower($this->input->post('referee_onames'))),
			'referee_fname'=>ucwords(strtolower($this->input->post('referee_fname'))),
			'referee_dob'=>$this->input->post('referee_dob'),
			'referee_email'=>$this->input->post('referee_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'referee_phone'=>$this->input->post('referee_phone'),
			'civilstatus_id'=>$this->input->post('civil_status_id'),
			'referee_address'=>$this->input->post('referee_address'),
			'referee_locality'=>$this->input->post('referee_locality'),
			'title_id'=>$this->input->post('title_id'),
			'referee_number'=>$this->input->post('referee_number'),
			'referee_city'=>$this->input->post('referee_city'),
			'referee_post_code'=>$this->input->post('referee_post_code'),
			'referee_national_id_number'=>$this->input->post('referee_national_id_number'),
			'bank_account_number'=>$this->input->post('bank_account_number'),
			'bank_branch_code'=>$this->input->post('bank_branch_code'),
			'engagement_date'=>$this->input->post('engagement_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'referee_password' => md5('123456'),


			'educational_background'=>$this->input->post('educational_background'),
			'year_of_graduation_from'=>$this->input->post('year_of_graduation_from'),
			'accolades'=>$this->input->post('accolades'),
			'computer_literacy'=>$this->input->post('computer_literacy'),
			'occupation'=>$this->input->post('occupation'),
			'year_of_graduation_to'=>$this->input->post('year_of_graduation_to'),
			'created_on' =>date('Y-m-d H:i:s'),
			'year_qualified'=>$this->input->post('year_qualified'),
			'training_town'=>$this->input->post('training_town'),
			'training_county'=>$this->input->post('training_county'),
			'training_referee'=>$this->input->post('training_referee'),
			'futuro_instructor'=>$this->input->post('futuro_instructor'),
			'caf_instructor'=>$this->input->post('caf_instructor'),
			'fifa_instructor'=>$this->input->post('fifa_instructor'),
			'grades_attained'=>$this->input->post('grades_attained'),
			'referee_type'=>$this->input->post('referee_type'),
			'current_status'=>$this->input->post('current_status')
		);
		
		if($this->db->insert('referee', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function edit_referee($referee_id)
	{
		$data = array(
			'referee_onames'=>ucwords(strtolower($this->input->post('referee_onames'))),
			'referee_fname'=>ucwords(strtolower($this->input->post('referee_fname'))),
			'referee_dob'=>$this->input->post('referee_dob'),
			'referee_email'=>$this->input->post('referee_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'referee_phone'=>$this->input->post('referee_phone'),
			'civilstatus_id'=>$this->input->post('civil_status_id'),
			'referee_address'=>$this->input->post('referee_address'),
			'referee_locality'=>$this->input->post('referee_locality'),
			'title_id'=>$this->input->post('title_id'),
			'referee_number' => $this->input->post('referee_number'),
			'referee_city' => $this->input->post('referee_city'),
			'referee_post_code' => $this->input->post('referee_post_code'),
			'bank_account_number'=>$this->input->post('bank_account_number'),
			'bank_branch_code'=>$this->input->post('bank_branch_code'),
			'referee_national_id_number' => $this->input->post('referee_national_id_number'),
			'engagement_date'=>$this->input->post('engagement_date'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'educational_background'=>$this->input->post('educational_background'),
			'year_of_graduation_from'=>$this->input->post('year_of_graduation_from'),
			'accolades'=>$this->input->post('accolades'),
			'computer_literacy'=>$this->input->post('computer_literacy'),
			'occupation'=>$this->input->post('occupation'),
			'year_of_graduation_to'=>$this->input->post('year_of_graduation_to'),
			'year_qualified'=>$this->input->post('year_qualified'),
			'training_town'=>$this->input->post('training_town'),
			'training_county'=>$this->input->post('training_county'),
			'training_referee'=>$this->input->post('training_referee'),
			'futuro_instructor'=>$this->input->post('futuro_instructor'),
			'caf_instructor'=>$this->input->post('caf_instructor'),
			'fifa_instructor'=>$this->input->post('fifa_instructor'),
			'grades_attained'=>$this->input->post('grades_attained'),
			'referee_type'=>$this->input->post('referee_type'),
			'current_status'=>$this->input->post('current_status')
		);
		
		$this->db->where('referee_id', $referee_id);
		if($this->db->update('referee', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_referee($referee_id)
	{
		$data = array(
				'referee_status' => 1
			);
		$this->db->where('referee_id', $referee_id);
		

		if($this->db->update('referee', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_referee($referee_id)
	{
		$data = array(
				'referee_status' => 0
			);
		$this->db->where('referee_id', $referee_id);
		
		if($this->db->update('referee', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_referee($referee_id)
	{
		//delete referee
		$data = array(
				'deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('referee_id', $referee_id);
		
		if($this->db->update('referee', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_referee($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all refereees
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}

	public function get_all_referee_league($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all refereees
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	public function get_referee($referee_id)
	{
		$this->db->select('*');
		$this->db->where('personnel_id = '.$referee_id);
		$query = $this->db->get('personnel');
		
		return $query;
	}
	public function validate_user()
	{
		//select the user by email from the database
		$this->db->select('*');
		//$this->db->where(array('referee_username' => $this->input->post('personnel_username'), 'referee_password' => md5($this->input->post('personnel_password'))));
		$this->db->where(array('referee_national_id_number' => $this->input->post('personnel_username'), 'referee_password' => md5($this->input->post('personnel_password'))));
		$query = $this->db->get('referee');

		//if users exists
		if ($query->num_rows() > 0)
		{//die();
			$result = $query->result();
			//  check whether this personnel exisit in the personnel password item
			$personnel_id = $result[0]->referee_id;
			
			$newdata = array(
                   'login_status'     			=> TRUE,
                   'first_name'     			=> $result[0]->referee_fname,
                   'other_names'     			=> $result[0]->referee_onames,
                   'username'     				=> $result[0]->referee_username,
                   'personnel_id'  				=> $result[0]->referee_id,
                   'branch_code'  				=> $result[0]->branch_code,
                   'personnel_number'  			=> $result[0]->referee_number,
				   'referee_is_logged_in'		=>1
               );

			$this->session->set_userdata($newdata);
			
			//update personnel's last login date time
			//var_dump($newdata); die();
			$personnel_id = $this->session->userdata('personnel_id');
			$referee_is_logged_in = $this->session->userdata('referee_is_logged_in');
			return TRUE;
		}
		
		//if user doesn't exist
		else
		{
			return FALSE;
		}
	}
	public function get_all_referee_games($referee_id)
	{
		$this->db->select('*');
		$this->db->where('referee_id = '.$referee_id);
		$query = $this->db->get('fixture_referee');
		
		return $query;
	}
	public function get_all_tournament_referee_games($referee_id)
	{
		$this->db->select('*');
		$this->db->where('referee_id = '.$referee_id);
		$query = $this->db->get('tournament_fixture_referee');
		
		return $query;
	}
	public function get_league_duraion($fixture_id)
	{
		$league_duration_id = 0;
		$this->db->select('league_duration_id');
		$this->db->where('fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture')->row();
		
		$league_duration_id = $query->league_duration_id;
		return $league_duration_id;
	}
	
	public function get_fixture_detail($fixture_id)
	{
		$league_duration_id = 0;
		$this->db->select('*');
		$this->db->where('fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture');
		
		return $query;
	}
	public function get_fixture_detail_commissioner($fixture_id)
	{
		$league_duration_id = 0;
		$this->db->select('*');
		$this->db->where('fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture');
		
		return $query;
	}
	public function get_tournament_duration($tournament_fixture_id)
	{
		$tournament_duration_id = 0;
		$this->db->select('tournament_duration_id');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture')->row();
		
		$tournament_duration_id = $query->tournament_duration_id;
		return $tournament_duration_id;
	}
	public function get_league($league_duraion_id)
	{
		$league_id = 0;
		$this->db->select('league_id');
		$this->db->where('league_duration_id = '.$league_duraion_id);
		$query = $this->db->get('league_duration')->row();
		
		$league_id = $query->league_id;
		return $league_id;
	}
	public function get_tournament($tournament_duration_id)
	{
		$tournament_id = 0;
		$this->db->select('tournament_id');
		$this->db->where('tournament_duration_id = '.$tournament_duration_id);
		$query = $this->db->get('tournament_duration')->row();
		
		$tournament_id = $query->tournament_id;
		return $tournament_id;
	}
	public function get_tournament_name($tournament_id)
	{
		$name = '';
		$this->db->select('tournament_name');
		$this->db->where('tournament_id = '.$tournament_id);
		$query = $this->db->get('tournament');
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $leaguename)
			{
				$name = $leaguename->tournament_name;
			}
		}
		return $name;
	}
	public function get_first_half_goals($minute,$fixture_id)
	{
		$this->db->select('*');
		$this->db->where('goal_minute <= "'.$minute.'" AND fixture_id = '.$fixture_id.' AND goal_delete = 0');
		$number = $this->db->get('goal')->num_rows();
		return $number;
	}
	public function get_tournament_first_half_goals($minute,$fixture_id)
	{
		$this->db->select('*');
		$this->db->where('goal_minute <= "'.$minute.'" AND tournament_fixture_id = '.$fixture_id);
		$number = $this->db->get('tournament_goal')->num_rows();
		return $number;
	}
	public function get_second_half_goals($minute,$fixture_id,$last_minute)
	{
		$this->db->select('*');
		$this->db->where('goal_minute > "'.$minute.'" AND goal_minute <= "'.$last_minute.'" AND fixture_id = '.$fixture_id.' AND goal_delete = 0');
		$number = $this->db->get('goal')->num_rows();
		return $number;
	}
	public function get_penalties_goals($fixture_id,$last_minute)
	{
		$this->db->select('*');
		$this->db->where('goal_minute > "'.$last_minute.'" AND fixture_id = '.$fixture_id.' AND goal_delete = 0');
		$number = $this->db->get('goal')->num_rows();
		return $number;
	}
	public function get_players_of_team($team_id,$fixture_id)
	{
		$this->db->select('player.*,fixture_team_player.fixture_team_player_id');
		$this->db->where('fixture_team.fixture_id = '.$fixture_id.' AND fixture_team.fixture_team_id = fixture_team_player.fixture_team_id AND fixture_team_player.player_id = player.player_id AND player.team_id = '.$team_id.' ');
		$query = $this->db->get('fixture_team, fixture_team_player,player');
		return $query;
	}
	public function get_tournament_players_of_team($team_id,$fixture_id)
	{
		$this->db->select('player.*,tournament_fixture_team_player.tournament_fixture_team_player_id');
		$this->db->where('tournament_fixture_team.tournament_fixture_id = '.$fixture_id.' AND tournament_fixture_team.tournament_fixture_team_id = tournament_fixture_team_player.tournament_fixture_team_id AND tournament_fixture_team_player.player_id = player.player_id ');
		$query = $this->db->get('tournament_fixture_team, tournament_fixture_team_player,player');
		return $query;
	}

	function get_all_venue($where, $table)
	{
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function add_venue()
	{
		$data = array(
						'venue_name'=>$this->input->post('venue_name'),
						'venue_location'=>$this->input->post('venue_location'),
						'venue_longitude'=>$this->input->post('venue_longitude'),
						'venue_latitude'=>$this->input->post('venue_latitude'),
						'venue_status'=>1,
						'venue_deleted'=>0
					);
		if($this->db->insert('venue', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function edit_venue($venue_id)
	{
		$data = array(
						'venue_name'=>$this->input->post('venue_name'),
						'venue_location'=>$this->input->post('venue_location'),
						'venue_longitude'=>$this->input->post('venue_longitude'),
						'venue_latitude'=>$this->input->post('venue_latitude'),
						'venue_status'=>1,
						'venue_deleted'=>0
					);
		$this->db->where('venue_id = '.$venue_id);
		if($this->db->update('venue', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_venue($venue_id)
	{
		$data = array(
				'venue_status' => 1
			);
		$this->db->where('venue_id', $venue_id);
		

		if($this->db->update('venue', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_venue($venue_id)
	{
		$data = array(
				'venue_status' => 0
			);
		$this->db->where('venue_id', $venue_id);
		
		if($this->db->update('venue', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_venue($venue_id)
	{
		//delete referee
		$data = array(
				'venue_deleted' => 1,
				'last_modified' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('venue_id', $venue_id);
		
		if($this->db->update('venue', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	function get_venue($venue_id)
	{
		$this->db->where('venue_id = '.$venue_id);
		$query = $this->db->get('venue');
		
		return $query;
	}

	public function get_tournament_home_team($fixture_id)
	{
		$this->db->select('team.team_name');
		$this->db->where('tournament_fixture_team.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.team_id = team.team_id AND tournament_fixture_team_type_id = 1 AND tournament_fixture_team.tournament_fixture_id = '.$fixture_id);
		$query = $this->db->get('tournament_fixture_team,tournament_fixture_team, team');
		$home_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$home_team = $name->team_name;
			}
		}
		return $home_team;
	}
	public function delete_fixture_player($player_fixture_id)
	{
		$data = array('fixture_team_player_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d H-i-s'));
		$this->db->where('fixture_team_player_id = '.$player_fixture_id);
		if($this->db->update('fixture_team_player', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>