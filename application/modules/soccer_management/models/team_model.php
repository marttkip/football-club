<?php
class Team_model extends CI_Model 
{
	public function all_teams()
	{
		$this->db->where('team_deleted = 0 AND team_type = 1');
		$query = $this->db->get('team');
		
		return $query;
	}
	public function get_team($team_id)
	{
		$this->db->where('team_id = '.$team_id);
		$query = $this->db->get('team');
		
		return $query;
	}
	public function get_all_team($table, $where, $config, $page, $order, $order_method)
	{
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	public function activate_team($team_id)
	{
		$data = array(
				'team_status' => 1
			);
		$this->db->where('team_id', $team_id);
		

		if($this->db->update('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_team($team_id)
	{
		$data = array(
				'team_status' => 0
			);
		$this->db->where('team_id', $team_id);
		
		if($this->db->update('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_team($team_id)
	{
		//delete team
		$data = array(
				'team_deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('team_id', $team_id);
		
		if($this->db->update('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function upload_team_logo($team_logo_path, $edit = NULL)
	{
		//upload product's gallery images
		$resize['width'] = 500;
		$resize['height'] = 500;
		
		if(!empty($_FILES['logo_image']['tmp_name']))
		{
			$image = $this->session->userdata('team_logo_name');
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				//delete any other uploaded image
				$this->file_model->delete_file($team_logo_path."\\".$image, $team_logo_path);
				
				//delete any other uploaded thumbnail
				$this->file_model->delete_file($team_logo_path."\\thumbnail_".$image, $team_logo_path);
			}
			//Upload image
			$response = $this->file_model->upload_file($team_logo_path, 'logo_image', $resize, 'height');
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name']; 
				//Set sessions for the image details
				$this->session->set_userdata('team_logo_name', $file_name);
				$this->session->set_userdata('team_thumb_name', $thumb_name);
			
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('team_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('team_error_message', '');
			return FALSE;
		}
	}
	
	public function edit_team($team_id)
	{
		$data2 = array(
			'team_name'=>$this->input->post("team_name"),
			'team_venue'=>$this->input->post("team_venue"),
			'team_logo'=>$logo_name
		);
		$this->db->where('team_id', $team_id);
		if($this->db->update('team', $data2))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_team_name($team_id)
	{
		$this->db->select('team_name');
		$this->db->where('team_id = '.$team_id);
		$query = $this->db->get('team');
		$team_name  = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$team_name = $row->team_name;
			}
		}
		return $team_name;
	}
	function team_template()
	{
		$this->load->library('Excel');
		
		$title = 'Team Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Team Name';
		$report[$row_count][1] = 'Team Venue';
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	public function import_csv_teams($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_team_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_team_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		$comment = ''; //var_dump($total_columns,$total_rows);die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 2))
		{
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Name</th>
						  <th>Venue</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$items['team_name'] = mysql_real_escape_string(ucwords(strtolower($array[$r][0])));
				$items['team_venue'] = mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
				$team_name =  mysql_real_escape_string(ucwords(strtolower($array[$r][0])));
				$team_venue =  mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
				if(!empty($team_name))
				{
					// check if the team already exists
					if($this->check_current_team_exisits($team_name,$team_venue))
					{

						//check fields aren't empty
						if(!empty($array[$r][0])){
							$items['team_name'] = mysql_real_escape_string(ucwords(strtolower($array[$r][0])));
						}
						if(!empty($array[$r][1])){
							$items['team_venue'] = mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
						}
						$items['created_by'] = $this->session->userdata('personnel_id');
						$items['modified_by'] = $this->session->userdata('personnel_id');
						$items['created'] = date('Y-m-d H-i-s');
						
						//update the individual details
						$this->db->where('team_name = "'.$team_name.'" AND team_venue = "'.$team_venue.'"');
						if($this->db->update('team',$items))
						{
							//number exists
							$comment .= '<br/>Duplicate team name and venue entered, details updated';
							$class = 'warning';
						}
						else
						{
							$comment .= '<br/>Internal error. Could not udpate team. Please contact the site administrator';
							$class = 'danger';
						}

					}
					else
					{
						//echo 'doesnt exist';die();
						// number does not exisit
						//save product in the db
						if($this->db->insert('team', $items))
						{
							$comment .= '<br/>Team successfully added to the database';
							$class = 'success';
						}
						
						else
						{
							$comment .= '<br/>Internal error. Could not add team to the database. Please contact the site administrator';
							$class = 'warning';
						}
					}
				}
				
				else
				{
					$comment .= '<br/>Not saved ensure you have a team name entered';
					$class = 'danger';
				}
				
				
				$response .= '
					
						<tr class="'.$class.'">
							<td>'.$r.'</td>
							<td>'.$items['team_name'].'</td>
							<td>'.$items['team_venue'].'</td>
							<td>'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	function check_current_team_exisits($team_name,$team_venue)
	{
		$this->db->where('team_name = "'.$team_name.'" AND team_venue = "'.$team_venue.'"');
		
		$query = $this->db->get('team');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
}
?>