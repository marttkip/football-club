<?php
class Soccer_management_model extends CI_Model 
{
	public function get_youth_league()
	{
		$this->db->select('*');
		$this->db->where('league_type = 2 AND league_deleted = 0');
		$query = $this->db->get('league');
		
		return $query;
	}
	public function add_youth_league()
	{
		$data = array(
			'league_name'=>ucwords(strtolower($this->input->post('league_name'))),
			'created_by'=>$this->session->userdata('personnel_id'),
			'created_on' =>date('Y-m-d H:i:s'),
			'league_type'=>2
		);
		
		if($this->db->insert('league', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function get_league_type($league_id)
	{
		$league_type_id = 1;
		$this->db->select('league_type');
		$this->db->where('league_id = '.$league_id);
		$query = $this->db->get('league');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$league_type_id = $row->league_type;
			}
		}
		return $league_type_id;
	}
	public function all_league_teams($league_duration_id)
	{
		$this->db->select('*');
		$this->db->where('team.team_type = 2 AND team.team_id NOT IN (SELECT league_team.team_id FROM league_team, team WHERE league_team.league_duration_id = '.$league_duration_id.' AND league_team.league_team_id = team.team_id)');
		$this->db->group_by('team.team_id');
		$query = $this->db->get('team');


		return $query;
	}
	public function get_youth_league_teams()
	{
		$this->db->where('team_deleted = 0 AND team_type = 2');
		$query = $this->db->get('team');
		
		return $query;
	}
	public function add_player_guardian($player_id)
	{
		$player_guardian_fname = $this->input->post('player_guardian_fname');
		$player_guardian_surname = $this->input->post('player_guardian_surname');
		$player_guardian = array(
							'player_id'=>$player_id,
							'player_guardian_name'=>$player_guardian_fname.' '.$player_guardian_surname,
							'player_guardian_email'=>$this->input->post('player_guardian_email'),
							'player_guardian_phone_no'=>$this->input->post('player_guardian_phone_nu'),
							);
		if($this->db->insert('player_guardian',$player_guardian))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function add_player_school($player_id)
	{
		$player_school = array(
							'player_id'=>$player_id,
							'teacher_name'=>$this->input->post('teacher_name'),
							'teacher_email'=>$this->input->post('teacher_email'),
							'teacher_phone_no'=>$this->input->post('teacher_phone_nu'),
							'school_name'=>$this->input->post('school_name'),
							'branch'=>$this->input->post('branch'),
							'sub_branch'=>$this->input->post('sub_branch')
							);
		if($this->db->insert('player_school',$player_school))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>