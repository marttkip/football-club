<?php
class Player_model extends CI_Model 
{
	var $csv_path;
	
	function __construct()
	{
		parent:: __construct();
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
	public function all_player()
	{
		$this->db->where('deleted = 0');
		$query = $this->db->get('player');
		
		return $query;
	}
	public function create_licence_number()
	{
		//select product code
		$this->db->from('player');
		$this->db->select('MAX(licence_number) AS number');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number = substr($number, 3);
			$number = intval($number);
			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}
		$number = sprintf('%06d', $number);
		return 'FKF'.$number;
	}
	public function add_player($file_name)
	{
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			$player_type = 2;
		}
		else
		{
			$player_type = 1;
		}
		$data = array(
			'player_onames'=>ucwords(strtolower($this->input->post('player_onames'))),
			'player_fname'=>ucwords(strtolower($this->input->post('player_fname'))),
			'player_dob'=>$this->input->post('player_dob'),
			'player_position'=>$this->input->post('player_position'),
			'player_email'=>$this->input->post('player_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'team_id'=>$this->input->post('team_id'),
			'player_phone'=>$this->input->post('player_phone'),
			'player_address'=>$this->input->post('player_address'),
			'player_locality'=>$this->input->post('player_locality'),
			'title_id'=>$this->input->post('title_id'),
			'licence_number'=>$this->create_licence_number(),
			'player_city'=>$this->input->post('player_city'),
			'player_post_code'=>$this->input->post('player_post_code'),
			'player_national_id_number'=>$this->input->post('player_national_id_number'),
			'engagement_date'=>$this->input->post('engagement_date'),
			'contract_end_date'=>$this->input->post('contract_end_date'),
			'player_contract_value'=>$this->input->post('player_contract_value'),
			'bank_account_number'=>$this->input->post('bank_account_number'),
			'bank_branch_code'=>$this->input->post('bank_branch_code'),
			'player_duration'=>$this->input->post('player_duration'),
			//'licence_number'=>$this->input->post('license_number'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'player_image'=>$file_name,
			'created_on' =>date('Y-m-d H:i:s'),
			'player_type' =>$player_type

		);
		
		if($this->db->insert('player', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function edit_player($player_id,$file_name)
	{
		$data = array(
			'player_onames'=>ucwords(strtolower($this->input->post('player_onames'))),
			'player_fname'=>ucwords(strtolower($this->input->post('player_fname'))),
			'player_dob'=>$this->input->post('player_dob'),
			'player_position'=>$this->input->post('player_position'),
			'player_email'=>$this->input->post('player_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'team_id'=>$this->input->post('team_id'),
			'player_image'=>$file_name,
			'player_phone'=>$this->input->post('player_phone'),
			'player_address'=>$this->input->post('player_address'),
			'player_locality'=>$this->input->post('player_locality'),
			'title_id'=>$this->input->post('title_id'),
			//'licence_number' => $this->input->post('licence_number'),
			'player_city' => $this->input->post('player_city'),
			'player_post_code' => $this->input->post('player_post_code'),
			'player_national_id_number' => $this->input->post('player_national_id_number'),
			'bank_account_number'=>$this->input->post('bank_account_number'),
			'bank_branch_code'=>$this->input->post('bank_branch_code'),
			'player_contract_value'=>$this->input->post('player_contract_value'),
			'player_duration'=>$this->input->post('player_duration'),
			'engagement_date'=>$this->input->post('engagement_date'),
			'contract_end_date'=>$this->input->post('contract_end_date'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		$this->db->where('player_id', $player_id);
		if($this->db->update('player', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_player($player_id)
	{
		$data = array(
				'player_status' => 1
			);
		$this->db->where('player_id', $player_id);
		

		if($this->db->update('player', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_player($player_id)
	{
		$data = array(
				'player_status' => 0
			);
		$this->db->where('player_id', $player_id);
		
		if($this->db->update('player', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_player($player_id)
	{
		//delete player
		$data = array(
				'deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('player_id', $player_id);
		
		if($this->db->update('player', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_player($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all playeres
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	public function get_player($player_id)
	{
		$this->db->select('*');
		$this->db->where('player_id = '.$player_id);
		$query = $this->db->get('player');
		
		return $query;
	}
	function upload_personnel_documents($player_id, $document)
	{
		$data = array(
			'document_name'=> $this->input->post('document_item_name'),
			'document_upload_name'=> $document,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'player_id'=>$player_id
		);
		
		if($this->db->insert('player_document_uploads', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	function get_player_uploads($player_id)
	{
		$this->db->select('*');
		$this->db->where('player_id = '.$player_id);
		$query = $this->db->get('player_document_uploads');
		
		return $query;
	}
	function player_template()
	{
		$this->load->library('Excel');
		
		$title = 'Player Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Player FName';
		$report[$row_count][1] = 'Player Other names';
		$report[$row_count][2] = 'Jersey name';
		$report[$row_count][3] = 'Date of Birth (i.e. YYYY-MM-DD)';
		$report[$row_count][4] = 'Gender (F/M)';
		$report[$row_count][5] = 'ID Number/Passport Number';
		$report[$row_count][6] = 'Bank Account number';
		$report[$row_count][7] = 'Licence number';
		$report[$row_count][8] = 'Contract Start Date (i.e. YYYY-MM-DD)';
		$report[$row_count][9] = 'Contract End Date (i.e. YYYY-MM-DD)';
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	public function import_csv_players($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_player_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_player_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		$comment = '';
		$team_id = $this->input->post('team_id');
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 10))
		{
			$branch_id = $this->input->post('branch_id');
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Licence Number</th>
						  <th>First Name</th>
						  <th>Other Names</th>
						  <th>Licence NUmber</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$current_licence_number = $items['licence_number'] = $this->create_licence_number();;
				$items['player_fname'] = mysql_real_escape_string(ucwords(strtolower($array[$r][0])));
				$items['player_onames'] = mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
				$items['jersey_name'] = mysql_real_escape_string(ucwords(strtolower($array[$r][2])));
				$items['team_id'] = $team_id;
				if(!empty($current_licence_number))
				{
					//echo $current_individual_number;die();
					// check if the number already exists
					if($this->check_current_player_exisits($current_licence_number))
					{

						//check fields aren't empty
						if(!empty($array[$r][0])){
							$items['player_fname'] = mysql_real_escape_string(ucwords(strtolower($array[$r][0])));
						}
						if(!empty($array[$r][1])){
							$items['player_onames'] = mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
						}
						if(!empty($array[$r][2])){
							$items['jersey_name'] = mysql_real_escape_string(ucwords(strtolower($array[$r][2])));
						}
						
						if(!empty($array[$r][3])){
						$items['player_dob'] = date('Y-m-d',strtotime($array[$r][3]));
						}
						$gender = $array[$r][4];
						if(isset($gender))
						{
							if($gender == 'M')
							{
								$items['gender_id'] = 1;
							}
							else if($gender == 'F')
							{
								$items['gender_id'] = 2;
							}else
							{
								$gender_id = '';
							}
						}
						
						if(!empty($array[$r][5])){
						$items['player_national_id_number'] = $array[$r][5];
						}
						if(!empty($array[$r][6])){
						$items['bank_account_number'] = $array[$r][6];
						}
						//$items['licence_number'] = $this->create_licence_number();
						$items['modified_by'] = $this->session->userdata('personnel_id');
						if(!empty($array[$r][8])){
						$items['contract_start_date'] = date('Y-m-d',strtotime($array[$r][8]));
						}
						if(!empty($array[$r][9])){
						$items['contract_end_date'] = date('Y-m-d',strtotime($array[$r][9]));
						}
						
						//update the individual details
						$this->db->where('licence_number = "'.$current_licence_number.'"');
						if($this->db->update('player',$items))
						{
							//number exists
							$comment .= '<br/>Duplicate member number entered, details updated';
							$class = 'warning';
						}
						else
						{
							$comment .= '<br/>Internal error. Could not udpate member. Please contact the site administrator';
							$class = 'danger';
						}

					}
					else
					{
						//echo 'doesnt exist';die();
						// number does not exisit
						//save product in the db
						if($this->db->insert('player', $items))
						{
							$comment .= '<br/>Member successfully added to the database';
							$class = 'success';
						}
						
						else
						{
							$comment .= '<br/>Internal error. Could not add member to the database. Please contact the site administrator';
							$class = 'warning';
						}
					}
				}
				
				else
				{
					$comment .= '<br/>Not saved ensure you have a member number entered';
					$class = 'danger';
				}
				
				
				$response .= '
					
						<tr class="'.$class.'">
							<td>'.$r.'</td>
							<td>'.$items['player_fname'].'</td>
							<td>'.$items['player_onames'].'</td>
							<td>'.$items['jersey_name'].'</td>
							<td>'.$items['licence_number'].'</td>
							<td>'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	public function check_current_player_exisits($licence_number)
	{
		$this->db->where('licence_number = "'.$licence_number.'"');
		
		$query = $this->db->get('player');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
}
?>