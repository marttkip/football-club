<?php
class Payments_model extends CI_Model 
{
	//tournament referee payments
	public function get_payment_referees($table, $where, $config, $page, $order, $order_method)
	{
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	//tournament referee payments
	public function get_payment_commissioners($table, $where, $config, $page, $order, $order_method)
	{
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
	public function get_payment_amount($referee_type)
	{
		$amount = 0;
		$this->db->select('referee_payment_amount');
		$this->db->where('referee_type_id = '.$referee_type);
		$query = $this->db->get('referee_payment')->row();
		
		$amount = $query->referee_payment_amount;
		return $amount;
	}
	public function get_commissioner_payment_amount($commissioner_type)
	{
		$amount = 0;
		$this->db->select('commissioner_payment_amount');
		$this->db->where('commissioner_type_id = '.$commissioner_type);
		$query = $this->db->get('commissioner_payment');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$amount = $row->commissioner_payment_amount;
		}
		return $amount;
	}
	public function get_referee_role($referee_type)
	{
		$referee_role = '';
		$this->db->select('referee_type_name');
		$this->db->where('referee_type_id = '.$referee_type);
		$query = $this->db->get('referee_type')->row();
		
		$referee_role = $query->referee_type_name;
		return $referee_role;
	}

	public function get_fixture_date($tournament_fixture_id)
	{
		$date = '';
		$this->db->select('tournament_fixture_date');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture')->row();
		
		$date = $query->tournament_fixture_date;
		return $date;
	}
	public function get_queue_id($referee_id, $tournament_fixture_id)
	{
		$queue_id = 0;
		$this->db->select('referee_payments_queue_id');
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = 2');
		$query = $this->db->get('referee_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$queue_row = $query->row();
			$queue_id = $queue_row->referee_payments_queue_id;
		}
		return $queue_id;
	}
	public function get_commissioner_queue_id($commissioner_id, $tournament_fixture_id)
	{
		$queue_id = 0;
		$this->db->select('commissioner_payments_queue_id');
		$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = 2');
		// fixture_type = 2
		$query = $this->db->get('commissioner_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$queue_row = $query->row();
			$queue_id = $queue_row->commissioner_payments_queue_id;
		}
		// var_dump($tournament_fixture_id); die();
		return $queue_id;
	}
	public function get_league_queue_id($referee_id, $fixture_id)
	{
		$queue_id = 0;
		$this->db->select('referee_payments_queue_id');
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = 1');
		$query = $this->db->get('referee_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$queue_row = $query->row();
			$queue_id = $queue_row->referee_payments_queue_id;
		}
		return $queue_id;
	}
	public function get_commissioner_league_queue_id($commissioner_id, $fixture_id)
	{
		$queue_id = 0;
		$this->db->select('commissioner_payments_queue_id');
		$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$fixture_id.' AND fixture_type = 1');
		$query = $this->db->get('commissioner_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$queue_row = $query->row();
			$queue_id = $queue_row->commissioner_payments_queue_id;
		}
		return $queue_id;
	}
	public function get_league_payment_referees()
	{
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			$this->db->select('referee.*, fixture_referee.*');
			$this->db->where('referee.referee_id = fixture_referee.referee_id AND fixture_referee.fixture_id = fixture.fixture_id  AND fixture.league_duration_id = league_duration.league_duration_id AND league_duration.league_id = league.league_id AND league.league_type = 2');
			$query = $this->db->get('referee, fixture_referee, fixture, league_duration, league');
		}
		else
		{
			$this->db->select('*');
			$this->db->where('referee.referee_id = fixture_referee.referee_id');
			$query = $this->db->get('referee, fixture_referee');
		}
		return $query;
	}

	public function get_league_payment_commissioners()
	{
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			$this->db->select('personnel.*, fixture_commissioner.*');
			$this->db->where('personnel.personnel_id = fixture_commissioner.personnel_id AND fixture_commissioner.fixture_id = fixture.fixture_id  AND fixture.league_duration_id = league_duration.league_duration_id AND league_duration.league_id = league.league_id AND league.league_type = 2');
			$query = $this->db->get('personnel, fixture_commissioner, fixture, league_duration, league');
		}
		else
		{
			$this->db->select('*');
			$this->db->where('personnel.personnel_id = fixture_commissioner.personnel_id');
			$query = $this->db->get('personnel, fixture_commissioner');
		}
		return $query;
	}
	public function get_tournament_payment_referees()
	{
		$this->db->select('*');
		$this->db->where('referee.referee_id = tournament_fixture_referee.referee_id');
		$query = $this->db->get('referee, tournament_fixture_referee');
		
		return $query;
		
	}
}
?>