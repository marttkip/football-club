<?php
$league_standing = '';
$array = $array_sorted = array();
if($teams->num_rows() > 0)
{
	//echo $teams->num_rows();die();
	$league_standing .='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>P</th>
						<th>W</th>
						<th>D</th>
						<th>L</th>
						<th>GF</th>
						<th>GA</th>
						<th>GD</th>
						<th>Pts</th>
					</tr>
				</thead>
				<tbody>'; 
	$r =0;
	foreach($teams->result() as $team_points)
	{
		$tournament_team_id  = $team_points->tournament_team_id;
		$team_name = $team_points->team_name;
		//results
		$results = $this->tournament_model->calculate_results($tournament_team_id);
		$wins = $results['wins'];
		$losses = $results['losses'];
		$draws= $results['draws'];
		$goals_against= $results['goals_against'];
		$goals_scored= $results['goals_scored'];
		$points = $this->league_model->calculate_points($wins,$draws);
		
		$array[$r]['team_name'] = $team_name;
		$array[$r]['played'] = $wins + $losses + $draws;
		$array[$r]['wins'] = $wins;
		$array[$r]['draws'] = $draws;
		$array[$r]['losses'] = $losses;
		$array[$r]['goals_scored']= $goals_scored;
		$array[$r]['goals_against'] = $goals_against;
		$array[$r]['goal_difference'] = $goals_scored - $goals_against;
		$array[$r]['points'] = $points;
		$r++;
	}
	//var_dump($array);die();
	$total_teams = count($array);
	$greatest = $last = $count =$last_goal_difference  = $last_goals_against =$last_goals_scored = 0;
	
	while($count < $total_teams)
	{
		for($p = 0;$p<$total_teams;$p++)
		{
			$current= $array[$p]['points'];
			$current_goal_difference = $array[$p]['goal_difference'];
			$current_goals_against = $array[$p]['goals_against'];
			$current_goals_scored = $array[$p]['goals_scored'];
			//var_dump($current);die();
			if($p!=0)
			{
				if($last < $current)
				{
					$s = $p-1;
					$temp = $array[$s];
					$array[$s] = $array[$p];
					$array[$p] = $temp;
				}
				
				elseif(($last == $current)&&($current_goal_difference == $last_goal_difference))
				{
					if($current_goals_against < $last_goals_against)
					{
						$s = $p-1;
						$temp = $array[$s];
						$array[$s] = $array[$p];
						$array[$p] = $temp;
					}
				}
				elseif(($last == $current) &&($current_goal_difference != $last_goal_difference))
				{
					if($current_goal_difference > $last_goal_difference)
					{
						$s = $p-1;
						$temp = $array[$s];
						$array[$s] = $array[$p];
						$array[$p] = $temp;
					}
				}
			}
			$last = $current;
			$last_goal_difference = $current_goal_difference;
			$last_goals_scored  = $current_goals_scored;
			$last_goals_against = $current_goals_against;
		}
		
		$count++;
	}
	for($r = 0;$r<$total_teams;$r++)
	{
		$team_name = $array[$r]['team_name'];
		$wins = $array[$r]['wins'];
		$draws = $array[$r]['draws'];
		$losses = $array[$r]['losses'];
		$played = $wins + $losses + $draws;
		$goals_scored = $array[$r]['goals_scored'];
		$goals_against = $array[$r]['goals_against'];
		$goal_difference = $goals_scored - $goals_against;
		$points = $array[$r]['points'];
		$count = $r + 1;
		$league_standing .=
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$played.'</td>
					<td>'.$wins.'</td>
					<td>'.$draws.'</td>
					<td>'.$losses.'</td>
					<td>'.$goals_scored.'</td>
					<td>'.$goals_against.'</td>
					<td>'.$goal_difference.'</td>
					<td>'.$points.'</td>
				</tr>
			';
	}
	$league_standing .=
			'
				</tbody>
			</table>
			';
}
?>
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">League Standings</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $league_standing;?>
        </div> 
    </div>
</section>