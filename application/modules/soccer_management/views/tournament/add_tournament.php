<?php
//tournament data
$tournament_name = set_value('tournament_name');
$tournament_type_id = set_value('tournament_type_id');
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>soccer-management/tournament" class="btn btn-info pull-right">Back to tournament</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Tournament name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="tournament_name" placeholder="Tournament name" value="<?php echo $tournament_name;?>">
            </div>
        </div>
	</div>
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Tournament type: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="tournament_type_id">
                	<?php
                    	if($tournament_types->num_rows() > 0)
						{
							echo '<option value="" selected>--Select Tournamen type--</option>';
							$tournament_type = $tournament_types->result();
							
							foreach($tournament_type as $res)
							{
								$db_tournament_type_id = $res->tournament_type_id;
								$tournament_type_name = $res->tournament_type_name;
								
								if($db_tournament_type_id== $tournament_type_id)
								{
									echo '<option value="'.$db_tournament_type_id.'" selected>'.$tournament_type_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_tournament_type_id.'">'.$tournament_type_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add tournament
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>