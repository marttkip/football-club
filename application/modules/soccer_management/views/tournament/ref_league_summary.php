<?php 
//fixture details
$fixture_details_result = $result = $foul_result = $home_team_players = $away_team_players = $penaltyresult = $ref_sum_results = $home_substitutes_result = $away_substitutes_result = '';

if($away_substitutes->num_rows() > 0)
{
	$count = 0;
	$away_substitutes_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Player In</th>
						<th>Player Out</th>
						<th>MInutes</th>
					</tr>
				</thead>
				<tbody>';
	foreach($away_substitutes->result() as $awaysubs)
	{
		$count++;
		$away_home_player_in = $awaysubs->player_in;
		$away_player_in_name = $this->league_model->get_player_name($away_home_player_in);
		$away_home_player_out = $awaysubs->player_out;
		$away_player_out_name = $this->league_model->get_player_name($away_home_player_out);
		$away_home_player_min = $awaysubs->minute;
		
		$away_substitutes_result.='
					<tr>
						<td>'.$count.'</td>
						<td>'.$away_player_in_name.'</td>
						<td>'.$away_player_out_name.'</td>
						<td>'.$away_home_player_min.'</td>
					</tr>';
	}
	$away_substitutes_result.=
				'
				</tbody>
			</table>
			';
}
else
{
	$away_substitutes_result.= 'No substitues made for '.$away_team;
}
if($home_substitutes->num_rows() > 0)
{
	$count = 0;
	$home_substitutes_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Player In</th>
						<th>Player Out</th>
						<th>MInutes</th>
					</tr>
				</thead>
				<tbody>';
	foreach($home_substitutes->result() as $subs)
	{
		$count++;
		$home_player_in = $subs->player_in;
		$player_in_name = $this->league_model->get_player_name($home_player_in);
		$home_player_out = $subs->player_out;
		$player_out_name = $this->league_model->get_player_name($home_player_out);
		$home_player_min = $subs->minute;
		
		$home_substitutes_result.='
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_in_name.'</td>
						<td>'.$player_out_name.'</td>
						<td>'.$home_player_min.'</td>
					</tr>';
	}
	$home_substitutes_result.=
				'
				</tbody>
			</table>
			';
}
else
{
	$home_substitutes_result.= 'No substitues made for '.$home_team;
}
if($ref_summary->num_rows() > 0)
{
	$count = 0;
	$ref_sum_results .=
			'<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Comments</th>
					</tr>
				</thead>
				<tbody>
				';
	foreach($ref_summary->result() as $summary)
	{
		$count++;
		$referee_summary = $summary->summary;
		
		$ref_sum_results .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$referee_summary.'</td>
					</tr>
					';
		
	}
	$ref_sum_results .='</tbody>
				</table>';
}
else
{
	$ref_sum_results .= 'No comments were added by the commissioning referee';
}

if($home_players->num_rows() > 0)
{
	$count = 0;
	$home_team_players .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>No</th>
						<th>Name</th>
						<th>Licence Number</th>
						<th>Line Up</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($home_players->result() as $home)
	{

		$player_fname = $home->player_fname;
		$player_onames = $home->player_onames;
		$player_name = $player_fname.' '.$player_onames;

		$licence_number = $home->licence_number;
		$player_number = $home->player_number;
		$player_type = $home->fixture_player_type_name;
		
		$count++;
		$home_team_players .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_number.'</td>
						<td>'.$player_name.'</td>
						<td>'.$licence_number.'</td>
						<td>'.$player_type.'</td>
						
					</tr>
					';
		
	}
	$home_team_players .='</tbody>
				</table>';
}
else
{
	$home_team_players .= "No players were added for ".$home_team;
}
if($away_players->num_rows() > 0)
{
	$count = 0;
	$away_team_players .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>No</th>
						<th>Name</th>
						<th>Licence Number</th>
						<th>Line Up</th>
						
					</tr>
				</thead>
				  <tbody>
				';
	foreach($away_players->result() as $away)
	{

		$player_fname1 = $away->player_fname;
		$player_onames1 = $away->player_onames;
		$player_name1 = $player_fname1.' '.$player_onames1;
		$player_type_name1 = $away->fixture_player_type_name;

		$licence_number1 = $away->licence_number;
		$player_number1 = $away->player_number;
		
		$count++;
		$away_team_players .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_number1.'</td>
						<td>'.$player_name1.'</td>
						<td>'.$licence_number1.'</td>
						<td>'.$player_type_name1.'</td>
						
					</tr>
					';
		
	}
	$away_team_players .='</tbody>
				</table>';
}
else
{
	$away_team_players .= "No players were added for ".$home_team;
}
if($fixture_details->num_rows() > 0)
{
	$fixture_details_result.=
		'
		<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>Fixture NUmber</th>
						<th>Fixture Date</th>
						<th>Fixture Time</th>
						<th>Fixture Venue</th>
						<th>First Half Start Time</th>
						<th>First Half End Time</th>
						<th>Second Half Start Time</th>
						<th>Second Half End Time</th>
						
					</tr>
				</thead>
				<tbody>
				  
			';
	foreach($fixture_details->result() as $fixture)
	{
		$fixture_id = $fixture->tournament_fixture_id;
		$fixture_number = $fixture->tournament_fixture_number;
		$venue_name = $fixture->venue_name;
		$fixture_date = $fixture->tournament_fixture_date;
		$fixture_time = $fixture->tournament_fixture_time;
		$fixture_status = $fixture->tournament_fixture_status;
		$first_half_start_time = $fixture->first_half_start_time;
		$first_half_end_time = $fixture->first_half_end_time;
		$second_half_start_time = $fixture->second_half_start_time;
		$second_half_end_time = $fixture->second_half_end_time;
		
		$fixture_details_result .= '
				<tr>
					<td>'.$fixture_number.'</td>
					<td>'.date('jS M Y',strtotime($fixture_date)).'</td>
					<td>'.$fixture_time.'</td>
					<td>'.$venue_name.'</td>
					<td>'.$first_half_start_time.'</td>
					<td>'.$first_half_end_time.'</td>
					<td>'.$second_half_start_time.'</td>
					<td>'.$second_half_end_time.'</td>
				</tr>
				';
	}
	$fixture_details_result.='
			</tbody>
		</table>
		';
}
if($total_fixture_goal->num_rows() > 0)
{
	$count = 0;
	$result.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Goal Type</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($total_fixture_goal->result() as $fixture_goals)
	{
		
		$team_name = $fixture_goals->team_name;
		$player_number = $fixture_goals->player_number;
		$player_fname = $fixture_goals->player_fname;
		$player_onames = $fixture_goals->player_onames;
		$player_name = $player_fname. ' ' .$player_onames;
		$score_minute = $fixture_goals->goal_minute;
		$goal_type = $fixture_goals->goal_type_name;
		$count++;
		
		$result.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$player_number.'</td>
					<td>'.$player_name.'</td>
					<td>'.$score_minute.'</td>
					<td>'.$goal_type.'</td>
				</tr>
				';
	}
	$result .='</tbody>
				</table>';
}
else
{
	$result .= 'No goals scored';
}
if($penalty_goal->num_rows() > 0)
{
	$count = 0;
	$penaltyresult.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Score</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($penalty_goal->result() as $penalty)
	{
		
		$team_name2 = $penalty->team_name;
		$player_number2 = $penalty->player_number;
		$player_fname2 = $penalty->player_fname;
		$player_onames2 = $penalty->player_onames;
		$player_name2 = $player_fname. ' ' .$player_onames;
		$score_minute2 = $penalty->goal_minute;
		$goal_scored = $penalty->goal_scored;
		if($goal_scored == 0)
		{
			$goal_scored2 = 'No';
		}
		elseif($goal_scored == 1)
		{
			$goal_scored2 = 'Yes';
		}
		$count++;
		
		$penaltyresult.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name2.'</td>
					<td>'.$player_number2.'</td>
					<td>'.$player_name2.'</td>
					<td>'.$score_minute2.'</td>
					<td>'.$goal_scored2.'</td>
				</tr>
				';
	}
	$penaltyresult .='</tbody>
				</table>';
}
else
{
	$penaltyresult .= 'No penalties awarded';
}
if($fixture_fouls->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Reason</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($fixture_fouls->result() as $fouls_committed)
	{
		$action_name = $fouls_committed->action_name;
		$foul_type_name = $fouls_committed->foul_type_name;
		$foul_team_name = $fouls_committed->team_name;
		$foul_player_number = $fouls_committed->player_number;
		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$foul_player_name = $player_fname. ' ' .$player_onames;
		$foul_minute_time = $fouls_committed->foul_minute;
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$foul_team_name.'</td>
						<td>'.$foul_player_number.'</td>
						<td>'.$foul_player_name.'</td>
						<td>'.$foul_minute_time.'</td>
						<td>'.$foul_type_name.'</td>
						<td>'.$action_name.'</td>
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Referee Fixture Summary</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
			@media print
			{
				#page-break
				{
					page-break-after: always;
					page-break-inside : avoid;
				}
				.print-no-display
				{
					display: none !important;
				}
			}
        </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
    	
       <div class="row receipt_bottom_border">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                         <h2 class="panel-title"><?php echo $title;?></h2>
                    </header>             
                    
                    <!-- Widget content -->
                    <div class="panel-body">
                    	<strong><h4>Fixture Details</h4></strong>
                        <?php echo $fixture_details_result;?>
                        </br>
                        <strong><h4>Goal Details</h4></strong>
                        <?php echo $result;?>
                        </br>
                        <strong><h4>Penalties</h4></strong>
                        <?php echo $penaltyresult;?>
                        <br/>
                        <strong><h4>Caution Details</h4></strong>
                        <?php echo $foul_result;?>
                        </br>
                        <strong><h4><?php echo $home_team;?> Players</h4></strong>
                        <?php echo $home_team_players;?>
                        </br>
                        <strong><h4><?php echo $away_team;?> Players</h4></strong>
                        <?php echo $away_team_players;?>
                        </br>
                        <strong><h4>Referee Summary</h4></strong>
                        <?php echo $ref_sum_results;?>
                        </br>
                        <strong><h4><?php echo $home_team;?> Substitutions</h4></strong>
                        <?php echo $home_substitutes_result;?>
                        </br>
                        <strong><h4><?php echo $away_team;?> Substitutions</h4></strong>
                        <?php echo $away_substitutes_result;?>
                        </br>
                    </div>
                </section>
            </div>
        </div>

    </body>
</html>