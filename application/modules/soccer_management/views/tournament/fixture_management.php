<?php
//add fixture teams
$fixture_league_team_id = set_value('tournament_team_id');
$fixture_team_type_id = set_value('fixture_team_type_id');
$result = '';
if($fixture_teams->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team name</a></th>
						<th>Team type </a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($fixture_teams->result() as $all_teams)
	{
		$team_name = $all_teams->team_name;
		$fixture_team_type_name = $all_teams->fixture_team_type_name;
		$tournament_fixture_team_id = $all_teams->tournament_fixture_team_id;
		$team_id = $all_teams->team_id;
		$count++;
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$fixture_team_type_name.'</td>
				<td><a class="btn btn-warning" href="'.site_url().'soccer-management/add-tournament-fixture-player/'.$team_id.'/'.$tournament_fixture_team_id.'/'.$tournament_duration_id.'/'.$tournament_id.'/'.$tournament_fixture_id.'" title="Add team "><i class="fa fa-users"></i>Add Players</a></td>
					<td><a class="btn btn-success" href="'.site_url().'soccer-management/edit-team/'.$team_id.'" class="btn btn-sm btn-success" title="Edit '.$team_name.'"><i class="fa fa-pencil"></i> Edit Team</a></td>
					<td><a class="btn btn-info" href="'.site_url().'soccer-management/remove-tournament-fixture-team/'.$tournament_fixture_team_id.'/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id.'" class="btn btn-sm btn-danger" title="Remove '.$team_name.' from fixture"><i class="fa fa-pencil"></i> Remove Team</a></td>
				</tr>
				';
	}
	$result .=
				'</tbody>
			</table>
			';
}
else
{
	$result .= 'There are no teams for this fixture';
}
?>          
			<section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Add Team to fixture <?php echo $tournament_fixture_id;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url().'soccer-management/add-tournament-duration-fixture/'.$tournament_duration_id.'/'.$tournament_id;?>" class="btn btn-info pull-right">Back to fixture</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                  
                    <?php echo form_open('soccer-management/add-tournament-fixture-team/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id, array("class" => "form-horizontal", "role" => "form")); ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-5 control-label">Team: </label>
								<div class="col-lg-7">
									<select class="form-control" name="tournament_team_id">
                                    	<option value="--Select Team--" selected>--Select Team--</option>
                                    	<?php
											if($season_teams->num_rows()> 0)
											{
												foreach($season_teams->result() as $res)
												{
													$db_league_team_id = $res->tournament_team_id;
													$team_name = $res->team_name;
													
													if($db_league_team_id == $fixture_league_team_id)
													{
														echo '<option value="'.$db_league_team_id.'" selected>'.$team_name.'</option>';
													}
													
													else
													{
														echo '<option value="'.$db_league_team_id.'">'.$team_name.'</option>';
													}
												}
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-5 control-label">Team Type: </label>
								<div class="col-lg-7">
									<select class="form-control" name="fixture_team_type_id">
                                    	<option value="">---Select Team Type---</option>
										<?php
											if($team_types->num_rows()> 0)
											{
												foreach($team_types->result() as $res)
												{
													$db_fixture_team_type_id = $res->fixture_team_type_id;
													$fixture_team_type_name = $res->fixture_team_type_name;
													
													if($db_fixture_team_type_id == $fixture_team_type_id)
													{
														echo '<option value="'.$db_fixture_team_type_id.'" selected>'.$fixture_team_type_name.'</option>';
													}
													
													else
													{
														echo '<option value="'.$db_fixture_team_type_id.'">'.$fixture_team_type_name.'</option>';
													}
												}
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-md-12">
							<div class="form-actions center-align">
								<button class="submit btn btn-primary" type="submit">
									Add team
								</button>
							</div>
						</div>
					</div>
					<?php echo form_close();?>
					<h2 class="panel-title">Teams in fixture <?php echo $tournament_fixture_id;?></h2>

					<div class="table-responsive">
						
						<?php echo $result;?>
				
					</div>
				</div>
			</section>
            <?php
$referee_fixture_id = set_value('referee_id');
$fixture_team_type_id = set_value('fixture_team_type_id');
$fixture_referee_result = '';
if($fixture_referees->num_rows() > 0)
{
	$referee_count = 0;
	$fixture_referee_result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Referee name</th>
						<th>Referee type</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($fixture_referees->result() as $referee_fixture)
	{
		$referee_fname = $referee_fixture->personnel_fname;
		$referee_id = $referee_fixture->referee_id;
		$referee_id = $referee_fixture->personnel_id;
		$referee_type = $referee_fixture->referee_type_name;
		$referee_onames = $referee_fixture->personnel_onames;
		$referee_name = $referee_fname . ' ' .$referee_onames;
		$referee_count++;
		$fixture_referee_result .=
				'
				<tr>
					<td>'.$referee_count.'</td>
					<td>'.$referee_name.'</td>
					<td>'.$referee_type.'</td>
					<td><a href="'.site_url().'soccer-management/edit-referee/'.$referee_id.'" class="btn btn-sm btn-success" title="Edit '.$referee_name.'"><i class="fa fa-pencil"></i>Edit Referee Details</a></td>
				</tr>
				';
	}
	$fixture_referee_result .=
				'</tbody>
			</table>
			';
}
else
{
	$fixture_referee_result .= 'No referee asssigned for this fixture';
}
?>          
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Add Referee to <?php echo $tournament_fixture_id;?></h2>
                </header>
                <div class="panel-body">
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php  echo form_open('soccer-management/add-tournament-fixture-referee/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id, array("class" => "form-horizontal", "role" => "form")); ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-5 control-label">Referee: </label>
								<div class="col-lg-7">
									<select class="form-control" name="referee_id">
                                    	<option value="">---Select Referee---</option>
										<?php
											if($referees->num_rows()> 0)
											{
												foreach($referees->result() as $res)
												{
													$db_personnel_id = $res->personnel_id;
													$personnel_fname = $res->personnel_fname;
													$personnel_onames = $res->personnel_onames;
													$personnel_name = $personnel_fname. ' ' .$personnel_onames;

													
													if($db_personnel_id == $personnel_fixture_id)
													{
														echo '<option value="'.$db_personnel_id.'" selected>'.$personnel_name.'</option>';
													}
													
													else
													{
														echo '<option value="'.$db_personnel_id.'">'.$personnel_name.'</option>';
													}
												}
											}
										?>
									</select>
								</div>
							</div>
						</div>
                        <div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-5 control-label">Referee Type: </label>
								<div class="col-lg-7">
									<select class="form-control" name="referee_type_id">
                                    	<option value="">---Select Referee Type---</option>
										<?php
											if($referee_types->num_rows()> 0)
											{
												foreach($referee_types->result() as $res)
												{
													$db_referee_type_id = $res->referee_type_id;
													$referee_type_name = $res->referee_type_name;
													
													if($db_referee_type_id == $referee_type_id)
													{
														echo '<option value="'.$db_referee_type_id.'" selected>'.$referee_type_name.'</option>';
													}
													
													else
													{
														echo '<option value="'.$db_referee_type_id.'">'.$referee_type_name.'</option>';
													}
												}
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-md-12">
							<div class="form-actions center-align">
								<button class="submit btn btn-primary" type="submit">
									Add referee
								</button>
							</div>
						</div>
					</div>
					<?php echo form_close();?>
                
            
					<h2 class="panel-title">Referee for fixture <?php echo $tournament_fixture_id;?></h2>
					<div class="table-responsive">
						<?php echo $fixture_referee_result;?>
					</div>
				</div>
			</section>
            <?php
$commissioner_id = set_value('commissioner_id');
$commissioner_result = '';
if($fixture_commissioners->num_rows() > 0)
{
	$commissioner_count = 0;
	$commissioner_result .= 
			'<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Commissioner name</th>
					</tr>
				</thead>
				  <tbody>
			';
	foreach($fixture_commissioners->result() as $commissioners)
	{
		$personnel_id = $commissioners->personnel_id;
		$personnel_fname = $commissioners->personnel_fname;
		$personnel_onames = $commissioners->personnel_onames;
		$personnel_name = $personnel_fname. ' '.$personnel_onames;
		$commissioner_count++;
		
		$commissioner_result .=
				'
				<tr>
					<td>'.$commissioner_count.'</td>
					<td>'.$personnel_name.'</td>
				</tr>';
	}
	$commissioner_result .=
				'
				</tbody>
			</table>';
}
else
{
	$commissioner_result .= 'No commissioner added to the fixture';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Add commissioner to fixture <?php echo $tournament_fixture_id;?></h2>
    </header>
    <div class="panel-body">   
        <!-- Adding Errors -->
        <?php
            $success = $this->session->userdata('success_message');
            $error = $this->session->userdata('error_message');
            
            if(!empty($success))
            {
                echo '
                    <div class="alert alert-success">'.$success.'</div>
                ';
                
                $this->session->unset_userdata('success_message');
            }
            
            if(!empty($error))
            {
                echo '
                    <div class="alert alert-danger">'.$error.'</div>
                ';
                
                $this->session->unset_userdata('error_message');
            }
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
        ?>
        
        <?php  echo form_open('soccer-management/add-tournament-fixture-commissioner/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id, array("class" => "form-horizontal", "role" => "form")); ?>
        <div class="row">
            <div class="col-md-6">
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Commissioner: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="commissioner_id">
                            <?php
                                if($personnel_commissioners->num_rows()> 0)
                                {
									echo '<option value="--Select Commissioner--" selected>--Select Commissioner--</option>';
                                    foreach($personnel_commissioners->result() as $res)
                                    {
                                        $db_commissioner_id = $res->personnel_id;
                                        $commissioner_fname = $res->personnel_fname;
                                        $commissioner_onames = $res->personnel_onames;
                                        $commissioner_name = $commissioner_fname. ' ' .$commissioner_onames;
                                        
                                        if($db_commissioner_id == $commissioner_id)
                                        {
                                            echo '<option value="'.$db_commissioner_id.'" selected>'.$commissioner_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_commissioner_id.'">'.$commissioner_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12">
                <div class="form-actions center-align">
                    <button class="submit btn btn-primary" type="submit">
                        Add commmissioner
                    </button>
                </div>
            </div>
        </div>
        <?php echo form_close();?>
        <h2 class="panel-title">Commissioner for <?php echo $tournament_fixture_id;?></h2>
    	<div class="table-responsive">
            <?php echo $commissioner_result;?>
        </div>
    </div>
</section>