<?php
$tournament_result = '';
if($query->num_rows() > 0)
{
	$count = 0;
	$tournament_result .='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'soccer-management/tournament/tournament_name/'.$order_method.'/'.$page.'">Tournament name</a></th>
						<th><a href="'.site_url().'soccer-management/tournament/tournament_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th><a href="'.site_url().'soccer-management/tournament/tournament_type_name/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				<tbody>
			';
	foreach($query->result() as $tournament_details)
	{
		$tournament_id = $tournament_details->tournament_id;
		$tournament_name = $tournament_details->tournament_name;
		$tournament_status = $tournament_details->tournament_status;
		$tournament_type_id = $tournament_details->tournament_type_id;
		
		$tournament_type_name = $this->tournament_model->get_tournament_type_name($tournament_type_id);
		//status
		if($tournament_status == 1)
		{
			$status = 'Active';
		}
		else
		{
			$status = 'Disabled';
		}
		
		//create deactivated status display
		if($tournament_status == 0)
		{
			$status = '<span class="label label-default">Deactivated</span>';
			$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-tournament/'.$tournament_id.'" onclick="return confirm(\'Do you want to activate '.$tournament_name.'?\');" title="Activate '.$tournament_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
		}
		//create activated status display
		else if($tournament_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-tournament/'.$tournament_id.'" onclick="return confirm(\'Do you want to deactivate '.$tournament_name.'?\');" title="Deactivate '.$tournament_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
		}
		
		$count++;
		$tournament_result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$tournament_name.'</td>
				<td>'.$tournament_type_name.'</td>
				<td>'.$status.'</td>
				<td><a href="'.site_url().'soccer-management/edit-tournament/'.$tournament_id.'" class="btn btn-sm btn-success" title="Edit '.$tournament_name.'"><i class="fa fa-pencil"></i>Edit Tournament</a></td>
				<td>'.$button.'</td>
				<td><a href="'.site_url().'soccer-management/delete-tournament/'.$tournament_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$tournament_name.'?\');" title="Delete '.$tournament_name.'"><i class="fa fa-trash"> Delete</i></a></td>
				<td><a href="'.site_url().'soccer-management/add-tournament-duration/'.$tournament_id.'" class="btn btn-sm btn-success" title="Add schedule for '.$tournament_name.'"><i class="fa fa-plus"></i> Seasons</a></td>
			</tr> 
		';
	}
}
else
{
	$tournament_result .='No tournaments have been added';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search tournament</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("soccer_management/tournament/search_tournament", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="form-group">
                    <label class="col-md-4 control-label">Tournament name: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="tournament_name" placeholder="Tournament names">
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-md-8 col-md-offset-4">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info btn-sm">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
        <?php
        $search = $this->session->userdata('tournament_search_title2');
        
        if(!empty($search))
        {
            echo '<h6>Filtered by: '.$search.'</h6>';
            echo '<a href="'.site_url().'soccer_management/tournament/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
        }
        $success = $this->session->userdata('success_message');

        if(!empty($success))
        {
            echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
            $this->session->unset_userdata('success_message');
        }
        
        $error = $this->session->userdata('error_message');
        
        if(!empty($error))
        {
            echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
            $this->session->unset_userdata('error_message');
        }
        ?>
        <div class="row" style="margin-bottom:20px;">
            <!--<div class="col-lg-2 col-lg-offset-8">
                <a href="<?php echo site_url();?>soccer-management/export-league" class="btn btn-sm btn-success pull-right">Export</a>
            </div>-->
            <div class="col-lg-12">
                <a href="<?php echo site_url();?>soccer-management/add-tournament" class="btn btn-sm btn-info pull-right">Add Tournament</a>
            </div>
        </div>
        <div class="table-responsive">
            
            <?php echo $tournament_result;?>
    
        </div>
    </div>
    <div class="panel-footer">
        <?php if(isset($links)){echo $links;}?>
    </div>
</section>