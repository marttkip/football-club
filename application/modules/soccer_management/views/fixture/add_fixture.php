<?php
$result = '';
if($league_fixtures->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Fixture NUmber</th>
						<th>Fixture Date</th>
						<th>Fixture Time</th>
						<th>Fixture Venue</th>
						<th>Status</th>
						<th colspan="6">Actions</th>
					</tr>
				</thead>
				<tbody>
				  
			';
	foreach($league_fixtures->result() as $season_fixtures)
	{
		$fixture_id = $season_fixtures->fixture_id;
		$fixture_number = $season_fixtures->fixture_number;
		$venue_name = $season_fixtures->venue_name;
		$fixture_date = $season_fixtures->fixture_date;
		$fixture_time = $season_fixtures->fixture_time;
		$fixture_status = $season_fixtures->fixture_status;
		$count++;
		
		//create deactivated status display
		if($fixture_status == 0)
		{
			$status = '<span class="label label-default">Deactivated</span>';
			$button = '<a class="btn btn-sm btn-info" href="'.site_url().'soccer-management/activate-league-duration-fixture/'.$fixture_id.'/'.$league_id.'" onclick="return confirm(\'Do you want to activate fixture '.$fixture_id.'?\');" title="Activate '.$fixture_id.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
		}
		//create activated status display
		else if($fixture_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<a class="btn btn-sm btn-default" href="'.site_url().'soccer-management/deactivate-league-duration-fixture/'.$fixture_id.'/'.$league_id.'" onclick="return confirm(\'Do you want to deactivate fixture '.$fixture_id.'?\');" title="Activate '.$fixture_id.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
		}
		
		$result .= '
			<tr>
				<td>'.$count.'</td>
				<td>'.$fixture_number.'</td>
				<td>'.date('jS M Y',strtotime($fixture_date)).'</td>
				<td>'.$fixture_time.'</td>
				<td>'.$venue_name.'</td>
				<td>'.$status.'</td>
				<td>'.$button.'</td>
				<td><a class="btn btn-sm btn-warning" href="'.site_url().'soccer-management/manage-fixture/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" title="Manage fixture "><i class="fa fa-plus"></i>Manage Fixture</a></td>
				<td><a class="btn btn-sm btn-info" href="'.site_url().'soccer-management/get-league-fixture-summary-commissioner/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" target="_blank" title="Fixture Summary By Referee"><i class="fa fa-soccer-ball-o"></i>Commissioner Summary</a></td>
				<td><a class="btn btn-sm btn-danger" href="'.site_url().'soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" title="Post results "><i class="fa fa-plus-square-o"></i> Post Results</a></td>
				<td><a class="btn btn-sm btn-success" href="'.site_url().'soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" title="Send officials to admin for payments"><i class="fa fa-money"></i> Payments</a></td>
				<td><a class="btn btn-sm btn-info" href="'.site_url().'soccer-management/get-league-fixture-summary/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" target="_blank" title="Fixture Summary By Referee"><i class="fa fa-soccer-ball-o"></i>Ref. Fixture Summary</a></td>
			</tr>
			';
	}
}

//fixture data
$fixture_date = set_value('fixture_date');
$fixture_time = set_value('fixture_time');
$venue_id = set_value('venue_id');
$fixture_number = set_value('fixture_number');
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                        	<a href="<?php echo base_url().'soccer-management/add-league-duration/'.$league_id;?>" class="btn btn-sm btn-info pull-right">Back to season</a> 
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Date of Fixture: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="fixture_date" placeholder="Date of Fixture" value="<?php echo $fixture_date;?>">
                </div>
            </div>
        </div>
        <div class="form-group">
    		<label class="col-lg-5 control-label"> Time of Fixture:</label>
            <div class="col-lg-7">
                <div class="input-group">
                	<span class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </span>
                    <input id="fixture_time" name="fixture_time"type="time" placeholder="Time of Fixture" class="form-control" required />
                </div>
            </div>
		</div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Venue: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="venue_id">
                	<?php
                    	if($venues->num_rows() > 0)
						{
							echo '<option value="--Select Venue--" selected>--Select Venue--</option>';
							$venue = $venues->result();
							
							foreach($venue as $res)
							{
								$venue_id = $res->venue_id;
								$venue_name = $res->venue_name;
								
								if($venue_id == $venue_id)
								{
									echo '<option value="'.$venue_id.'" selected>'.$venue_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$venue_id.'">'.$venue_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Fixture Number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="fixture_number" placeholder="Fixture Number" value="<?php echo $fixture_number;?>">
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-sm btn-primary" type="submit">
                Add fixture
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $result;?>
        </div> 
    </div>
</section>   