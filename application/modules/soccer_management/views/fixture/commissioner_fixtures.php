<?php
$result = '';
if($league_fixtures->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Fixture NUmber</th>
						<th>Fixture Date</th>
						<th>Fixture Time</th>
						<th>Fixture Venue</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				<tbody>
				  
			';
	foreach($league_fixtures->result() as $season_fixtures)
	{
		$fixture_id = $season_fixtures->fixture_id;
		$fixture_number = $season_fixtures->fixture_number;
		$venue_name = $season_fixtures->venue_name;
		$fixture_date = $season_fixtures->fixture_date;
		$fixture_time = $season_fixtures->fixture_time;
		$fixture_status = $season_fixtures->fixture_status;
		$league_id = $season_fixtures->league_id;
		$league_duration_id = $season_fixtures->league_duration_id;
		$count++;
		
		//create deactivated status display
		if($fixture_status == 0)
		{
			$status = '<span class="label label-default">Deactivated</span>';
			$button = '<a class="btn btn-sm btn-info" href="'.site_url().'soccer-management/activate-league-duration-fixture/'.$fixture_id.'/'.$league_id.'" onclick="return confirm(\'Do you want to activate fixture '.$fixture_id.'?\');" title="Activate '.$fixture_id.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
		}
		//create activated status display
		else if($fixture_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<a class="btn btn-sm btn-default" href="'.site_url().'soccer-management/deactivate-league-duration-fixture/'.$fixture_id.'/'.$league_id.'" onclick="return confirm(\'Do you want to deactivate fixture '.$fixture_id.'?\');" title="Activate '.$fixture_id.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
		}
		
		$result .= '
			<tr>
				<td>'.$count.'</td>
				<td>'.$fixture_number.'</td>
				<td>'.date('jS M Y',strtotime($fixture_date)).'</td>
				<td>'.$fixture_time.'</td>
				<td>'.$venue_name.'</td>
				<td>'.$status.'</td>
				<td><a class="btn btn-sm btn-info" href="'.site_url().'soccer-management/get-league-fixture-summary-commissioner/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" target="_blank" title="Fixture Summary By Referee"><i class="fa fa-soccer-ball-o"></i> Commissioner Summary</a></td>
				<td><a class="btn btn-sm btn-danger" href="'.site_url().'soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'/2" title="Post results "><i class="fa fa-plus-square-o"></i> Post Results</a></td>
				</tr>
			';
	}
}
else
{
	$result.='No league matches were assigned to you';
}

//fixture data
$fixture_date = set_value('fixture_date');
$fixture_time = set_value('fixture_time');
$venue_id = set_value('venue_id');
$fixture_number = set_value('fixture_number');
?>          

<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	 <?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}
			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
        ?>
    	<div class="table-responsive">
            <?php echo $result;?>
        </div> 
    </div>
</section>   