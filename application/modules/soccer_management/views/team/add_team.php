<?php
//team data
$team_name = set_value('team_name');
$team_venue = set_value('team_venue');
$team_logo = set_value('team_logo');
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                    <?php 
					$youth_teams = $this->session->userdata('youth_teams');
					if(!empty($youth_teams))
					{
						?>
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>soccer-management/youth-teams" class="btn btn-info pull-right">Back to youth team</a>
                        </div>
                        <?php
					}
					else
					{
						?>
						<div class="col-lg-12">
                            <a href="<?php echo site_url();?>soccer-management/team" class="btn btn-info pull-right">Back to team</a>
                        </div>
                        <?php
					}
					?>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	 <div class="col-md-4">
    	<!-- Image -->
        <div class="form-group">
            <div class="col-lg-12">
                
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="max-width:200px; max-height:200px;">
                        <img src="<?php echo $team_logo_name;?>" class="img-responsive">
                    </div>
                    <div>
                        <span class="btn btn-file btn-success"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="logo_image"></span>
                        <a href="#" class="btn btn-info fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-md-4">
       
        <div class="form-group">
            <label class="col-lg-5 control-label">Team Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="team_name" placeholder="Team Name" value="<?php echo $team_name;?>">
            </div>
        </div>
	</div>
    <div class="col-md-4">
       
        <div class="form-group">
            <label class="col-lg-5 control-label">Team Venue: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="team_venue" placeholder="Team Venue" value="<?php echo $team_venue;?>">
            </div>
        </div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add team
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>