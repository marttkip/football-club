<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'soccer-management/team/team_name/'.$order_method.'/'.$page.'">Team name</a></th>
						<th><a href="'.site_url().'soccer-management/team/team_venue/'.$order_method.'/'.$page.'"> Venue</a></th>
						<th><a href="'.site_url().'soccer-management/team/team_venue/'.$order_method.'/'.$page.'"> Coach</a></th>
						<th><a href="'.site_url().'soccer-management/team/team_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$team_id = $row->team_id;
				$team_name = $row->team_name;
				$team_venue = $row->team_venue;
				$team_logo = $row->team_logo;
				$team_status = $row->team_status;
				
				$coach_id = '';
				//status
				if($team_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($team_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-team/'.$team_id.'" onclick="return confirm(\'Do you want to activate '.$team_name.'?\');" title="Activate '.$team_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($team_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-team/'.$team_id.'" onclick="return confirm(\'Do you want to deactivate '.$team_name.'?\');" title="Deactivate '.$team_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				//get team
				$coach = '';
				$coach_id = $this->coach_model->get_team_coach($team_id);
				$coach = $this->coach_model->get_coach_name($coach_id);
				if(empty($coach ))
				{
					$coach = '-';
				}
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$team_name.'</td>
						<td>'.$team_venue.'</td>
						<td>'.$coach.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'soccer-management/edit-team/'.$team_id.'" class="btn btn-sm btn-success" title="Edit '.$team_name.'"><i class="fa fa-pencil"></i>Edit Team</a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'soccer-management/delete-team/'.$team_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$team_name.'?\');" title="Delete '.$team_name.'"><i class="fa fa-trash"> Delete</i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no team";
		}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search team</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("soccer_management/team/search_team", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Coach: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="coach_id">
                            	<option value="">---Select Coach---</option>
                                <?php
                                    if($coaches->num_rows() > 0){
                                        foreach($coaches->result() as $row):
                                            $coach_onames = $row->coach_onames;
											$coach_fname = $row->coach_fname;
                                            $coach_id= $row->coach_id;
											$coach_name = $coach_fname.' '.$coach_onames;
                                            ?><option value="<?php echo $coach_id; ?>" ><?php echo $coach_name; ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Team Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="team_number" placeholder="Team number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Team name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="team_name" placeholder="Team name">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Team Venue: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="team_venue" placeholder="Team Venue">
                        </div>
                    </div>
            
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4">
                        	<div class="center-align">
                            	<button type="submit" class="btn btn-info btn-sm">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
							</header>
							<div class="panel-body">
                            	<?php
								$search = $this->session->userdata('team_search_title2');
								
								if(!empty($search))
								{
									echo '<h6>Filtered by: '.$search.'</h6>';
									echo '<a href="'.site_url().'soccer_management/team/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
								}
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>soccer-management/export-team" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <?php 
									$youth_teams = $this->session->userdata('youth_teams');
									if(!empty($youth_teams))
									{
										?>
                                        <div class="col-lg-12">
                                            <a href="<?php echo site_url();?>soccer-management/add-youth-team" class="btn btn-sm btn-info pull-right">Add Youth Team</a>
                                        </div>
                                        <?php
									}
									else
									{
										?>
										<div class="col-lg-12">
											<a href="<?php echo site_url();?>soccer-management/add-team" class="btn btn-sm btn-info pull-right">Add Team</a>
										</div>
                                        <?php
									}
									?>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>