<?php
$assessment_result = $comment_result = $fixture_referee_result = $fixture_details_result = '';
if($fixture_details->num_rows() > 0)
{
	$fixture_details_result.=
		'
		<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>Fixture NUmber</th>
						<th>Fixture Date</th>
						<th>Fixture Time</th>
						<th>Fixture Venue</th>
						
					</tr>
				</thead>
				<tbody>
				  
			';
	foreach($fixture_details->result() as $fixture)
	{
		$fixture_id = $fixture->fixture_id;
		$fixture_number = $fixture->fixture_number;
		$venue_name = $fixture->venue_name;
		$fixture_date = $fixture->fixture_date;
		$fixture_time = $fixture->fixture_time;
		
		$fixture_details_result .= '
				<tr>
					<td>'.$fixture_number.'</td>
					<td>'.date('jS M Y',strtotime($fixture_date)).'</td>
					<td>'.$fixture_time.'</td>
					<td>'.$venue_name.'</td>
				</tr>
				';
	}
	$fixture_details_result.='
			</tbody>
		</table>
		';
}
if($all_fixture_comments->num_rows() > 0)
{
	$comment_result .= 
				'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Comment</th>
							<th>Comment description</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	$count=0;	
	foreach($all_fixture_comments->result() as $comments_of_fixture)
	{
		$comment_id = $comments_of_fixture->comment_id;
		$comment_fixture_description = $comments_of_fixture->comment_fixture_description;
		$comment_name = $comments_of_fixture->comment_name;
		$count++;
		$comment_result .= 
						'<tr>
							<td>'.$count.'</td>
							<td>'.$comment_name.'</td>
							<td>'.$comment_fixture_description.'</td>
						</tr>
						';
	}
	$comment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$comment_result .= 'No comments for this fixture';
}
if($all_assessment_fixtures->num_rows() > 0)
{
	$assessment_count = 0;
	$assessment_result .=
	'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Assessmnet</th>
							<th>Comment description</th>
							<th>Rating</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	foreach($all_assessment_fixtures->result() as $assessments_of_fixture)
	{
		$assessment_id = $assessments_of_fixture->assessment_id;
		$assessment_fixture_description = $assessments_of_fixture->assessment_fixture_description;
		$assessment_name = $assessments_of_fixture->assessment_name;
		$assessment_rating = $assessments_of_fixture->assessment_fixture_rating;
		$assessment_count++;
		$assessment_result .= 
						'<tr>
							<td>'.$assessment_count.'</td>
							<td>'.$assessment_name.'</td>
							<td>'.$assessment_fixture_description.'</td>
							<td>'.$assessment_rating.'</td>
						</tr>
						';
	}
	$assessment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$assessment_result .= 'No assessments added to this fixture';
}
if($fixture_referees->num_rows() > 0)
{
	$referee_count = 0;
	$fixture_referee_result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Referee name</th>
						<th>Referee type</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($fixture_referees->result() as $referee_fixture)
	{
		$referee_fname = $referee_fixture->personnel_fname;
		$referee_type = $referee_fixture->referee_type_name;
		$referee_onames = $referee_fixture->personnel_onames;
		$referee_name = $referee_fname . ' ' .$referee_onames;
		$referee_count++;
		$fixture_referee_result .=
				'
				<tr>
					<td>'.$referee_count.'</td>
					<td>'.$referee_name.'</td>
					<td>'.$referee_type.'</td>
				</tr>
				';
	}
	$fixture_referee_result .=
				'</tbody>
			</table>
			';
}
else
{
	$fixture_referee_result .= 'No referee asssigned for this fixture';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Referee Fixture Summary</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
			@media print
			{
				#page-break
				{
					page-break-after: always;
					page-break-inside : avoid;
				}
				.print-no-display
				{
					display: none !important;
				}
			}
        </style>
    </head>
    <body class="receipt_spacing"  onLoad="window.print();return false;">
    	
       <div class="row receipt_bottom_border">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                         <h2 class="panel-title"><?php echo $title;?></h2>
                    </header>             
                    
                    <!-- Widget content -->
                    <div class="panel-body">
                    	<strong><h4>Fixture Details</h4></strong>
                    	<?php echo $fixture_details_result;?>
                        
                    	<strong><h4>Fixture Referees</h4></strong>
                        <?php echo $fixture_referee_result;?>
                        </br>
                        <strong><h4>Comments</h4></strong>
                        <?php echo $comment_result;?>
                        </br>
                        <strong><h4>Assessment</h4></strong>
                        <?php echo $assessment_result;?>
                        </br>
                    </div>
                </section>
            </div>
        </div>

    </body>
</html>