<?php
//league data
$team_id = set_value('team_id');
$fixture_team_type_id = set_value('fixture_team_type_id');
$result = '';
if($fixture_referees->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Referee name</th>
						<th>Referee type</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($fixture_referees->result() as $referee_fixture)
	{
		$referee_fname = $referee_fixture->referee_fname;
		$referee_id = $referee_fixture->referee_id;
		$referee_type = $referee_fixture->referee_type_name;
		$referee_onames = $referee_fixture->referee_onames;
		$referee_name = $referee_fname . ' ' .$referee_onames;
		$count++;
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$referee_name.'</td>
					<td>'.$referee_type.'</td>
					<td><a href="'.site_url().'soccer-management/edit-referee/'.$referee_id.'" class="btn btn-sm btn-success" title="Edit '.$referee_name.'"><i class="fa fa-pencil"></i>Edit Referee Details</a></td>
				</tr>
				';
	}
}
else
{
	$result .= 'No referee asssigned for this fixture';
}
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url().'soccer-management/add-league-duration/'.$league_id;?>" class="btn btn-info pull-right">Back to season</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Referee: </label>
            <div class="col-lg-7">
                <select class="form-control" name="referee_id">
                	<?php
                    	if($referees->num_rows()> 0)
						{
							foreach($referees->result() as $res)
							{
								$db_referee_id = $res->referee_id;
								$referee_fname = $res->referee_fname;
								$referee_onames = $res->referee_onames;
								$referee_name = $referee_fname. ' ' .$referee_onames;
								
								if($db_team_id == $team_id)
								{
									echo '<option value="'.$db_referee_id.'" selected>'.$referee_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_referee_id.'">'.$referee_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add referee
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>
<section class="panel">

    <header class="panel-heading">
        <h2 class="panel-title">Referee for fixture <?php echo $fixture_id;?></h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
</section>