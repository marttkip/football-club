<?php
$fixture_team_player_id = set_value('fixture_team_player_id');
$comment_fixture_description = set_value('comment_fixture_description');
$assessment_fixture_description = set_value('assessment_fixture_description');
$foul_player_id = set_value('foul_player_id');
$foul_minute = set_value('foul_minute');
$action_id= set_value('action_id');
$foul_type_id= set_value('foul_type_id');
$goul_type_id = set_value('goul_type_id');
$goal_minute = set_value('goal_minute');
$result = '';
$foul_result = '';
$comment_result = '';
$assessment_result = '';
if($fixture_fouls->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Reason</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($fixture_fouls->result() as $fouls_committed)
	{
		$action_name = $fouls_committed->action_name;
		$foul_type_name = $fouls_committed->foul_type_name;
		$foul_team_name = $fouls_committed->team_name;
		$foul_player_number = $fouls_committed->player_number;
		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$foul_player_name = $player_fname. ' ' .$player_onames;
		$foul_minute_time = $fouls_committed->foul_minute;
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$foul_team_name.'</td>
						<td>'.$foul_player_number.'</td>
						<td>'.$foul_player_name.'</td>
						<td>'.$foul_minute_time.'</td>
						<td>'.$foul_type_name.'</td>
						<td>'.$action_name.'</td>
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}
if($total_fixture_goal->num_rows() > 0)
{
	$count = 0;
	$result.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Goal Type</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($total_fixture_goal->result() as $fixture_goals)
	{
		
		$team_name = $fixture_goals->team_name;
		$player_number = $fixture_goals->player_number;
		$player_fname = $fixture_goals->player_fname;
		$player_onames = $fixture_goals->player_onames;
		$player_name = $player_fname. ' ' .$player_onames;
		$score_minute = $fixture_goals->goal_minute;
		$goal_type = $fixture_goals->goal_type_name;
		$count++;
		
		$result.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$player_number.'</td>
					<td>'.$player_name.'</td>
					<td>'.$score_minute.'</td>
					<td>'.$goal_type.'</td>
				</tr>
				';
	}
	$result .='</tbody>
				</table>';
}
else
{
	$result .= 'No goals scored';
}

if($all_fixture_comments->num_rows() > 0)
{
	$comment_result .= 
				'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Comment</th>
							<th>Comment description</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	$count=0;	
	foreach($all_fixture_comments->result() as $comments_of_fixture)
	{
		$comment_id = $comments_of_fixture->comment_id;
		$comment_fixture_description = $comments_of_fixture->comment_fixture_description;
		$comment_name = $comments_of_fixture->comment_name;
		$count++;
		$comment_result .= 
						'<tr>
							<td>'.$count.'</td>
							<td>'.$comment_name.'</td>
							<td>'.$comment_fixture_description.'</td>
						</tr>
						';
	}
	$comment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$comment_result .= 'No comments for this fixture';
}
if($all_assessment_fixtures->num_rows() > 0)
{
	$assessment_count = 0;
	$assessment_result .=
	'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Assessmnet</th>
							<th>Comment description</th>
							<th>Rating</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	foreach($all_assessment_fixtures->result() as $assessments_of_fixture)
	{
		$assessment_id = $assessments_of_fixture->assessment_id;
		$assessment_fixture_description = $assessments_of_fixture->assessment_fixture_description;
		$assessment_name = $assessments_of_fixture->assessment_name;
		$assessment_rating = $assessments_of_fixture->assessment_fixture_rating;
		$assessment_count++;
		$assessment_result .= 
						'<tr>
							<td>'.$assessment_count.'</td>
							<td>'.$assessment_name.'</td>
							<td>'.$assessment_fixture_description.'</td>
							<td>'.$assessment_rating.'</td>
						</tr>
						';
	}
	$assessment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$assessment_result .= 'No assessments added to this fixture';
}
?>

<?php
$fixture_team_player_id = set_value('fixture_team_player_id');
$foul_player_id = set_value('foul_player_id');
$foul_minute = set_value('foul_minute');
$action_id= set_value('action_id');
$foul_type_id= set_value('foul_type_id');
$goal_minute = set_value('goal_minute');
$goul_type_id = set_value('goul_type_id');
$result = '';
$foul_result = '';
if($fixture_fouls->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Reason</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($fixture_fouls->result() as $fouls_committed)
	{
		$action_name = $fouls_committed->action_name;
		$foul_type_name = $fouls_committed->foul_type_name;
		$foul_team_name = $fouls_committed->team_name;
		$foul_player_number = $fouls_committed->player_number;
		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$foul_player_name = $player_fname. ' ' .$player_onames;
		$foul_minute_time = $fouls_committed->foul_minute;
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$foul_team_name.'</td>
						<td>'.$foul_player_number.'</td>
						<td>'.$foul_player_name.'</td>
						<td>'.$foul_minute_time.'</td>
						<td>'.$foul_type_name.'</td>
						<td>'.$action_name.'</td>
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}
if($total_fixture_goal->num_rows() > 0)
{
	$count = 0;
	$result.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Goal Type</th>
                        <th></th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($total_fixture_goal->result() as $fixture_goals)
	{
		
		$team_name = $fixture_goals->team_name;
		$player_number = $fixture_goals->player_number;
		$player_fname = $fixture_goals->player_fname;
		$player_onames = $fixture_goals->player_onames;
		$player_name = $player_fname. ' ' .$player_onames;
		$score_minute = $fixture_goals->goal_minute;
		$goal_type = $fixture_goals->goal_type_name;
        $goal_scored = $fixture_goals->goal_scored;

        if($goal_scored == 1)
        {
            $is_scored = '<div class="success">scored</div>';
        }
        elseif($goal_scored == 0)
        {
            $is_scored = '<div class="danger">missed</div>';
        }
		else
        {
            $is_scored = '<div class="warning">-</div>';
        }
		$count++;
		
		$result.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$player_number.'</td>
					<td>'.$player_name.'</td>
					<td>'.$score_minute.'</td>
					<td>'.$goal_type.'</td>
                    <td>'.$is_scored.'</td>
				</tr>
				';
	}
	$result .='</tbody>
				</table>';
}
else
{
	$result .= 'No goals scored';
}
foreach ($fixture_detail->result() as $key_fixture) {
    # code...
    $first_half_start_time = $key_fixture->commissioner_first_half_start_time;
    $first_half_end_time = $key_fixture->commissioner_first_half_end_time;
    $second_half_start_time = $key_fixture->commissioner_second_half_start_time;
    $second_half_end_time = $key_fixture->commissioner_second_half_end_time;
    $first_half_score = 0;
    $second_half_score = 0;
    $penalty_scores = 0;
   if($first_half_end_time != NULL )
   {
         $array = explode(':', $first_half_end_time);
         $first_half_minute = $array[1];

         $first_half_score = $this->referee_model->get_first_half_goals($first_half_minute,$fixture_id);
   }
   
   if($second_half_end_time != null)
   {

    $array2 = explode(':', $second_half_end_time);

    $second_half_minute = $array[1];

    $second_half_score = $this->referee_model->get_second_half_goals($first_half_minute,$fixture_id,$second_half_minute);
    
    $penalty_scores = $this->referee_model->get_penalties_goals($fixture_id,$second_half_minute);

   }
   

}
?>


<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
        <div class="pull-right"  style="margin-top: -25px;">
            <a href="<?php echo base_url().'commissioner/league';?>" class="btn  btn-sm btn-info fa fa-arrow-left"> 
            Back to leagues 
            </a>
        </div>
    </header>
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-2">
                <button type="button" class="btn btn-default btn-sm col-md-12"  data-toggle="modal" data-target="#add_match_timing">
                     Match Timings
                    </button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-success btn-sm col-md-12"  data-toggle="modal" data-target="#add_scores_items">
                     Add Score
                    </button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-danger btn-sm col-md-12"  data-toggle="modal" data-target="#add_faul_items">
                     Add Caution
                    </button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-warning btn-sm col-md-12"  data-toggle="modal" data-target="#add_subs">
                     Substitutions
                    </button>
            </div>
             <!-- <div class="col-md-2">
                <button type="button" class="btn btn-danger btn-sm col-md-12"  data-toggle="modal" data-target="#add_match_summary">
                     Match Summary
                    </button>
            </div> -->
            
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-sm col-md-12"  data-toggle="modal" data-target="#add_scores">
                     Scores Report
                    </button>
            </div>
            
        </div>
         <!-- Adding Errors -->
        <?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}
			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
        ?>
       
    
	    <div class="modal fade" id="add_scores_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">Add Score</h4>
	                </div>
	                <div class="modal-body">
	                    <?php echo form_open('soccer_management/referee/add_fixture_goal/'.$fixture_id.'/1', array("class" => "form-horizontal", "role" => "form"));
	                    ?>
	                    <div class="row">
	                        <div class="col-md-12">
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Team: </label>
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="team_id" id="player_team_id" onchange="check_department_type()">
	                                        <?php
	                                            if($fixture_teams->num_rows()> 0)
	                                            {
	                                                echo '<option value="0" selected>--Select Team--</option>';
	                                                foreach($fixture_teams->result() as $res)
	                                                {
	                                                    $team_id = $res->team_id;
	                                                    $team_name = $res->team_name;
	                                                    
	                                                    echo '<option value="'.$team_id.'">'.$team_name.'</option>';
	                                                }
	                                            }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Select Player: </label>
	                                
	                                <div class="col-lg-8">
	                                    <select name="fixture_team_player_id" class="form-control" id="team_players">
	                                        
	                                    </select>
	                                </div>
	                            </div>
	                        
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Goal Type: </label>
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="goal_type_id" id="goal_type_id" onchange="check_goal_type()">
	                                        <?php
	                                            if($goul_types->num_rows()> 0)
	                                            {
	                                                echo '<option value="--Select Scorer--" selected>--Select Goal Type--</option>';
	                                                foreach($goul_types->result() as $res)
	                                                {
	                                                    $db_goul_type_id = $res->goal_type_id;
	                                                    $goul_type_name = $res->goal_type_name;
	                                                    
	                                                    if($db_goul_type_id == $goal_type_id)
	                                                    {
	                                                        echo '<option value="'.$db_goul_type_id.'" selected>'.$goul_type_name.'</option>';
	                                                    }
	                                                    
	                                                    else
	                                                    {
	                                                        echo '<option value="'.$db_goul_type_id.'">'.$goul_type_name.'</option>';
	                                                    }
	                                                }
	                                            }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>
	                             <div class="form-group" id="goal_type_score" style="display:none">
	                                    <label class="col-lg-3 control-label">Scored ? </label>
	                                    <div class="col-lg-4">
	                                        <div class="radio">
	                                            <label>
	                                                <input id="optionsRadios2" type="radio" name="penalty_score_status" value="0" checked="checked" >
	                                                No
	                                            </label>
	                                        </div>
	                                    </div>
	                                    
	                                    <div class="col-lg-5">
	                                        <div class="radio">
	                                            <label>
	                                                <input id="optionsRadios2" type="radio" name="penalty_score_status" value="1" >
	                                                Yes
	                                            </label>
	                                        </div>
	                                    </div>
	                                </div>
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Score Minute </label>
	                                
	                                <div class="col-lg-8">
	                                    <input type="text" class="form-control" name="goal_minute" placeholder="Minute" value="<?php echo $goal_minute;?>">
	                                </div>
	                            </div>
	                        
	                            <div class="row" style="margin-top:10px;">
	                                <div class="col-md-12">
	                                    <div class="form-actions center-align">
	                                        <button class="submit btn btn-success" type="submit">
	                                            Submit Score
	                                        </button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <?php echo form_close();?>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="modal fade" id="add_subs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">Make Substitution</h4>
	                </div>
	                <div class="modal-body">
	                    <?php 
	                    $minute = set_value('minute');
	                    $home_player_in = set_value('home_player_in');
	                    $away_player_in = set_value('away_player_in');
	                    $home_player_out = set_value('home_player_out');
	                    $away_player_out = set_value('away_player_out');
	                    ?>
	                    <div class = "row">
	                        <div class = "col-md-6">
	                            <section class="panel">
	                                <header class="panel-heading">
	                                    <h2 class="panel-title"><?php echo $title_home;?></h2>
	                                </header>
	                               
	                                <div class="panel-body">
	                                     <?php echo form_open(base_url().'soccer_management/referee/home-t-substitute/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
	                                    <div class="form-group">
	                                        <label class="col-lg-5 control-label">Minute: </label>
	                                        
	                                        <div class="col-lg-7">
	                                            <input type="text" class="form-control" name="home_minute" placeholder="minute" value="<?php echo $minute;?>">
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="col-lg-5 control-label">Player Out: </label>
	                                        
	                                        <div class="col-lg-7">
	                                            <select class="form-control" name="home_player_out">
	                                                <?php
	                                                    if($team_details_home_out->num_rows() > 0)
	                                                    {
	                                                        $gender = $team_details_home_out->result();
	                                                        
	                                                        foreach($gender as $res)
	                                                        {
	                                                            $player_id = $res->player_id;
	                                                            $player_fname = $res->player_fname;
	                                                            $player_onames = $res->player_onames;
	                                                            $player_name = $player_fname.' '.$player_onames;
	                                                            
	                                                            if($player_id == $home_player_out)
	                                                            {
	                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
	                                                            }
	                                                            
	                                                            else
	                                                            {
	                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
	                                                            }
	                                                        }
	                                                    }
	                                                ?>
	                                            </select>
	                                        </div>
	                    
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="col-lg-5 control-label">Player In: </label>
	                                        
	                                        <div class="col-lg-7">
	                                            <select class="form-control" name="home_player_in">
	                                                <?php
	                                                    if($team_details_home_in->num_rows() > 0)
	                                                    {
	                                                        $gender = $team_details_home_in->result();
	                                                        
	                                                        foreach($gender as $res)
	                                                        {
	                                                            $player_id = $res->player_id;
	                                                            $player_fname = $res->player_fname;
	                                                            $player_onames = $res->player_onames;
	                                                            $player_name = $player_fname.' '.$player_onames;
	                                                            
	                                                            if($player_id == $home_player_in)
	                                                            {
	                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
	                                                            }
	                                                            
	                                                            else
	                                                            {
	                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
	                                                            }
	                                                        }
	                                                    }
	                                                ?>
	                                            </select>
	                                        </div>
	                                    </div>
	                                    <div class="row" style="margin-top:10px;">
	                                        <div class="form-actions center-align">
	                                            <button class="submit btn btn-success" type="submit">
	                                                Add Home Substitute
	                                            </button>
	                                        </div>
	                                    </div>
	                                    <?php echo form_close();?>
	                                </div>
	                            </section>
	                        </div>
	                        <div class = "col-md-6">
	                            <section class="panel">
	                                <header class="panel-heading">
	                                    <h2 class="panel-title"><?php echo $title_away;?></h2>
	                                </header>
	                               
	                                <div class="panel-body">
	                                    <?php echo form_open(base_url().'soccer_management/referee/away-t-substitute/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
	                                    <div class="form-group">
	                                        <label class="col-lg-5 control-label">Minute: </label>
	                                        
	                                        <div class="col-lg-7">
	                                            <input type="text" class="form-control" name="away_minute" placeholder="minute" value="<?php echo $minute;?>">
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label class="col-lg-5 control-label">Player Out: </label>
	                                        
	                                        <div class="col-lg-7">
	                                            <select class="form-control" name="away_player_out">
	                                                <?php
	                                                    if($team_details_away_out->num_rows() > 0)
	                                                    {
	                                                        $away_out = $team_details_away_out->result();
	                                                        
	                                                        foreach($away_out as $away_res)
	                                                        {
	                                                            $player_id = $away_res->player_id;
	                                                            $player_fname = $away_res->player_fname;
	                                                            $player_onames = $res->player_onames;
	                                                            $player_name = $player_fname.' '.$player_onames;
	                                                            
	                                                            if($player_id == $away_player_out)
	                                                            {
	                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
	                                                            }
	                                                            
	                                                            else
	                                                            {
	                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
	                                                            }
	                                                        }
	                                                    }
	                                                ?>
	                                            </select>
	                                        </div>

	                                    </div>
	                                    <div class="form-group">
	                                        <label class="col-lg-5 control-label">Player In: </label>
	                                        
	                                        <div class="col-lg-7">
	                                            <select class="form-control" name="away_player_in">
	                                                <?php
	                                                    if($team_details_away_in->num_rows() > 0)
	                                                    {
	                                                        $away_in = $team_details_away_in->result();
	                                                        
	                                                        foreach($away_in as $away_res)
	                                                        {
	                                                            $player_id = $res->player_id;
	                                                            $player_fname = $res->player_fname;
	                                                            $player_onames = $res->player_onames;
	                                                            $player_name = $player_fname.' '.$player_onames;
	                                                            
	                                                            if($player_id == $away_player_in)
	                                                            {
	                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
	                                                            }
	                                                            
	                                                            else
	                                                            {
	                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
	                                                            }
	                                                        }
	                                                    }
	                                                ?>
	                                            </select>
	                                        </div>
	                                    </div>
	                                    <div class="row" style="margin-top:10px;">
	                                        <div class="form-actions center-align">
	                                            <button class="submit btn btn-danger" type="submit">
	                                                Add Away Substitute
	                                            </button>
	                                        </div>
	                                    </div>
	                                </div>
	                                
	                            </section>
	                
	                        </div>
	                    </div>
	                    <?php echo form_close();?>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="modal fade" id="add_faul_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">Cautions</h4>
	                </div>
	                <div class="modal-body">
	                    <?php echo form_open('soccer_management/referee/add_fixture_foul/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
	                    <div class="row">
	                        <div class="col-md-12">
	                        
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Team: </label>
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="team_id" id="faul_team_id" onchange="check_department_type_faul()">
	                                        <?php
	                                            if($fixture_teams->num_rows()> 0)
	                                            {
	                                                echo '<option value="0" selected>--Select Team--</option>';
	                                                foreach($fixture_teams->result() as $res)
	                                                {
	                                                    $team_id = $res->team_id;
	                                                    $team_name = $res->team_name;
	                                                    
	                                                    echo '<option value="'.$team_id.'">'.$team_name.'</option>';
	                                                }
	                                            }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Select Player: </label>
	                                
	                                <div class="col-lg-8">
	                                    <select name="foul_player_id" class="form-control" id="faul_team_players">
	                                        
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Foul type: </label>
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="foul_type_id">
	                                        <?php
	                                            if($foul_types->num_rows()> 0)
	                                            {
	                                                echo '<option value="--Select Offense--" selected>--Select Offense-</option>';
	                                                foreach($foul_types->result() as $fouls)
	                                                {
	                                                    $db_foul_type_id = $fouls->foul_type_id;
	                                                    $foul_type_name = $fouls->foul_type_name;
	                                                    
	                                                    if($db_foul_type_id == $foul_type_id)
	                                                    {
	                                                        echo '<option value="'.$db_foul_type_id.'" selected>'.$foul_type_name.'</option>';
	                                                    }
	                                                    
	                                                    else
	                                                    {
	                                                        echo '<option value="'.$db_foul_type_id.'">'.$foul_type_name.'</option>';
	                                                    }
	                                                }
	                                            }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Foul Minute </label>
	                                
	                                <div class="col-lg-8">
	                                    <input type="text" class="form-control" name="foul_minute" placeholder="Minute" value="<?php echo $foul_minute;?>">
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-lg-3 control-label">Action: </label>
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="action_id">
	                                        <?php
	                                            if($action_types->num_rows()> 0)
	                                            {
	                                                echo '<option value="--Select Action--" selected>--Select Action--</option>';
	                                                foreach($action_types->result() as $actions)
	                                                {
	                                                    $db_action_id = $actions->action_id;
	                                                    $action_name = $actions->action_name;
	                                                    
	                                                    if($db_action_id == $action_id)
	                                                    {
	                                                        echo '<option value="'.$db_action_id.'" selected>'.$action_name.'</option>';
	                                                    }
	                                                    
	                                                    else
	                                                    {
	                                                        echo '<option value="'.$db_action_id.'">'.$action_name.'</option>';
	                                                    }
	                                                }
	                                            }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="row" style="margin-top:10px;">
	                                <div class="col-md-12">
	                                    <div class="form-actions center-align">
	                                        <button class="submit btn btn-danger" type="submit">
	                                            Add caution
	                                        </button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <?php echo form_close();?>
	                </div>
	            </div>
	        </div>
	    </div>
	  
	    <div class="modal fade" id="add_match_timing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">Add Match Timings</h4>
	                </div>
	                <div class="modal-body">
	                    <?php echo form_open('soccer_management/referee/add_fixture_timing/'.$fixture_id.'/1', array("class" => "form-horizontal", "role" => "form"));
	                    ?>
	                    <div class="row">
	                        <div class="col-md-6">
	                            <h5>First Half Timings</h5>
	                            <div class="form-group">
	                                <label class="col-lg-4 control-label">Start time : </label>
	                            
	                                <div class="col-lg-8">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-clock-o"></i>
	                                        </span>
	                                        <input type="text" class="form-control"  placeholder="08:00:00" name="first_half_start_time" value="<?php echo $first_half_start_time;?>" >
	                                    </div>
	                                </div>
	                            </div>
	                                
	                            <div class="form-group">
	                                <label class="col-lg-4 control-label">End time : </label>
	                                
	                                <div class="col-lg-8">      
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-clock-o"></i>
	                                        </span>
	                                        <input type="text" class="form-control"  placeholder="08:45:00"  name="first_half_end_time" value="<?php echo $first_half_end_time;?>">
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-md-6">
	                            <h5>Second Half Timings</h5>
	                            <div class="form-group">
	                                <label class="col-lg-4 control-label">Start time : </label>
	                            
	                                <div class="col-lg-8">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-clock-o"></i>
	                                        </span>
	                                        <input type="text" class="form-control"  placeholder="09:00:00"  name="second_half_start_time" value="<?php echo $second_half_start_time;?>">
	                                    </div>
	                                </div>
	                            </div>
	                                
	                            <div class="form-group">
	                                <label class="col-lg-4 control-label">End time : </label>
	                                
	                                <div class="col-lg-8">      
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-clock-o"></i>
	                                        </span>
	                                        <input type="text" class="form-control"  placeholder="09:45:00"  name="second_half_end_time" value="<?php echo $second_half_end_time;?>">
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row" style="margin-top:10px;">
	                        <div class="col-md-12">
	                            <div class="form-actions center-align">
	                                <button class="submit btn btn-success" type="submit">
	                                    Save changes
	                                </button>
	                            </div>
	                        </div>
	                    </div>
	                    <?php echo form_close();?>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="modal fade" id="add_scores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">Scores</h4>
	                </div>
	                <div class="modal-body">
	                    <div class="row">
	                        <div class="col-md-4"><label class="col-lg-12 control-label">Team</label>
	                        </div>
	                        <div class="col-md-4"><label class="col-lg-12 control-label">Timings</label>
	                        </div>
	                        <div class="col-md-4"><label class="col-lg-12 control-label">Scores</label>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <div class="col-lg-12">
	                                   <p><strong>Home:</strong> <?php echo $home_team;?></p>
	                                   <p><strong> Away:</strong> <?php echo $away_team;?></p>

	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <div class="col-lg-12">
	                                <p><strong> First Half :</strong> <?php echo $first_half_start_time.' - '.$first_half_end_time;?></p>
	                                <p><strong> Second Half:</strong> <?php echo $second_half_start_time.' - '.$second_half_end_time;?></p>
	                                <p><strong> Penalties:</strong></p>
	                                    
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <div class="col-lg-12">
	                                    <p><?php echo $first_half_score;?></p>
	                                    <p><?php echo $second_half_score;?></p>
	                                    <p><?php echo $penalty_scores;?></p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	            </div>
			</div>
		</div>
	</div>
</section>
<section class="panel">
	<header class="panel-heading">
    	<h2 class="	panel-title">Add comments</h2>
        <?php
        if($module == 2)
        {
        	?>
        	 <!-- <a href="<?php echo base_url().'commissioner/league';?>" class="btn btn-sm btn-info pull-right" style="margin-top:-25px;margin-left:5px;">Back to fixtures</a> -->
       
        	<?php
        }
        else
        {
        	?>
        	<!--  <a href="<?php echo base_url().'soccer-management/add-league-duration/'.$league_id;?>" class="btn btn-sm btn-info pull-right" style="margin-top:-25px;margin-left:5px;">Back to season</a> -->
       
        	<?php

        }

        ?>
    </header>
    <div class="panel-body">
    	<?php
		$add_comment_result = '';
        if($comment_type->num_rows() > 0)
        {
			
            foreach($comment_type->result() as $comments)
            {
                $comment_id = $comments->comment_id;
                $comment_name = $comments->comment_name;
                $add_comment_result .=
                form_open('soccer-management/post-fixture-comment/'.$comment_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id).'
					<div class="col-md-6" style="margin-bottom: 0.5em;">
						<div class = "col-md-9">
							<div class="form-group">
								<label class="col-lg-5 control-label">'.$comment_name.' </label>
								<div class="col-lg-7">
									<textarea rows="4" cols="5" class="form-control" name="comment_fixture_description" placeholder="'.$comment_name.'" value="'.$comment_fixture_description.'"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-warning" title="Add '.$comment_name.'"><i class="fa fa-plus"></i>Add Fixture Comment</button>
						</div>
                    </div>
                '.form_close();
            }
        }
		echo $add_comment_result;
        ?>
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
    	<h2 class="	panel-title">Add assessments</h2>
    </header>
    <div class="panel-body">
    	<?php
		$add_assessment_result = '';
        if($assessment_type->num_rows() > 0)
        {
			
            foreach($assessment_type->result() as $assessments)
            {
                $assessment_id = $assessments->assessment_id;
                $assessment_name = $assessments->assessment_name;
				$assessment_parent  =$assessments->assessment_parent;
				$assessment_parent_name = $this->fixture_model->get_parent_name($assessment_parent);
                $add_assessment_result .=
				form_open('soccer-management/post-fixture-assignment/'.$assessment_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id).'
                
                    <div class="col-md-6" style="margin-bottom: 0.5em;">
						<div class = "col-md-9">
							<div class="form-group">
								<div class="col-lg-12">
									  <input type="radio" name="assessment_name" value="1"> 1
									  <input type="radio" name="assessment_name" value="2"> 2
									  <input type="radio" name="assessment_name" value="3"> 3
									  <input type="radio" name="assessment_name" value="4"> 4
									  <input type="radio" name="assessment_name" value="5"> 5
									  <input type="radio" name="assessment_name" value="6"> 6
									  <input type="radio" name="assessment_name" value="7"> 7
									  <input type="radio" name="assessment_name" value="8"> 8
									  <input type="radio" name="assessment_name" value="9"> 9
									  <input type="radio" name="assessment_name" value="10"> 10
									<textarea rows="4" cols="5" class="form-control" name="assessment_fixture_description" placeholder="'.$assessment_name.'" value="'.$assessment_fixture_description.'"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-success" title="Add '.$assessment_name.'"><i class="fa fa-plus"></i>Add Fixture Assessment</a>
						</div>
                    </div>
                '.form_close();
            }
        }
		echo $add_assessment_result;
        ?>
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Goals</h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $result;?>
        </div> 
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Fouls</h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $foul_result;?>
        </div> 
    </div>
</section>  
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Comments</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $comment_result;?>
        </div> 
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Assessments</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $assessment_result;?>
        </div> 
    </div>
</section> 


<script type="text/javascript">
    
    function check_department_type()
    {
        var player_team_id = document.getElementById("player_team_id").value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_players/"+player_team_id+"/"+fixture_id;

        $.get( data_url , function( data ) 
        {
            $( "#team_players" ).html( data );
        });
    }
    function check_department_type_faul()
    {
        var player_team_id = document.getElementById("faul_team_id").value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_players/"+player_team_id+"/"+fixture_id;
     
        $.get( data_url , function( data ) 
        {
            $( "#faul_team_players" ).html( data );
        });
    }
     function check_department_type_substitution()
    {
        var player_team_id = document.getElementById("sub_team_id").value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_league_substitutions/"+player_team_id+"/"+fixture_id;
       
        $.get( data_url , function( data ) 
        {
            $( "#sub_team_players" ).html( data );
        });
    }
    function check_goal_type()
    {
          var goal_type_id = document.getElementById("goal_type_id").value;

          var myTarget2 = document.getElementById("goal_type_score");


          if(goal_type_id == 1)
          {
            // this is a penlty
            myTarget2.style.display = 'block';
          }
          else
          {
            myTarget2.style.display = 'none';
          }
    }
</script>