<?php
//league data
$league_team_id = set_value('league_team_id');
$fixture_team_type_id = set_value('fixture_team_type_id');
$result = '';
if($fixture_teams->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team name</a></th>
						<th>Team type </a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($fixture_teams->result() as $all_teams)
	{
		$team_name = $all_teams->team_name;
		$fixture_team_type_name = $all_teams->fixture_team_type_name;
		$league_team_id = $all_teams->league_team_id;
		$team_id = $all_teams->team_id;
		$fixture_team_id = $all_teams->fixture_team_id;
		$count++;
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$fixture_team_type_name.'</td>
				<td><a class="btn btn-warning" href="'.site_url().'soccer-management/add-fixture-player/'.$team_id.'/'.$fixture_team_id.'/'.$league_duration_id.'/'.$league_id.'" title="Add team "><i class="fa fa-users"></i>Add Players</a></td>
					<td><a class="btn btn-success" href="'.site_url().'soccer-management/edit-team/'.$fixture_team_id.'" class="btn btn-sm btn-success" title="Edit '.$team_name.'"><i class="fa fa-pencil"></i> Edit Team</a></td>
					<td><a class="btn btn-info" href="'.site_url().'soccer-management/remove-league-team/'.$fixture_team_id.'/'.$league_id.'" class="btn btn-sm btn-danger" title="Remove '.$team_name.' from league"><i class="fa fa-pencil"></i> Remove Team</a></td>
				</tr>
				';
	}
}
else
{
	$result .= 'There are no teams for this fixture';
}
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url().'soccer-management/add-league-duration/'.$league_id;?>" class="btn btn-info pull-right">Back to season</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Team: </label>
            <?php //var_dump($teams); die();?>
			<?php //var_dump($league_teams); die();?>
            <div class="col-lg-7">
                <select class="form-control" name="league_team_id">
                	<?php
                    	if($season_teams->num_rows()> 0)
						{
							echo '<option value="--Select Team--" selected>--Select Team--</option>';
							foreach($season_teams->result() as $res)
							{
								$db_league_team_id = $res->league_team_id;
								$team_name = $res->team_name;
								
								if($db_league_team_id == $league_team_id)
								{
									echo '<option value="'.$db_league_team_id.'" selected>'.$team_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_league_team_id.'">'.$team_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Team Type: </label>
            <?php //var_dump($teams); die();?>
			<?php //var_dump($league_teams); die();?>
            <div class="col-lg-7">
                <select class="form-control" name="fixture_team_type_id">
                	<?php
                    	if($team_types->num_rows()> 0)
						{
							foreach($team_types->result() as $res)
							{
								$db_fixture_team_type_id = $res->fixture_team_type_id;
								$fixture_team_type_name = $res->fixture_team_type_name;
								
								if($db_fixture_team_type_id == $fixture_team_type_id)
								{
									echo '<option value="'.$db_fixture_team_type_id.'" selected>'.$fixture_team_type_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_fixture_team_type_id.'">'.$fixture_team_type_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add team
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>
<section class="panel">

    <header class="panel-heading">
        <h2 class="panel-title">Teams in fixture <?php echo $fixture_id;?></h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
</section>