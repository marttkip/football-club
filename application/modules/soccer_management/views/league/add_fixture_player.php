<?php
//league data
$player_id = set_value('player_id');
$fixture_player_type_id = set_value('fixture_player_type_id');
$result = '';
//var_dump($fixture_players);die();
if($all_fixture_players->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Player name</th>
						<th>Position</th>
						<th>Type</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($all_fixture_players->result() as $fixture_player)
	{
		$player_fname = $fixture_player->player_fname;
		$player_id = $fixture_player->player_id;
		$player_onames = $fixture_player->player_onames;
		$player_position = $fixture_player->player_position;
		$fixture_team_player_type = $fixture_player->fixture_team_player_type_id;
		$player_name = $player_fname . ' ' .$player_onames;
		$player_type_name = $this->league_model->get_player_type($fixture_team_player_type);
		
		$count++;
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$player_name.'</td>
					<td>'.$player_position.'</td>
					<td>'.$player_type_name.'</td>
					<td><a href="'.site_url().'soccer-management/edit-player/'.$player_id.'" class="btn btn-sm btn-success" title="Edit '.$player_name.'"><i class="fa fa-pencil"></i>Edit Player Details</a></td>
				</tr>
				';
	}
}
else
{
	$result .= 'No player added for this fixture for the team';
}
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url().'soccer-management/add-league-duration-fixture/'.$league_duration_id.'/'.$league_id;?>" class="btn btn-info pull-right">Back to fixture</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Player: </label>
            <div class="col-lg-7">
                <select class="form-control" name="player_id">
                	<?php
                    	if($fixture_players->num_rows()> 0)
						{
							echo '<option value="--Select Player--" selected>--Select Player--</option>';
							foreach($fixture_players->result() as $res)
							{
								$db_player_id = $res->player_id;
								$player_fname = $res->player_fname;
								$player_onames = $res->player_onames;
								$player_name = $player_fname. ' ' .$player_onames;
								
								if($db_player_id == $player_id)
								{
									echo '<option value="'.$db_player_id.'" selected>'.$player_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_player_id.'">'.$player_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Player Type: </label>
            <div class="col-lg-7">
                <select class="form-control" name="fixture_player_type_id">
                	<?php
                    	if($player_type->num_rows()> 0)
						{
							echo '<option value="--Select Lineup--" selected>--Select Lineup--</option>';
							foreach($player_type->result() as $res)
							{
								$db_fixture_player_type_id = $res->fixture_player_type_id;
								$fixture_player_type_name = $res->fixture_player_type_name;
								
								if($db_fixture_player_type_id == $fixture_player_type_id)
								{
									echo '<option value="'.$db_fixture_player_type_id.'" selected>'.$fixture_player_type_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_fixture_player_type_id.'">'.$fixture_player_type_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add player
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>
<section class="panel">

    <header class="panel-heading">
        <h2 class="panel-title">Players for <?php echo $team_name;?></h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
</section>