<?php
//referees
$fixture_referee_result = '';
if($fixture_referees->num_rows() > 0)
{
	$referee_count = 0;
	$fixture_referee_result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Referee name</th>
						<th>Referee type</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($fixture_referees->result() as $referee_fixture)
	{
		$referee_fname = $referee_fixture->referee_fname;
		$referee_id = $referee_fixture->referee_id;
		$referee_type = $referee_fixture->referee_type_name;
		$referee_onames = $referee_fixture->referee_onames;
		//$referee_payment_status  =$referee_fixture->referee_payment_status;
		
		//get ref status in the referee queue
		$referee_payment_status = $this->league_model->get_referee_payment_status($referee_id,$fixture_id,1);
		$referee_name = $referee_fname . ' ' .$referee_onames;
		$referee_count++;
		
		if($referee_payment_status==0)
		{
			$button = '<a href="'.site_url().'soccer-management/send-league-referee-to-admin/'.$referee_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" class="btn btn-sm btn-warning" title="Send '.$referee_name.' to admin for approval"><i class=""></i>Send to Admin</a>';
		}
		elseif($referee_payment_status==1)
		{
			$button = '<a href="'.site_url().'soccer-management/send-league-referee-to-accounts/'.$referee_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" class="btn btn-sm btn-info" title="Send '.$referee_name.' to accounts for payment"><i class=""></i>Send to Accounts</a>';
		}
		elseif($referee_payment_status==2)
		{
			$button = '<a href="'.site_url().'soccer-management/send-league-payments-to-referee/'.$referee_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" class="btn btn-sm btn-success" title="Send '.$referee_name.' to accounts for payment"><i class=""></i>Make Payment</a>';
		}
		else
		{
			$button = '<a href="#" class="btn btn-sm btn-success"><i class=""></i>Request recieved by accounts</a>';
		}
		$fixture_referee_result .=
				'
				<tr>
					<td>'.$referee_count.'</td>
					<td>'.$referee_name.'</td>
					<td>'.$referee_type.'</td>
					<td>'.$button.'</td>
				</tr>
				';
	}
	$fixture_referee_result .=
				'</tbody>
			</table>
			';
}
else
{
	$fixture_referee_result .= 'No referee asssigned for this fixture';
}

$commissioner_result = '';
if($fixture_commissioners->num_rows() > 0)
{
	$commissioner_count = 0;
	$commissioner_result .= 
			'<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Commissioner name</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
			';
	foreach($fixture_commissioners->result() as $commissioners)
	{
		$personnel_id = $commissioners->personnel_id;
		$personnel_fname = $commissioners->personnel_fname;
		$personnel_onames = $commissioners->personnel_onames;

		$commissioner_payment_status = $this->league_model->get_commissioner_payment_status($personnel_id,$fixture_id,1);
		$personnel_name = $personnel_fname. ' '.$personnel_onames;
		$commissioner_count++;

		if($commissioner_payment_status==0)
		{
			$button = '<a href="'.site_url().'soccer-management/send-league-commissioner-to-admin/'.$personnel_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" class="btn btn-sm btn-warning" title="Send '.$personnel_name.' to admin for approval"><i class=""></i>Send to Admin</a>';
		}
		elseif($commissioner_payment_status==1)
		{
			$button = '<a href="'.site_url().'soccer-management/send-league-commissioner-to-accounts/'.$personnel_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" class="btn btn-sm btn-info" title="Send '.$personnel_name.' to accounts for payment"><i class=""></i>Send to Accounts</a>';
		}
		elseif($commissioner_payment_status==2)
		{
			$button = '<a href="'.site_url().'soccer-management/send-league-payments-to-commissioner/'.$personnel_id.'/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id.'" class="btn btn-sm btn-success" title="Send '.$personnel_name.' to accounts for payment"><i class=""></i>Make Payment</a>';
		}
		else
		{
			$button = '<a href="#" class="btn btn-sm btn-success"><i class=""></i>Request recieved by accounts</a>';
		}
		
		$commissioner_result .=
				'
				<tr>
					<td>'.$commissioner_count.'</td>
					<td>'.$personnel_name.'</td>
					<td>'.$button.'</td>
				</tr>';
	}
	$commissioner_result .=
				'
				</tbody>
			</table>';
}
else
{
	$commissioner_result .= 'No commissioner added to the fixture';
}
?>    
<div class="row">
	<section class="panel">
		<div class="col-md-12">
			<?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}
			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{

				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
		?>
		<a href="<?php echo base_url().'soccer-management/add-tournament-duration-fixture/'.$league_duration_id.'/'.$league_id;?>" class="btn btn-sm btn-info pull-right">Back to fixture</a>
        
			
		</div>
	
    </section>
</div>
<div class="row">
	<div class="col-md-6">
		<section class="panel">
		    <header class="panel-heading">
		        <h2 class="panel-title"> Referees for fixture <?php echo $fixture_id;?></h2>
		    </header>
		    <div class="panel-body">
		    	
		         <?php echo $fixture_referee_result;?>
		    </div>
		</section>
		
	</div>
	<div class="col-md-6">
		<section class="panel">
		    <header class="panel-heading">
		        <h2 class="panel-title"> Commissioners for fixture <?php echo $fixture_id;?></h2>
		    </header>
		    <div class="panel-body">
		    	<?php echo $commissioner_result;?>
		    </div>
		</section>
	</div>
</div>

