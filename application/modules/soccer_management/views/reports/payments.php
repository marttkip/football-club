<?php
$payment_result = '';
	if($league_referees->num_rows() > 0)
	{
		$count = 0;
		$payment_result .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Referee Name</a></th>
					<th>Referee Role</a></th>
					<th>Fixture Date</a></th>
					<th>Game</a></th>
					<th>Total Invoiced</th>
					<th>Total Payments</th>
					<th>Balance</th>
				</tr>
			</thead>
			<tbody>
		';
		foreach($league_referees->result() as $league_payments)
		{
			$league_payment_button = '';
			
			$league_referee_id = $league_payments->referee_id;
			$league_referee_fname = $league_payments->referee_fname;
			$league_referee_onames = $league_payments->referee_onames;
			$league_referee_name = $league_referee_fname.' '.$league_referee_onames;
			
			$league_referee_type = $league_payments->referee_type_id;
			$league_referee_role = $this->payments_model->get_referee_role($league_referee_type);
			$fixture_id = $league_payments->fixture_id;
			$league_fixture_type = 1;
			
			//get ficture details
			$league_home_team = $this->league_model->get_home_team($fixture_id);
			$league_away_team = $this->league_model->get_away_team($fixture_id);
			$league_fixture_date = $this->league_model->get_league_fixture_date($fixture_id);
			$count++;
			
			$league_referee_payments_queue_id = $this->payments_model->get_league_queue_id($league_referee_id, $fixture_id);
			$league_referee_invoice = $this->league_model->get_ref_invoice($fixture_id,$league_referee_id);
			$league_referee_payments = $this->league_model->get_ref_payment($league_referee_payments_queue_id);
			
			$payment_result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$league_referee_name.'</td>
								<td>'.$league_referee_role.'</td>
								<td>'.date('jS M Y',strtotime($league_fixture_date)).'</td>
								<td>'.$league_home_team.' VS '.$league_away_team.'</td>
								<td>'.number_format($league_referee_invoice,2).'</td>
								<td>'.number_format($league_referee_payments,2).'</td>
								<td>'.number_format($league_referee_invoice - $league_referee_payments,2).'</td>
							</tr>			
						';
		}
	}
	if($tournament_referees->num_rows() > 0)
	{
		foreach($tournament_referees->result() as $referees)
		{
			$payment_button = '';
			$referee_id = $referees->referee_id;
			$referee_fname = $referees->referee_fname;
			$referee_onames = $referees->referee_onames;
			$referee_name = $referee_onames.' '.$referee_fname;
			
			$referee_type = $referees->referee_type_id;
			$referee_role = $this->payments_model->get_referee_role($referee_type);
			$tournament_fixture_id = $referees->tournament_fixture_id;
			
			//get ficture details
			$home_team = $this->tournament_model->get_home_team($tournament_fixture_id);
			$away_team = $this->tournament_model->get_away_team($tournament_fixture_id);
			$fixture_date = $this->payments_model->get_fixture_date($tournament_fixture_id);
			$count++;
			$fixture_type = 2;
			
			$referee_payments_queue_id = $this->payments_model->get_queue_id($referee_id, $tournament_fixture_id);
			$referee_invoice = $this->tournament_model->get_ref_invoice($tournament_fixture_id,$referee_id);
			$referee_payments = $this->tournament_model->get_ref_payment($referee_payments_queue_id);
			$payment_result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$referee_name.'</td>
								<td>'.$referee_role.'</td>
								<td>'.date('jS M Y',strtotime($fixture_date)).'</td>
								<td>'.$home_team.' VS '.$away_team.'</td>
								<td>'.number_format($referee_invoice,2).'</td>
								<td>'.number_format($referee_payments,2).'</td>
								<td>'.number_format($referee_invoice - $referee_payments,2).'</td>
							</tr>			
						';
		
		}
		$payment_result .= 
		'		</tbody>
			</table>
			';
	}
?>

<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
                <div class="table-responsive">
            
            <?php echo $payment_result;?>
    
        </div>
    </div>
</section>