<?php
//coach data
$coach_onames = set_value('coach_onames');
$coach_fname = set_value('coach_fname');
$coach_dob = set_value('coach_dob');
$coach_email = set_value('coach_email');
$coach_phone = set_value('coach_phone');
$coach_address = set_value('coach_address');
$civil_status_id = set_value('civil_status_id');
$coach_locality = set_value('coach_locality');
$title_id = set_value('title_id');
$gender_id = set_value('gender_id');
$coach_city = set_value('coach_city');
$coach_number = set_value('coach_number');
$coach_post_code = set_value('coach_post_code');
$coach_national_id_number = set_value('coach_national_id_number'); 
$engagement_date = set_value('engagement_date');
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>soccer-management/coach" class="btn btn-info pull-right">Back to coach</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Title: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="title_id">
                	<?php
                    	if($titles->num_rows() > 0)
						{
							$title = $titles->result();
							
							foreach($title as $res)
							{
								$db_title_id = $res->title_id;
								$title_name = $res->title_name;
								
								if($db_title_id == $title_id)
								{
									echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Other Names: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_onames" placeholder="Other Names" value="<?php echo $coach_onames;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_fname" placeholder="First Name" value="<?php echo $coach_fname;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Coach number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_number" placeholder="Coach number" value="<?php echo $coach_number;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">ID number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_national_id_number" placeholder="ID number" value="<?php echo $coach_national_id_number;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Date of Birth: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="coach_dob" placeholder="Date of Birth" value="<?php echo $coach_dob;?>">
                </div>
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Gender: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="gender_id">
                	<?php
                    	if($genders->num_rows() > 0)
						{
							$gender = $genders->result();
							
							foreach($gender as $res)
							{
								$db_gender_id = $res->gender_id;
								$gender_name = $res->gender_name;
								
								if($db_gender_id == $gender_id)
								{
									echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
    
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Civil Status: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="civil_status_id">
                	<?php
                    	if($civil_statuses->num_rows() > 0)
						{
							$status = $civil_statuses->result();
							
							foreach($status as $res)
							{
								$status_id = $res->civil_status_id;
								$status_name = $res->civil_status_name;
								
								if($status_id == $civil_status_id)
								{
									echo '<option value="'.$status_id.'" selected>'.$status_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$status_id.'">'.$status_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_email" placeholder="Email Address" value="<?php echo $coach_email;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Phone: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_phone" placeholder="Phone" value="<?php echo $coach_phone;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Branch: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_locality" placeholder="Branch" value="<?php echo $coach_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Sub Branch: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_address" placeholder="Sub Branch" value="<?php echo $coach_address;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Level of Education: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_city" placeholder="Level of Education" value="<?php echo $coach_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Coaching Qualification: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="coach_post_code" placeholder="Coaching Qualification" value="<?php echo $coach_post_code;?>">
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-5 control-label">Engagement Date: </label>
            
             <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="engagement_date" placeholder="Engagement Date" value="<?php echo $engagement_date;?>">
                </div>
            </div>
        </div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add coach
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>