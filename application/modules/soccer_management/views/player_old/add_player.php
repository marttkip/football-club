<?php
//player data
$player_onames = set_value('player_onames');
$player_fname = set_value('player_fname');
$player_dob = set_value('player_dob');
$player_email = set_value('player_email');
$player_position = set_value('player_position');
$player_phone = set_value('player_phone');
$player_address = set_value('player_address');
$civil_status_id = set_value('civil_status_id');
$player_locality = set_value('player_locality');
$title_id = set_value('title_id');
$team_id = set_value('team_id');
$gender_id = set_value('gender_id');
$player_city = set_value('player_city');
$player_number = set_value('player_number');
$player_post_code = set_value('player_post_code');
$player_national_id_number = set_value('player_national_id_number'); 
$engagement_date = set_value('engagement_date');
$player_duration = set_value('player_duration');
$player_contract_value = set_value('player_contract_value');
$bank_account_number = set_value('bank_account_number');
$bank_branch_code = set_value('bank_branch_code');
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>soccer-management/player" class="btn btn-info pull-right">Back to player</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Team: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="team_id">
                	<?php
                    	if($teams->num_rows() > 0)
						{
							$team = $teams->result();
							
							foreach($team as $res)
							{
								$db_team_id = $res->team_id;
								$team_name = $res->team_name;
								
								if($db_team_id == $team_id)
								{
									echo '<option value="'.$db_team_id.'" selected>'.$team_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_team_id.'">'.$team_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Title: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="title_id">
                	<?php
                    	if($titles->num_rows() > 0)
						{
							$title = $titles->result();
							
							foreach($title as $res)
							{
								$db_title_id = $res->title_id;
								$title_name = $res->title_name;
								
								if($db_title_id == $title_id)
								{
									echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Other Names: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_onames" placeholder="Other Names" value="<?php echo $player_onames;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_fname" placeholder="First Name" value="<?php echo $player_fname;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Player number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_number" placeholder="Player number" value="<?php echo $player_number;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Player position: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_position" placeholder="Player position(eg Striker)" value="<?php echo $player_position;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">ID number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_national_id_number" placeholder="ID number" value="<?php echo $player_national_id_number;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Date of Birth: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="player_dob" placeholder="Date of Birth" value="<?php echo $player_dob;?>">
                </div>
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Gender: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="gender_id">
                	<?php
                    	if($genders->num_rows() > 0)
						{
							$gender = $genders->result();
							
							foreach($gender as $res)
							{
								$db_gender_id = $res->gender_id;
								$gender_name = $res->gender_name;
								
								if($db_gender_id == $gender_id)
								{
									echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Bank account number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="bank_account_number" placeholder="Bank account number" value="<?php echo $bank_account_number;?>">
            </div>
        </div>
	</div>
    
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Bank branch code: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="bank_branch_code" placeholder="Bank branch code" value="<?php echo $bank_branch_code;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Civil Status: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="civil_status_id">
                	<?php
                    	if($civil_statuses->num_rows() > 0)
						{
							$status = $civil_statuses->result();
							
							foreach($status as $res)
							{
								$status_id = $res->civil_status_id;
								$status_name = $res->civil_status_name;
								
								if($status_id == $civil_status_id)
								{
									echo '<option value="'.$status_id.'" selected>'.$status_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$status_id.'">'.$status_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_email" placeholder="Email Address" value="<?php echo $player_email;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Phone: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_phone" placeholder="Phone" value="<?php echo $player_phone;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Residence: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_locality" placeholder="Residence" value="<?php echo $player_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Address: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_address" placeholder="Address" value="<?php echo $player_address;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">City: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_city" placeholder="City" value="<?php echo $player_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Post code: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_post_code" placeholder="Post code" value="<?php echo $player_post_code;?>">
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-5 control-label">Engagement Date: </label>
            
             <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="engagement_date" placeholder="Engagement Date" value="<?php echo $engagement_date;?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Duration: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_duration" placeholder="Duration(months)" value="<?php echo $player_duration;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Contract Value: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_contract_value" placeholder="Contract Value" value="<?php echo $player_contract_value;?>">
            </div>
        </div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add player
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>