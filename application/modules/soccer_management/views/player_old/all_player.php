<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'soccer-management/player/team_id/'.$order_method.'/'.$page.'">Team</a></th>
						<th><a href="'.site_url().'soccer-management/player/player_number/'.$order_method.'/'.$page.'">Player number</a></th>
						<th><a href="'.site_url().'soccer-management/player/player_onames/'.$order_method.'/'.$page.'">Other names</a></th>
						<th><a href="'.site_url().'soccer-management/player/player_fname/'.$order_method.'/'.$page.'">First name</a></th>
						<th><a href="'.site_url().'soccer-management/player/player_phone/'.$order_method.'/'.$page.'">Phone number</a></th>
						<th><a href="'.site_url().'soccer-management/player/player_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$player_id = $row->player_id;
				$team_id = $row->team_id;
				$player_fname = $row->player_fname;
				$player_onames = $row->player_onames;
				$player_phone = $row->player_phone;
				$player_email = $row->player_email;
				$player_number = $row->player_number;
				$player_status = $row->player_status;
				$player_name = $player_fname.' '.$player_onames;
				
				//status
				if($player_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($player_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-player/'.$player_id.'" onclick="return confirm(\'Do you want to activate '.$player_name.'?\');" title="Activate '.$player_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($player_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-player/'.$player_id.'" onclick="return confirm(\'Do you want to deactivate '.$player_name.'?\');" title="Deactivate '.$player_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				//get team
				$team = '';
				if($teams->num_rows() > 0)
				{
					foreach($teams->result() as $res)
					{
						$team_id2 = $res->team_id;
						if($team_id == $team_id2)
						{
							$team = $res->team_name;
						}
					}
				}
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$team.'</td>
						<td>'.$player_number.'</td>
						<td>'.$player_onames.'</td>
						<td>'.$player_fname.'</td>
						<td>'.$player_phone.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'soccer-management/edit-player/'.$player_id.'" class="btn btn-sm btn-success" title="Edit '.$player_name.'"><i class="fa fa-pencil"></i>Edit Player</a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'soccer-management/delete-player/'.$player_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$player_name.'?\');" title="Delete '.$player_name.'"><i class="fa fa-trash"> Delete</i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no player";
		}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search player</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("soccer_management/player/search_player", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Team: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="team_id">
                            	<option value="">---Select Team---</option>
                                <?php
                                    if($teams->num_rows() > 0){
                                        foreach($teams->result() as $row):
                                            $team_name = $row->team_name;
                                            $team_id= $row->team_id;
                                            ?><option value="<?php echo $team_id; ?>" ><?php echo $team_name; ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Player Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="player_number" placeholder="Player number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">First name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="player_fname" placeholder="First name">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Other names: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="player_onames" placeholder="Other names">
                        </div>
                    </div>
            
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4">
                        	<div class="center-align">
                            	<button type="submit" class="btn btn-info btn-sm">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
							</header>
							<div class="panel-body">
                            	<?php
								$search = $this->session->userdata('player_search_title2');
								
								if(!empty($search))
								{
									echo '<h6>Filtered by: '.$search.'</h6>';
									echo '<a href="'.site_url().'soccer_management/player/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
								}
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>soccer-management/export-player" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    	<a href="<?php echo site_url();?>soccer-management/add-player" class="btn btn-sm btn-info pull-right">Add Player</a>
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>