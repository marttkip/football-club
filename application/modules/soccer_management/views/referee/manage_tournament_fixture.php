<?php
//add fixture teams
$fixture_tournament_team_id = set_value('tournament_team_id');
$tournament_fixture_team_type_id = set_value('tournament_fixture_team_type_id');
$result = '';
if($tournament_fixture_teams->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team name</a></th>
						<th>Team type </a></th>
						<th colspan="1">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($tournament_fixture_teams->result() as $all_teams)
	{
		$team_name = $all_teams->team_name;
		$fixture_team_type_name = $all_teams->fixture_team_type_name;
		$tournament_team_id = $all_teams->tournament_team_id;
		$team_id = $all_teams->team_id;
		$tournament_fixture_team_id = $all_teams->tournament_fixture_team_id;
		$count++;
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$fixture_team_type_name.'</td>
				<td><a class="btn btn-sm btn-warning" href="'.site_url().'soccer-management/add-tournament-fixture-player/'.$team_id.'/'.$tournament_fixture_team_id.'/'.$league_duration_id.'/'.$tournament_id.'/'.$tournament_fixture_id.'" title="Add team "><i class="fa fa-users"></i> Add Players</a></td>
				</tr>
				';
	}
	$result .=
				'</tbody>
			</table>
			';
}
else
{
	$result .= 'There are no teams for this fixture';
}
?>          
			<section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Teams in fixture <?php echo $tournament_fixture_id;?></h2>
                    <div class="row" style="margin-top:-25px;">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url().'referee/tournament';?>" class="btn btn-sm btn-info pull-right fa fa-arrow-left"> Back to tournaments</a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                	
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                  
                   
					

					<div class="table-responsive">
						
						<?php echo $result;?>
				
					</div>
				</div>
			</section>