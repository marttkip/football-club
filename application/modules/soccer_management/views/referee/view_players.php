<?php
$result = '';
$away_team_players = $home_team_players = '';
if($home_players->num_rows() > 0)
{
	$count = 0;
	$home_team_players .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>No</th>
						<th>Name</th>
						<th>Licence Number</th>
						
					</tr>
				</thead>
				  <tbody>
				';
	foreach($home_players->result() as $home)
	{

		$player_fname = $home->player_fname;
		$player_onames = $home->player_onames;
		$player_name = $player_fname.' '.$player_onames;

		$licence_number = $home->licence_number;
		$player_number = $home->player_number;
		
		$count++;
		$home_team_players .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_number.'</td>
						<td>'.$player_name.'</td>
						<td>'.$licence_number.'</td>
						
					</tr>
					';
		
	}
	$home_team_players .='</tbody>
				</table>';
}
else
{
	$home_team_players .= "No players were added for ".$home_team;
}
if($away_players->num_rows() > 0)
{
	$count = 0;
	$away_team_players .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>No</th>
						<th>Name</th>
						<th>Licence Number</th>
						
					</tr>
				</thead>
				  <tbody>
				';
	foreach($away_players->result() as $away)
	{

		$player_fname1 = $away->player_fname;
		$player_onames1 = $away->player_onames;
		$player_name1 = $player_fname1.' '.$player_onames1;

		$licence_number1 = $away->licence_number;
		$player_number1 = $away->player_number;
		
		$count++;
		$away_team_players .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_number1.'</td>
						<td>'.$player_name1.'</td>
						<td>'.$licence_number1.'</td>
						
					</tr>
					';
		
	}
	$away_team_players .='</tbody>
				</table>';
}
else
{
	$away_team_players .= "No players were added for ".$home_team;
}

?>


<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Players</h2>
    </header>
    <div class="panel-body" onLoad="window.print();return false;">
    	<div class="table-responsive">
        	<strong><?php echo $home_team;?> Players</strong>
            <?php echo $home_team_players;?>
            </br>
            <strong><?php echo $away_team;?> Players</strong>
            <?php echo $away_team_players;?>
        </div> 
    </div>
</section> 