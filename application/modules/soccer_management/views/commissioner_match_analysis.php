<?php
$assessment_result = $comment_result = '';
$comment_fixture_description = set_value('comment_fixture_description');
$assessment_fixture_description = set_value('assessment_fixture_description');
if($all_fixture_comments->num_rows() > 0)
{
	$comment_result .= 
				'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Comment</th>
							<th>Comment description</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	$count=0;	
	foreach($all_fixture_comments->result() as $comments_of_fixture)
	{
		$comment_id = $comments_of_fixture->comment_id;
		$comment_fixture_description = $comments_of_fixture->comment_fixture_description;
		$comment_name = $comments_of_fixture->comment_name;
		$count++;
		$comment_result .= 
						'<tr>
							<td>'.$count.'</td>
							<td>'.$comment_name.'</td>
							<td>'.$comment_fixture_description.'</td>
						</tr>
						';
	}
	$comment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$comment_result .= 'No comments for this fixture';
}
if($all_assessment_fixtures->num_rows() > 0)
{
	$assessment_count = 0;
	$assessment_result .=
	'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Assessmnet</th>
							<th>Comment description</th>
							<th>Rating</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	foreach($all_assessment_fixtures->result() as $assessments_of_fixture)
	{
		$assessment_id = $assessments_of_fixture->assessment_id;
		$assessment_fixture_description = $assessments_of_fixture->assessment_fixture_description;
		$assessment_name = $assessments_of_fixture->assessment_name;
		$assessment_rating = $assessments_of_fixture->assessment_fixture_rating;
		$assessment_count++;
		$assessment_result .= 
						'<tr>
							<td>'.$assessment_count.'</td>
							<td>'.$assessment_name.'</td>
							<td>'.$assessment_fixture_description.'</td>
							<td>'.$assessment_rating.'</td>
						</tr>
						';
	}
	$assessment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$assessment_result .= 'No assessments added to this fixture';
}
?>
<section class="panel">
	<header class="panel-heading">
    	<h2 class="	panel-title">Add comments</h2>
    </header>
    <div class="panel-body">
    	<?php
		$add_comment_result = '';
        if($comment_type->num_rows() > 0)
        {
			
            foreach($comment_type->result() as $comments)
            {
                $comment_id = $comments->comment_id;
                $comment_name = $comments->comment_name;
                $add_comment_result .=
                form_open('soccer_management/commissioners/post_fixture_comment/'.$comment_id.'/'.$fixture_id).'
					<div class="col-md-6" style="margin-bottom: 0.5em;">
						<div class = "col-md-9">
							<div class="form-group">
								<label class="col-lg-5 control-label">'.$comment_name.' </label>
								<div class="col-lg-7">
									<textarea rows="4" cols="5" class="form-control" name="comment_fixture_description" placeholder="'.$comment_name.'" value="'.$comment_fixture_description.'"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-warning" title="Add '.$comment_name.'"><i class="fa fa-plus"></i>Add Fixture Comment</button>
						</div>
                    </div>
                '.form_close();
            }
        }
		echo $add_comment_result;
        ?>
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
    	<h2 class="	panel-title">Add assessments</h2>
    </header>
    <div class="panel-body">
    	<?php
		$add_assessment_result = '';
        if($assessment_type->num_rows() > 0)
        {
			
            foreach($assessment_type->result() as $assessments)
            {
                $assessment_id = $assessments->assessment_id;
                $assessment_name = $assessments->assessment_name;
				$assessment_parent  =$assessments->assessment_parent;
				$assessment_parent_name = $this->fixture_model->get_parent_name($assessment_parent);
                $add_assessment_result .=
				form_open('soccer_management/commissioners/post_fixture_assignment/'.$assessment_id.'/'.$fixture_id).'
                
                    <div class="col-md-6" style="margin-bottom: 0.5em;">
						<div class = "col-md-9">
							<div class="form-group">
								<div class="col-lg-12">
									  <input type="radio" name="assessment_name" value="1"> 1
									  <input type="radio" name="assessment_name" value="2"> 2
									  <input type="radio" name="assessment_name" value="3"> 3
									  <input type="radio" name="assessment_name" value="4"> 4
									  <input type="radio" name="assessment_name" value="5"> 5
									  <input type="radio" name="assessment_name" value="6"> 6
									  <input type="radio" name="assessment_name" value="7"> 7
									  <input type="radio" name="assessment_name" value="8"> 8
									  <input type="radio" name="assessment_name" value="9"> 9
									  <input type="radio" name="assessment_name" value="10"> 10
									<textarea rows="4" cols="5" class="form-control" name="assessment_fixture_description" placeholder="'.$assessment_name.'" value="'.$assessment_fixture_description.'"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-success" title="Add '.$assessment_name.'"><i class="fa fa-plus"></i>Add Fixture Assessment</a>
						</div>
                    </div>
                '.form_close();
            }
        }
		echo $add_assessment_result;
        ?>
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Comments</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $comment_result;?>
        </div> 
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Assessments</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $assessment_result;?>
        </div> 
    </div>
</section> 