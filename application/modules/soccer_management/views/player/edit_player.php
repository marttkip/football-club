<?php
//player data
$row = $player->row();

$player_onames = $row->player_onames;
$player_fname = $row->player_fname;
$player_dob = $row->player_dob;
$player_position = $row->player_position;
$player_email = $row->player_email;
$player_phone = $row->player_phone;
$player_address = $row->player_address;
$player_locality = $row->player_locality;
$title_id = $row->title_id;
$engagement_date = $row->engagement_date;
$contract_end_date = $row->contract_end_date;
$team_id = $row->team_id;
$player_duration = $row->player_duration;
$player_contract_value = $row->player_contract_value;
$gender_id = $row->gender_id;
$player_city = $row->player_city;
$player_number = $row->player_number;
$player_post_code = $row->player_post_code;
$player_national_id_number = $row->player_national_id_number;
$bank_account_number = $row->bank_account_number;
$bank_branch_code = $row->bank_branch_code;
$image = $row->player_image;
$license_number = $row->licence_number;
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$team_id = set_value('team_id');
	$engagement_date = set_value('engagement_date');
	$player_onames = set_value('player_onames');
	$player_fname = set_value('player_fname');
	$player_position = set_value('player_position');
	$player_dob = set_value('player_dob');
	$player_email = set_value('player_email');
	$player_phone = set_value('player_phone');
	$player_address = set_value('player_address');
	$player_locality = set_value('player_locality');
	$title_id = set_value('title_id');
	$gender_id = set_value('gender_id');
	$player_city = set_value('player_city');
	$player_number = set_value('player_number');
	$player_post_code = set_value('player_post_code');
	$team_id = set_value('team_id');
	$bank_account_number = set_value('bank_account_number');
	$player_nssf_number = set_value('player_nssf_number');
	$player_kra_pin = set_value('player_kra_pin');
	$player_national_id_number = set_value('player_national_id_number');
	$player_nhif_number = set_value('player_nhif_number');
	$player_type_id2 = set_value('player_type_id');
	$bank_id2 = set_value('bank_id');
	$bank_team_id2 = set_value('bank_team_id');
	$player_duration = set_value('player_duration');
	$player_contract_value = set_value('player_contract_value');
	$bank_account_number = set_value('bank_account_number');
	$bank_branch_code = set_value('bank_branch_code');
	$contract_end_date = set_value('contract_end_date');
	$license_number = set_value('license_number');
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">About <?php echo $player_onames.' '.$player_fname;?></h2>
    </header>
    <div class="panel-body">
    <!-- Adding Errors -->
    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }

    ?>

<?php echo form_open_multipart('soccer-management/edit-player/'.$player_id, array("class" => "form-horizontal", "id" => "player_edit"));?>
<input type="hidden" name="current_player_image" value="<?php echo $image;?>" />

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Title: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="title_id">
                	<?php
                    	if($titles->num_rows() > 0)
						{
							$title = $titles->result();
							
							foreach($title as $res)
							{
								$db_title_id = $res->title_id;
								$title_name = $res->title_name;
								
								if($db_title_id == $title_id)
								{
									echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Other Names: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_onames" placeholder="Other Names" value="<?php echo $player_onames;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_fname" placeholder="First Name" value="<?php echo $player_fname;?>">
            </div>
        </div>
        
       <!--<div class="form-group">
            <label class="col-lg-5 control-label">Player number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_number" placeholder="Player number" value="<?php echo $player_number;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">FKF license number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="license_number" placeholder="FKF license number" value="<?php echo $license_number;?>">
            </div>
        </div>-->
        <div class="form-group">
            <label class="col-lg-5 control-label">Player position: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_position" placeholder="Player position" value="<?php echo $player_position;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Contract start date: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name=" engagement_date" placeholder="Engagement Date" value="<?php echo $engagement_date;?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Contract end date: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="contract_end_date" placeholder="Contract end date" value="<?php echo $contract_end_date;?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Date of Birth: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="player_dob" placeholder="Date of Birth" value="<?php echo $player_dob;?>">
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">ID number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_national_id_number" placeholder="ID number" value="<?php echo $player_national_id_number;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Gender: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="gender_id">
                	<?php
                    	if($genders->num_rows() > 0)
						{
							$gender = $genders->result();
							
							foreach($gender as $res)
							{
								$db_gender_id = $res->gender_id;
								$gender_name = $res->gender_name;
								
								if($db_gender_id == $gender_id)
								{
									echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Bank account number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="bank_account_number" placeholder="Bank account number" value="<?php echo $bank_account_number;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Player image *:</label>
            <div class="col-lg-7">
                
                <div class="row">
                
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                     <img src="<?php echo base_url()."assets/document_uploads/".$image;?>">
                                </div>
	                            <div>
		                            <span class="btn btn-file btn_pink"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="player_image"></span>
		                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
		                        </div>
                        	</div>
                    	</div>
                	</div>
                </div>
            </div>
        </div>
	</div>
    
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Bank branch code: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="bank_branch_code" placeholder="Bank branch code" value="<?php echo $bank_branch_code;?>">
            </div>
        </div>
    	 <div class="form-group">
		
            <label class="col-lg-5 control-label">Civil Status: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="civil_status_id">
                	<?php
                    	if($civil_statuses->num_rows() > 0)
						{
							$status = $civil_statuses->result();
							
							foreach($status as $res)
							{
								$status_id = $res->civil_status_id;
								$status_name = $res->civil_status_name;
								
								if($status_id == $civil_status_id)
								{
									echo '<option value="'.$status_id.'" selected>'.$status_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$status_id.'">'.$status_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
		 <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_email" placeholder="Email Address" value="<?php echo $player_email;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Phone: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_phone" placeholder="Phone" value="<?php echo $player_phone;?>">
            </div>
        </div>
        
         <div class="form-group">
            <label class="col-lg-5 control-label">Residence: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_locality" placeholder="Residence" value="<?php echo $player_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Address: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_address" placeholder="Address" value="<?php echo $player_address;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">City: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_city" placeholder="City" value="<?php echo $player_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Post code: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_post_code" placeholder="Post code" value="<?php echo $player_post_code;?>">
            </div>
        </div>
        <div class="form-group">
		
            <label class="col-lg-5 control-label">Team: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="team_id">
                	<?php
                    	if($teams->num_rows() > 0)
						{
							$team = $teams->result();
							
							foreach($team as $res)
							{
								$db_team_id = $res->team_id;
								$team_name = $res->team_name;
								
								if($db_team_id == $team_id)
								{
									echo '<option value="'.$db_team_id.'" selected>'.$team_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_team_id.'">'.$team_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Contract Duration: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_duration" placeholder="Duration(months)" value="<?php echo $player_duration;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Contract Value: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="player_contract_value" placeholder="Contract Value" value="<?php echo $player_contract_value;?>">
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit player
            </button>
        </div>
    </div>
</div>
            <?php echo form_close();?>
                </div>
            </section>
            <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Contract Upload</h2>
    </header>
    <div class="panel-body">
    	<?php echo form_open_multipart('soccer_management/player/upload_player_documents/'.$player_id, array("class" => "form-horizontal", "role" => "form"));?>
    
	    <div class="row" style="margin-top:10px;">
	        <div class="col-md-12">
	        	<div class="col-md-6">
		            <div class="form-group">
		                <label class="col-lg-5 control-label ">Document Name *: </label>
		                <div class="col-lg-7">
		                    <input type="text" class="form-control " name="document_item_name"  placeholder="Document Title Name">
		                </div>
		            </div>
		        </div>
                	<!-- Image -->
                    
	        	<div class="col-md-6">
		            <div class="form-group">
		                <label class="col-lg-5 control-label ">Document Scan *: </label>
		                
		                <div class="col-lg-7">
		                    <input type="file" class="form-control " name="document_scan"  value="">
		                </div>
		            </div>
		        </div>
	        </div>
	    </div>
	    <div class="row" style="margin-top:10px;">
	        <div class="col-md-12">
	            <div class="form-actions center-align">
	                <button class="submit btn btn-primary" type="submit">
	                    Upload document scan
	                </button>
	            </div>
	        </div>
	    </div>
	<?php echo form_close();?><div class="row" style="margin-top:10px;">
	   		<div class="col-md-12">
		       <?php
		       if($player_other_documents->num_rows() > 0)
		        {
		            $count = 0;
		                
		            $identification_result = 
		            '
		            <table class="table table-bordered table-striped table-condensed">
		                <thead>
		                    <tr>
		                        <th>#</th>
		                        <th>Document Name</th>
		                        <th>Download Link</th>
		                        <th colspan="2">Actions</th>
		                    </tr>
		                </thead>
		                  <tbody>
		                  
		            ';
		            
		            foreach ($player_other_documents->result() as $row)
		            {
		                $document_upload_id = $row->document_upload_id;
		                $document_name = $row->document_name;
		                $document_upload_name = $row->document_upload_name;
		                $document_status = $row->document_status;
		                
		                //create deactivated status display
		                if($document_status == 0)
		                {
		                    $status = '<span class="label label-default">Deactivated</span>';
		                    $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-personnel-identification/'.$document_upload_id.'/'.$player_id.'" onclick="return confirm(\'Do you want to activate?\');" title="Activate "><i class="fa fa-thumbs-up"></i></a>';
		                }
		                //create activated status display
		                else if($document_status == 1)
		                {
		                    $status = '<span class="label label-success">Active</span>';
		                    $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-personnel-identification/'.$document_upload_id.'/'.$player_id.'" onclick="return confirm(\'Do you want to deactivate ?\');" title="Deactivate "><i class="fa fa-thumbs-down"></i></a>';
		                }
		                
		                $count++;
		                $identification_result .= 
		                '
		                    <tr>
		                        <td>'.$count.'</td>
		                        <td>'.$document_name.'</td>
		                        <td><a href="'.$this->document_upload_location.''.$document_upload_name.'" target="_blank" >Download Here</a></td>
		                        <td>'.$status.'</td>
		                    </tr> 
		                ';
		            }
		            
		            $identification_result .= 
		            '
		                          </tbody>
		                        </table>
		            ';
		        }
		        
		        else
		        {
		            $identification_result = "<p>No documents have been added</p>";
		        }
		        echo $identification_result;
		       ?>
	       </div>
       </div>
    </div>
    </section>