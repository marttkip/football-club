          
          <department class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                    </div>
            
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>administration/departments" class="btn btn-info pull-right">Back to departments</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                    }
                    
                    $validation_errors = validation_errors();
                    
                    if(!empty($validation_errors))
                    {
                        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                    }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                    <!-- Department Name -->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Department Name</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="department_name" placeholder="Department Name" value="<?php echo set_value('department_name');?>" >
                        </div>
                    </div>
                    <!-- Department Parent -->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Parent</label>
                        <div class="col-lg-6">
                            <select name="department_parent" class="form-control" >
                                <?php
                                echo '<option value="0">No Parent</option>';
                                if($all_departments->num_rows() > 0)
                                {
                                    $result = $all_departments->result();
                                    
                                    foreach($result as $res)
                                    {
                                        if($res->department_id == set_value('department_parent'))
                                        {
                                            echo '<option value="'.$res->department_id.'" selected>'.$res->department_name.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$res->department_id.'">'.$res->department_name.'</option>';
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- Department Position -->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Position</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="department_position" placeholder="Position" value="<?php echo set_value('department_position');?>" >
                        </div>
                    </div>
                    <!-- Department Icon -->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Icon</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="department_icon" placeholder="Icon" value="<?php echo set_value('department_icon');?>" >
                        </div>
                    </div>
                    <!-- Activate checkbox -->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Activate Department?</label>
                        <div class="col-lg-6">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios1" type="radio" checked value="1" name="department_status">
                                    Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" value="0" name="department_status">
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6 col-lg-offset-4">
                            <div class="form-actions center-align">
                                <button class="submit btn btn-primary" type="submit">
                                    Add department
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <br />
                    <?php echo form_close();?>
                </div>
            </department>